package org.uaipet.api.data.model;


import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.util.ArrayList;
import java.util.List;

@ToString
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "id")
@Table(name =  "company")
public class Company extends DefaultEntity {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name = "id", length = 20)
    private Long id;

    @Column(name = "cnpj", length = 14, unique = true)
    private String cnpj;

    @Column(name = "fee")
    private Integer fee;

    @Column(name = "monthly_fee")
    private Integer monthlyFee;

    @Column(name = "minimum_order_amount")
    private String minimumOrderAmount;

    @Column(name = "amount_free_freight")
    private String amountFreeFreight;

    @Column(name = "image")
    private String image;

    @Column(name = "phone", length = 11)
    private String phone;

    @Column(name = "name")
    private String name;

    @Column(name = "social_contract_name")
    private String socialContractName;

    @OneToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "address_id")
    private Address address;

    @OneToMany(mappedBy = "company",
            cascade = CascadeType.ALL)
    private List<BusinessHour> businessHours = new ArrayList<>();

    @OneToMany(mappedBy = "company", fetch = FetchType.EAGER,
            cascade = CascadeType.ALL)
    private List<DeliveryRange> deliveryRanges = new ArrayList<>();

    @OneToMany(mappedBy = "company",
            cascade = CascadeType.PERSIST)
    private List<User> userList = new ArrayList<>();

}
