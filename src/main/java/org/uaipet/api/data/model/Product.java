package org.uaipet.api.data.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;

@ToString
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "id")
@Entity
@Table(name =  "PRODUCT")
public class Product extends DefaultEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;

    @Column(name = "NAME")
    private String name;

    @Column(name = "DESCRIPTION", length = 4000)
    private String description;

    @Column(name = "PRICE")
    private String price;

    @Column(name = "STOCK_COUNT")
    private Integer stockCount;

    @Column(name = "MEASUREMENT_VALUE")
    private String measurementValue;

    @Column(name = "IMAGE", length = 4000)
    private String image;

    @ManyToOne
    @JoinColumn(name = "PRODUCT_BRAND_ID")
    private ProductBrand productBrand;

    @ManyToOne
    @JoinColumn(name = "PRODUCT_ANIMAL_CATEGORY_ID")
    private ProductAnimalCategory productAnimalCategory;

    @ManyToOne
    @JoinColumn(name = "PRODUCT_CATEGORY_ID")
    private ProductCategory productCategory;

    @ManyToOne
    @JoinColumn(name = "PRODUCT_MEASUREMENT_CATEGORY_ID")
    private ProductMeasurementCategory productMeasurementCategory;

    @ManyToOne
    @JoinColumn(name = "AGE_CATEGORY_ID")
    private ProductAgeCategory productAgeCategory;

    @ManyToOne
    @JoinColumn(name = "COMPANY_ID")
    private Company company;

    @Column(name =" deleted", columnDefinition = "boolean default false")
    private Boolean deleted;

    @Column(name =" spotlight", columnDefinition = "boolean default false")
    private Boolean spotlight;

    @Column(name =" available", columnDefinition = "boolean default true")
    private Boolean available;

}
