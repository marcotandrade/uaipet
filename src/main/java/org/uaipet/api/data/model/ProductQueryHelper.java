package org.uaipet.api.data.model;


import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@StaticMetamodel(Product.class)
public class ProductQueryHelper {

    public static volatile SingularAttribute<Product, String> name;
    public static volatile SingularAttribute<Product, String> company;
    public static volatile SingularAttribute<Product, String> brandCategory;
    public static volatile SingularAttribute<Product, String> animalCategory;
    public static volatile SingularAttribute<Product, String> ageCategory;
    public static final String NAME = "name";
    public static final String COMPANY = "company";
    public static final String PRODUCT_BRAND = "productBrand";
    public static final String ANIMAL_CATEGORY = "productAnimalCategory";
    public static final String PRODUCT_CATEGORY = "productCategory";
    public static final String AGE_CATEGORY = "productAgeCategory";
    public static final String DELETED = "deleted";
    public static final String SPOTLIGHT = "spotlight";
    public static final String AVAILABLE = "available";
    public static final String PRICE = "price";
}
