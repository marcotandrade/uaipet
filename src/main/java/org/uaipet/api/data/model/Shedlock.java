package org.uaipet.api.data.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDateTime;


@Getter
@Setter
@Builder
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name =  "shedlock")
public class Shedlock {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name = "id", length = 20)
    private Long id;

    @Column(name = "name", length = 64)
    private String name;

    @Column(name = "locked_at", length = 70)
    private LocalDateTime lockedAt;

    @Column(name = "lock_until", length = 70)
    private LocalDateTime lockUntil;

    @Column(name = "locked_by")
    private String lockedBy;

}
