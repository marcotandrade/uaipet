package org.uaipet.api.data.model;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.Column;
import javax.persistence.EntityListeners;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import java.time.LocalDateTime;


@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
public class DefaultEntity {

    //@CreatedDate
    @Column(name = "created_date", updatable = false)
    private LocalDateTime createdDate;

    //@LastModifiedDate
    @Column(name = "last_modified_date")
    private LocalDateTime lastModifiedDate;

    public LocalDateTime getCreatedDate() {
        return createdDate;
    }

    @PrePersist
    public void setCreatedDate() {
        this.createdDate = LocalDateTime.now().minusHours(3);
        this.lastModifiedDate = LocalDateTime.now().minusHours(3);
    }

    public LocalDateTime getLastModifiedDate() {
        return lastModifiedDate;
    }

    @PreUpdate
    public void setLastModifiedDate() {
        this.lastModifiedDate = LocalDateTime.now().minusHours(3);
    }
}
