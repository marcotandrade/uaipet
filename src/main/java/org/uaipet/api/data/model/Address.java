package org.uaipet.api.data.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.uaipet.api.data.enums.StateEnum;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@ToString
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "id")
@Table(name =  "address")
public class Address extends DefaultEntity {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name = "id", length = 20)
    private Long id;

    @Column(name = "city", length = 150)
    private String city;

    @Column(name = "complement", length = 50)
    private String complement;

    @Column(name = "district", length = 50)
    private String district;

    @Column(name = "number")
    private Integer number;

    @Column(name = "zip_code")
    private Integer zipCode;

    @Enumerated(EnumType.STRING)
    @Column(name = "state")
    private StateEnum state;

    @Column(name = "street")
    private String  street;

    @Column(name = "latitude")
    private Double latitude;

    @Column(name = "longitude")
    private Double longitude;

    @Column(name = "default_address")
    private Boolean defaultAddress;

    @ToString.Exclude
    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    @Column(name = "deleted", columnDefinition = "boolean default false")
    private Boolean deleted;

}
