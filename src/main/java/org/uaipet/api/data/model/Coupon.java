package org.uaipet.api.data.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.uaipet.api.data.enums.CouponTypeEnum;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.time.LocalDateTime;

@ToString
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonIdentityInfo(
        generator = ObjectIdGenerators.PropertyGenerator.class,
        property = "id")
@Entity
@Table(name =  "coupon")
public class Coupon extends DefaultEntity {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name = "id", length = 20)
    private Long id;

    @Column(name = "code", length = 50)
    private String code;

    @Column(name = "expiration_date")
    private LocalDateTime expirationDate;

    @Column(name = "active")
    private Boolean active;

    @Column(name = "amount")
    private String value;

    @Column(name = "rule")
    private String rule;

    @Column(name = "description")
    private String description;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    private User user;

    @Enumerated(EnumType.STRING)
    @Column(name = "type", length = 12)
    private CouponTypeEnum type;

    @Column(name = "usage_count")
    private Integer usageCount;

    @Column(name = "max_amount_allowed")
    private String maxAmountAllowed;

    @Column(name = "discount_percentage")
    private Integer discountPercentage;

}
