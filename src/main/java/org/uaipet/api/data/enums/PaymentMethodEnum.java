package org.uaipet.api.data.enums;

public enum PaymentMethodEnum {

    CARD("card"),
    CASH("cash");

    public String description;

    PaymentMethodEnum(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

}
