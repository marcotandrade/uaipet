package org.uaipet.api.data.enums;

import lombok.Getter;

@Getter
public enum UserRoleEnum {

    USER("role_user"),
    COMPANY_ADMIN("role_company_admin"),
    COMPANY_ATTENDANT("role_company_attendant");

    public String name;

    UserRoleEnum(String name) {
        this.name = name;
    }

    public static UserRoleEnum toEnum(Integer cod) {

        if (cod == null) {
            return null;
        }

        for (UserRoleEnum x : UserRoleEnum.values()) {
            if (cod.equals(x.getName())) {
                return x;
            }
        }

        throw new IllegalArgumentException("Id inválido: " + cod);
    }
}
