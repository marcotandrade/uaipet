package org.uaipet.api.data.enums;

public enum SaleStatusEnum {

    WAITING_CONFIRMATION("waiting_confirmation"),
    PREPARING_PRODUCT("preparing_product"),
    SENT_TO_DELIVERY("sent_to_delivery"),
    FINISHED("finished"),
    CANCELED("canceled");

    public String description;


    SaleStatusEnum(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }
}
