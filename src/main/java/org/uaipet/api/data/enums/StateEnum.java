package org.uaipet.api.data.enums;

import lombok.Getter;
import org.uaipet.api.service.exception.CityNotConfiguredException;

@Getter
public enum StateEnum {
    MG("MG"),
    SP("SP"),
    RJ("RJ"),
    GO("GO"),
    TO("TO"),
    BA("BA"),
    MA("MA"),
    SC("SC"),
    PA("PA"),
    RS("RS"),
    AC("AC"),
    AL("AL"),
    AP("AP"),
    AM("AM"),
    CE("CE"),
    ES("ES"),
    MT("MT"),
    MS("MS"),
    PB("PB"),
    PE("PE"),
    PI("PI"),
    SE("SE"),
    DF("DF"),
    RR("RR"),
    RN("RN"),
    RO("RO");

    public String name;


    StateEnum(String name) {
        this.name = name;
    }

    public static StateEnum toEnum(String stateName) {
        if (stateName == null) {
            return null;
        }
        for (StateEnum state : StateEnum.values()) {
            if (stateName.equals(state.getName())) {
                return state;
            }
        }
        throw new CityNotConfiguredException();
    }
}
