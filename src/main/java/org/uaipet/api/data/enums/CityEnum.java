package org.uaipet.api.data.enums;

import lombok.Getter;
import org.uaipet.api.service.exception.CityNotConfiguredException;

@Getter
public enum CityEnum {

    UBERLANDIA("uberlândia");

    public String name;

    CityEnum(String name) {
        this.name = name;
    }

    public static CityEnum toEnum(String cityName) {
        if (cityName == null) {
            return null;
        }
        for (CityEnum city : CityEnum.values()) {
            if (cityName.equals(city.getName())) {
                return city;
            }
        }
        throw new CityNotConfiguredException();
    }
}
