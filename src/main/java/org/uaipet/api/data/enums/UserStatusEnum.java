package org.uaipet.api.data.enums;

import lombok.Getter;

@Getter
public enum UserStatusEnum {

    VALID("valid"),
    NOT_VALID("not_valid");

    private String status;

    UserStatusEnum(String status) {
        this.status = status;
    }
}
