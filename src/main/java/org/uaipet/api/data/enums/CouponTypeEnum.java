package org.uaipet.api.data.enums;

public enum CouponTypeEnum {

    MULTIPLE_USE("multiple_use"),
    SINGLE_USE("single_use");

    public String name;


    CouponTypeEnum(String name) {
        this.name = name;
    }
}
