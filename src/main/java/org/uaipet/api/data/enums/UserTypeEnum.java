package org.uaipet.api.data.enums;

public enum UserTypeEnum {

    CUSTOMER(1, "customer"),
    COMPANY(2, "company");

    private int cod;
    private String description;

    UserTypeEnum(int cod, String description) {
        this.cod = cod;
        this.description = description;
    }

    public int getCod() {
        return cod;
    }

    public String getDescription() {
        return description;
    }
}
