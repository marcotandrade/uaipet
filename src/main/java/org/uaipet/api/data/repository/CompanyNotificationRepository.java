package org.uaipet.api.data.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.uaipet.api.data.model.CompanyNotification;
import org.uaipet.api.data.model.SaleStatus;
import org.uaipet.api.data.repository.projection.NotificationProjection;

import java.util.List;

public interface CompanyNotificationRepository extends JpaRepository<CompanyNotification, Long> {


    @Query(nativeQuery = true,
            value = "select users.name as customerName, s.id as orderId, " +
                    "notif.id as notificationId, notif.viewed as viewed , notif.created_date as createdAt , COUNT(1) OVER() totalNotification " +
                    "from company c " +
                    "inner join sale s on c.id = s.company_id " +
                    "inner join users users on users.id = s.user_id " +
                    "inner join sale_status ss on s.id = ss.sale_id " +
                    "and ss.status = 'WAITING_CONFIRMATION' " +
                    "inner join company_notification notif on notif.sale_status_id = ss.id " +
                    "and viewed = false " +
                    "where c.cnpj = :cnpj " +
                    "and 1 = (select count(id) " +
                    "from sale_status ss2 " +
                    "where ss2.sale_id = s.id) ")
    List<NotificationProjection> findCompanyNotifications(@Param("cnpj") String cnpj);

    CompanyNotification findBySaleStatusAndViewedFalse(SaleStatus saleStatus);

}
