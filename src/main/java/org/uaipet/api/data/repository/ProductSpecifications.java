package org.uaipet.api.data.repository;

import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.uaipet.api.data.model.Company;
import org.uaipet.api.data.model.Product;
import org.uaipet.api.data.model.ProductQueryHelper;
import org.uaipet.api.dto.request.ProductRequest;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

public class ProductSpecifications {

    public static Specification<Product> nameLike(String name){
        if(name == null){
            return null;
        }
        return (root, query, criteriaBuilder)
                -> criteriaBuilder.like(
                criteriaBuilder.upper(root.get(ProductQueryHelper.NAME)), "%"+name.toUpperCase()+"%");
    }

    public static Specification<Product> notDeleted(){
        return (root, query, criteriaBuilder)
                -> criteriaBuilder.equal(root.get(ProductQueryHelper.DELETED), false);
    }

    public static Specification<Product> onSpotlight(){
        return (root, query, criteriaBuilder)
                -> criteriaBuilder.equal(root.get(ProductQueryHelper.SPOTLIGHT), true);
    }

    public static Specification<Product> isAvailable(){
        return (root, query, criteriaBuilder)
                -> criteriaBuilder.equal(root.get(ProductQueryHelper.AVAILABLE), true);
    }

    public static Specification<Product> companyIn(List<Company> companyList) {
        if (companyList == null) {
            return null;
        }
        return (root, query, criteriaBuilder) ->
                criteriaBuilder.in(root.get(ProductQueryHelper.COMPANY)).value(companyList);
    }

    public static Specification<Product> ageCategoryEquals(ProductRequest request){
        if(request == null || request.getAgeCategory() == null || request.getAgeCategory().getId() == null){
            return null;
        }
        return (root, query, criteriaBuilder)
                -> criteriaBuilder.equal(root.get(ProductQueryHelper.AGE_CATEGORY), request.getAgeCategory().getId());
    }

    public static Specification<Product> animalCategoryEquals(ProductRequest request){
        if(request == null || request.getProductAnimalCategory() == null || request.getProductAnimalCategory().getId() == null){
            return null;
        }
        return (root, query, criteriaBuilder)
                -> criteriaBuilder.equal(root.get(ProductQueryHelper.ANIMAL_CATEGORY), request.getProductAnimalCategory().getId());
    }

    public static Specification<Product> productCategoryEquals(ProductRequest request){
        if(request == null || request.getProductCategory() == null || request.getProductCategory().getId() == null){
            return null;
        }
        return (root, query, criteriaBuilder)
                -> criteriaBuilder.equal(root.get(ProductQueryHelper.PRODUCT_CATEGORY), request.getProductCategory().getId());
    }

    public static Specification<Product> productBrandEquals(ProductRequest request){
        if(request == null || request.getProductBrand() == null || request.getProductBrand().getId() == null){
            return null;
        }
        return (root, query, criteriaBuilder)
                -> criteriaBuilder.equal(root.get(ProductQueryHelper.PRODUCT_BRAND), request.getProductBrand().getId());
    }


    public static Specification<Product> priceOrdered(ProductRequest request){
        if(request == null || request.getSortBy() == null || !request.getSortBy().equalsIgnoreCase("price")){
            return null;
        }

        return (root, query, criteriaBuilder)
                -> {
            query.orderBy(mapSortOrderToCriteriaOrder(criteriaBuilder, request, root, Sort.Order.by(request.getDirection())));
            return criteriaBuilder.and();
        };
    }

    private static Order mapSortOrderToCriteriaOrder(CriteriaBuilder criteriaBuilder, ProductRequest request,
                                                     Root<Product> root, Sort.Order sortOrder) {
        Expression<?> expression = root.get(request.getSortBy());
        if (ProductQueryHelper.PRICE.equals(request.getSortBy())) {
            expression = expression.as(BigDecimal.class);
        }
        return sortOrder.isAscending() ? criteriaBuilder.asc(expression) : criteriaBuilder.desc(expression);

    }


}
