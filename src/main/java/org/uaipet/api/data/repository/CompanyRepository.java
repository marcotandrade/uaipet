package org.uaipet.api.data.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.uaipet.api.data.enums.CityEnum;
import org.uaipet.api.data.enums.StateEnum;
import org.uaipet.api.data.model.Company;

import java.util.List;

public interface CompanyRepository  extends JpaRepository<Company, Long> {

    Company findOneByCnpj(String cnpj);

    List<Company> findByAddressCityAndAddressState(String city, StateEnum state);

}
