package org.uaipet.api.data.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.uaipet.api.data.model.DeliveryRange;

import java.util.List;

public interface DeliveryRangeRepository extends JpaRepository<DeliveryRange, Long> {

    @Query("SELECT d FROM DeliveryRange d WHERE d.company.id = :id")
    List<DeliveryRange> findByCompanyId(Long id);
}
