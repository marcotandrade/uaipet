package org.uaipet.api.data.repository;

import org.uaipet.api.data.model.Product;

import java.util.Comparator;

public class ProductComparator implements Comparator<Product> {

    @Override
    public int compare(Product product, Product product2) {
       Double p1 = Double.valueOf(product.getPrice());
       Double p2 = Double.valueOf(product2.getPrice());
       return p1.compareTo(p2);
    }
}
