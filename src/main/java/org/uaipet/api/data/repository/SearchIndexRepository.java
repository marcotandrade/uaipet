package org.uaipet.api.data.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.uaipet.api.data.model.Address;
import org.uaipet.api.data.model.Company;
import org.uaipet.api.data.model.SearchIndex;
import org.uaipet.api.data.model.User;

import java.util.List;

public interface SearchIndexRepository  extends JpaRepository<SearchIndex, Long> {

    SearchIndex findFirstByUserAndCompanyAndUserAddress(User user, Company company, Address address);
    List<SearchIndex> findByUserAndUserAddress(User user, Address address);

    Integer deleteByUserAddress(Address address);

    Integer deleteByUserAddressId(Long address);
    Integer deleteByCompany(Company company);
}
