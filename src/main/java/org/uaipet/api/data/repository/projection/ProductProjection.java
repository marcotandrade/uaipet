package org.uaipet.api.data.repository.projection;

public interface ProductProjection {

    public Long getId();
    public void setId(Long id);

    public Long getCompanyId();
    public void setCompanyId(Long id);

    public String getName();
    public void setName(String name);

    public String getDescription();
    public void setDescription(String description);

    public String getPrice();
    public void setPrice(String price);

    public Integer getStockCount();
    public void setStockCount(Integer stockCount);

    public String getMeasurementValue();
    public void setMeasurementValue(String measurementValue);

    public String getImage();
    public void setImage(String image);

}
