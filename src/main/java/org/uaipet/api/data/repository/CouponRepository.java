package org.uaipet.api.data.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.uaipet.api.data.model.Coupon;
import org.uaipet.api.data.model.User;

import java.time.LocalDateTime;
import java.util.List;

public interface CouponRepository extends JpaRepository<Coupon, Long> {

    Coupon findByCodeAndUserAndActiveTrueAndExpirationDateAfter(String code, User user, LocalDateTime date);
    Coupon findByCodeAndActiveTrueAndExpirationDateAfter(String code, LocalDateTime date);

    List<Coupon> findByActiveTrueAndExpirationDateBefore(LocalDateTime date);

    Coupon findByCodeAndActiveTrue(String code);


}
