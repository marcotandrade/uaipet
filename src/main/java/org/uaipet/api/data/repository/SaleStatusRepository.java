package org.uaipet.api.data.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.uaipet.api.data.enums.SaleStatusEnum;
import org.uaipet.api.data.model.SaleStatus;

import java.util.Optional;

public interface SaleStatusRepository extends JpaRepository<SaleStatus, Long> {

    Optional<SaleStatus> findBySaleIdAndStatus(Long id, SaleStatusEnum status);
}
