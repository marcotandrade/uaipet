package org.uaipet.api.data.repository.projection;

import java.util.Date;

public interface NotificationProjection {

    String getCustomerName();
    String setCustomerName(String s);

    Integer getOrderId();
    Integer setOrderId(Integer i);

    Integer getNotificationId();
    Integer setNotificationId(Integer i);

    Boolean getViewed();
    void setViewed(Boolean b);

    Date getCreatedAt();
    void setCreatedAt(Date d);

    Integer getTotalNotification();
    Integer setTotalNotification(Integer i);

}
