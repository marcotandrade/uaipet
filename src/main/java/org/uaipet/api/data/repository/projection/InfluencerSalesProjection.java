package org.uaipet.api.data.repository.projection;

public interface InfluencerSalesProjection {

    String getCustomerName();
    String setCustomerName(String s);

    String getProfit();
    String setProfit(String s);
}
