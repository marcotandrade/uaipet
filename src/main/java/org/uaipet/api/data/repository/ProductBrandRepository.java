package org.uaipet.api.data.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.uaipet.api.data.model.ProductBrand;

import java.util.List;


public interface ProductBrandRepository extends JpaRepository<ProductBrand, Long> {


    List<ProductBrand> findAllByOrderByName();
}
