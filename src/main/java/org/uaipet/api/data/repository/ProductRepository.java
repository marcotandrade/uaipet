package org.uaipet.api.data.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.uaipet.api.data.model.Product;
import org.uaipet.api.data.repository.projection.ProductProjection;


public interface ProductRepository extends JpaRepository<Product, Long> , JpaSpecificationExecutor<Product> {

    Boolean existsByIdAndStockCountGreaterThanEqual(Long name, Integer count);

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    @Modifying
    @Query(value = "UPDATE PRODUCT SET STOCK_COUNT = STOCK_COUNT -?1 WHERE ID = ?2", nativeQuery = true)
    void countDownProductStockQtt(Integer qtt, Long prodId);

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    @Modifying
    @Query(value = "UPDATE PRODUCT SET STOCK_COUNT = STOCK_COUNT +?1 WHERE ID = ?2", nativeQuery = true)
    void rollbackStockCountDown(Integer qtt, Long prodId);


    @Query(nativeQuery = true,
    value = "select id, name, description, price, stock_count stockCount, measurement_value measurementValue, image\n" +
            "company_id companyId\n" +
            "from product\n" +
            "where  id = :id")
    ProductProjection findProductById(@Param("id") Long id);


}
