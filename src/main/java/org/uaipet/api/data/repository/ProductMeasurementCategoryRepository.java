package org.uaipet.api.data.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.uaipet.api.data.model.ProductMeasurementCategory;


public interface ProductMeasurementCategoryRepository extends JpaRepository<ProductMeasurementCategory, Long> {

}
