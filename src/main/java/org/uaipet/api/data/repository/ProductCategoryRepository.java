package org.uaipet.api.data.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.uaipet.api.data.model.ProductCategory;


public interface ProductCategoryRepository extends JpaRepository<ProductCategory, Long> {

}
