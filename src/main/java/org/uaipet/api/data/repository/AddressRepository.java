package org.uaipet.api.data.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.uaipet.api.data.model.Address;
import org.uaipet.api.data.model.User;

import java.util.List;
import java.util.Optional;

public interface AddressRepository extends JpaRepository<Address, Long> {

    @Modifying
    @Query(value = "update address set default_address = false where user_id =:userId ", nativeQuery = true)
    Integer updateAddressesToNonDefault(Long userId);

    Optional<Address> findByIdAndUserEmail(Long id, String email);
    Integer countByUserEmail(String email);

    Optional<Address> findOneByUserEmail(String email);

    Integer deleteByIdAndUserEmail(Long id, String email);

    List<Address> findByUser(User user);

}
