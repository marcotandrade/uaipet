package org.uaipet.api.data.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.uaipet.api.data.model.BusinessHour;
import org.uaipet.api.data.model.Company;

import java.util.List;

public interface BusinessHourRepository extends JpaRepository<BusinessHour, Long> {

    List<BusinessHour> findByActiveFalse();

    int deleteByCompany(Company company);
}
