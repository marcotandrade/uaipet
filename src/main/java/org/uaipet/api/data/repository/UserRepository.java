package org.uaipet.api.data.repository;

import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.uaipet.api.data.enums.StateEnum;
import org.uaipet.api.data.model.User;
import org.uaipet.api.data.repository.projection.InfluencerSalesProjection;

import java.util.List;

public interface UserRepository extends JpaRepository<User, Long>  {

    User findByEmail(String email);


    List<User> findAllByEmailNotNull();

    User findByEmailAndId(String email, Long id);

    User findFirstByExpoToken(String token);

    Slice<User> findByAddressListCityAndAddressListState(String city, StateEnum state, Pageable pageable);

    Slice<User> findByExpoTokenNotNullAndEmailIsNull(Pageable pageable);
    Slice<User> findByExpoTokenNotNullAndEmailNotNull(Pageable pageable);

    Boolean existsByEmail(String email);

    @Query(value = "select u.* from users u " +
            "inner join company c " +
            "on c.id = u.company_id " +
            "where c.cnpj = :cnpj",
    nativeQuery = true)
    List<User> findByCompany(String cnpj);



    @Query(value = "SELECT U.NAME as customerName,\n" +
            "\tSUM(TO_NUMBER(AMOUNT,'999D99')) * 0.04 as profit\n" +
            "FROM SALE S\n" +
            "INNER JOIN USERS U ON U.ID = S.USER_ID\n" +
            "INNER JOIN SALE_STATUS SS ON SS.SALE_ID = S.ID\n" +
            "AND SS.STATUS = 'FINISHED'\n" +
            "WHERE S.CREATED_DATE >= NOW() - INTERVAL '45 DAYS'\n" +
            "\tAND COUPON_ID IN\n" +
            "\t\t(SELECT CO.ID\n" +
            "\t\t\tFROM USERS US\n" +
            "\t\t\tINNER JOIN COUPON CO ON CO.USER_ID = US.ID\n" +
            "\t\t\tAND CO.ACTIVE = TRUE\n" +
            "\t\t\tWHERE CPF = :cpf)\n" +
            "GROUP BY U.NAME", nativeQuery = true)
    List<InfluencerSalesProjection> getCouponSale(@Param("cpf") String cpf);

}
