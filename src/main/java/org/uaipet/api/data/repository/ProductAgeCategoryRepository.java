package org.uaipet.api.data.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.uaipet.api.data.model.ProductAgeCategory;


public interface ProductAgeCategoryRepository extends JpaRepository<ProductAgeCategory, Long> {

}
