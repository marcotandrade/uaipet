package org.uaipet.api.data.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.uaipet.api.data.model.Sale;
import org.uaipet.api.data.model.SaleStatus;
import org.uaipet.api.data.model.User;
import org.uaipet.api.dto.response.DashboardProductsProjectionResponse;
import org.uaipet.api.dto.response.DashboardSalesProjectionResponse;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

public interface SaleRepository extends JpaRepository<Sale, Long> , JpaSpecificationExecutor<Sale> {

    List<Sale> findByUser(User user);

    @Query(value = "SELECT s.* \n" +
            "from Sale s\n" +
            "    inner join sale_status ss2 on s.id = ss2.sale_id\n" +
            "where\n" +
            "    company_id = :id\n" +
            "    and\n" +
            "      (select status\n" +
            "         from sale_status ss1\n" +
            "         where ss1.sale_id = s.id\n" +
            "         order by ss1.id desc\n" +
            "        limit 1) in :statusList\n" +
            "group by s.id",
            countQuery = "SELECT count(s.id)\n" +
                    "            from Sale s\n" +
                    "                inner join sale_status ss2 on s.id = ss2.sale_id\n" +
                    "            where\n" +
                    "                company_id = :id\n" +
                    "                and\n" +
                    "                  (select status\n" +
                    "                     from sale_status ss1\n" +
                    "                     where ss1.sale_id = s.id\n" +
                    "                     order by ss1.id desc\n" +
                    "                    limit 1) in :statusList",

            nativeQuery = true)
    Page<Sale> findAllOrders(@Param("id") Long companyId,
                             @Param("statusList")  List<String> saleStatus, Pageable pageable);



    @Query(value = "select count(s.id)                                                                          as salesQuantity,\n" +
            "       coalesce(SUM(to_number(s.amount, '99G999.99')),'0.00')                                      as saleAmount,\n" +
            "       COUNT(DISTINCT s.user_id)                                                               qttCustomerPurchased,\n" +
            "       (select COUNT(DISTINCT si.user_id)\n" +
            "        from search_index si\n" +
            "        where si.company_id = (select id from company cc where cc.cnpj = :cnpj)) as qttCustomerAround\n" +
            "from company c\n" +
            "         inner join sale s on c.id = s.company_id\n" +
            "where c.cnpj = :cnpj\n" +
            "  and s.created_date between :initialDate and :finalDate",
    nativeQuery = true)
    DashboardSalesProjectionResponse getSalesDashboard(@Param("cnpj") String cnpj, @Param("initialDate") LocalDateTime initialDate,
                                                             @Param("finalDate") LocalDateTime finalDate);



    @Query(value = "SELECT SUM(si.product_quantity) as quantity,\n" +
            "        p.name ||' '|| p.measurement_value ||' '|| m.name as product\n" +
            "FROM sale_items si\n" +
            " inner join sale s on s.id = si.sale_id\n" +
            "    inner join company c on c.id = s.company_id\n" +
            "    inner join product p on si.product_id = p.id\n" +
            "    inner join product_measurement_category m on p.product_measurement_category_id = m.id\n" +
            "where c.cnpj = :cnpj\n" +
            "  and s.created_date between :initialDate and :finalDate\n" +
            "GROUP BY p.name, p.measurement_value, m.name\n" +
            "order by SUM(si.product_quantity) desc\n" +
            "limit 5",
            nativeQuery = true)
    List<DashboardProductsProjectionResponse> getProductsDashboard(@Param("cnpj") String cnpj, @Param("initialDate") LocalDateTime initialDate,
                                                                   @Param("finalDate") LocalDateTime finalDate);


    @Query(nativeQuery = true,
            value = "select sum(to_number(coupon_amount, '999D99')) from sale where coupon_id = :couponId")
    String getSaleAmountWithCoupon(Long couponId);



}
