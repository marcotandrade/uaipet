package org.uaipet.api.data.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.uaipet.api.data.model.Sale;
import org.uaipet.api.data.model.SaleItems;

import java.util.List;

public interface SaleItemsRepository extends JpaRepository<SaleItems, Long> {

    List<SaleItems> findBySale(Sale sale);

}
