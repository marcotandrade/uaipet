package org.uaipet.api.data.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.uaipet.api.data.model.ProductAnimalCategory;


public interface ProductAnimalCategoryRepository extends JpaRepository<ProductAnimalCategory, Long> {

}
