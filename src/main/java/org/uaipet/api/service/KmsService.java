package org.uaipet.api.service;

import java.nio.ByteBuffer;

public interface KmsService {

    ByteBuffer encode(String value, String keyqa);
    String decode(String cypherText, String keyId);

}
