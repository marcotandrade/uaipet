package org.uaipet.api.service;

import org.uaipet.api.data.model.ProductMeasurementCategory;

import java.util.List;

public interface ProductMeasurementCategoryService {

    List< ProductMeasurementCategory > findAll();

    ProductMeasurementCategory findOne(Long id);

    ProductMeasurementCategory save(ProductMeasurementCategory productMeasurementCategory);

    void update(Long id, ProductMeasurementCategory productMeasurementCategory);

}
