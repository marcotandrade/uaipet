package org.uaipet.api.service;

import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public interface S3StorageService {

    ArrayList<String> uploadImage(List<MultipartFile> images, String imageName, String folderName) throws IOException;

    Boolean deleteImage();

}
