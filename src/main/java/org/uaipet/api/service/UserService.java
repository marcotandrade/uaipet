package org.uaipet.api.service;

import org.springframework.web.multipart.MultipartFile;
import org.uaipet.api.config.security.UserSS;
import org.uaipet.api.data.model.Address;
import org.uaipet.api.data.model.User;
import org.uaipet.api.dto.request.AddressRequest;
import org.uaipet.api.dto.request.UserRequest;
import org.uaipet.api.dto.request.ValidationRequest;
import org.uaipet.api.dto.response.InfluencerSalesResponse;
import org.uaipet.api.dto.response.RegisterResponse;
import org.uaipet.api.dto.response.ReportProblemResponse;
import org.uaipet.api.dto.response.UserOrdersResponse;

import java.io.IOException;
import java.util.List;

public interface UserService {

    RegisterResponse saveUser(UserRequest userRequest, List<MultipartFile> image);

    RegisterResponse saveUserIncomplete(UserRequest userRequest);

    Boolean validateUserEmail(ValidationRequest validationRequest);

    Address saveOrUpdateAddress(AddressRequest addressRequest);

    User findUser(UserRequest userRequest);

    User findUserFromEmail(String userRequest);

    User findUserFromEmailAndId(UserRequest userRequest);

    String deleteAddress(Long id, String email);

    User updateUser(UserRequest userRequest, List<MultipartFile> image) throws IOException;

    List<UserOrdersResponse> findUserOrders(String email);

    ReportProblemResponse reportProblem(String saleId);

    UserSS authenticated();

    void requestUserIdEqualsTokenUserId(Long requestUserId);

    Boolean requestUserIsAssociatedWithCompany(Long companyId);

    List<User> findUsersByCompanyCnpj(String cnpj);

    List<InfluencerSalesResponse> getSalesInfoInfluencerCupom(String cpf);

}
