package org.uaipet.api.service;

import org.uaipet.api.data.model.Address;
import org.uaipet.api.data.model.Company;

public interface SearchIndexService {

    void save(Address userAddress, Address storeAddress, Company company);

    void deleteAddressRecord(Long addressId);
}
