package org.uaipet.api.service;

import org.uaipet.api.data.model.ProductCategory;

import java.util.List;

public interface ProductCategoryService {

    List< ProductCategory > findAll();

    ProductCategory findOne(Long id);

    ProductCategory save(ProductCategory productCategory);

    void update(Long id, ProductCategory productCategory);

}
