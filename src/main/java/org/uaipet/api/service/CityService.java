package org.uaipet.api.service;

import org.uaipet.api.data.model.City;
import org.uaipet.api.dto.request.AddressRequest;

public interface CityService {

    City findCity(AddressRequest city, Boolean blockRegister);


}
