package org.uaipet.api.service;

import org.uaipet.api.dto.request.PushNotificationRequest;

public interface PushNotificationService {

   void sendNotification(PushNotificationRequest pushNotificationRequest,Boolean isToValidate);

}
