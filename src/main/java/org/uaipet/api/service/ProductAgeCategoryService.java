package org.uaipet.api.service;

import org.uaipet.api.data.model.ProductAgeCategory;

import java.util.List;

public interface ProductAgeCategoryService {

    List< ProductAgeCategory > findAll();

    ProductAgeCategory findOne(Long id);

    ProductAgeCategory save(ProductAgeCategory ageCategory);

    void update(Long id, ProductAgeCategory ageCategory);

}
