package org.uaipet.api.service;

import org.uaipet.api.data.model.Sale;

public interface SaleStatusService {

    Boolean moveOrderStatus(Sale sale, Boolean isCanceling, String description);

}
