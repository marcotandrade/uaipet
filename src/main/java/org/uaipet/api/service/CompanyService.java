package org.uaipet.api.service;

import org.springframework.data.domain.Page;
import org.springframework.web.multipart.MultipartFile;
import org.uaipet.api.data.model.BusinessHour;
import org.uaipet.api.data.model.Company;
import org.uaipet.api.data.model.DeliveryRange;
import org.uaipet.api.data.model.Product;
import org.uaipet.api.data.model.User;
import org.uaipet.api.dto.request.CompanyOrderRequest;
import org.uaipet.api.dto.request.CompanyRequest;
import org.uaipet.api.dto.request.CompanyUserRequest;
import org.uaipet.api.dto.request.DashboardRequest;
import org.uaipet.api.dto.request.DeliveryRangeRequest;
import org.uaipet.api.dto.request.MoveOrderStatusRequest;
import org.uaipet.api.dto.request.ProductRequest;
import org.uaipet.api.dto.response.CompanyOrdersResponse;
import org.uaipet.api.dto.response.CompanyRegisterResponse;
import org.uaipet.api.dto.response.DashBoardResponse;
import org.uaipet.api.dto.response.DashboardSalesProjectionResponse;
import org.uaipet.api.dto.response.OrderDetailsResponse;

import java.io.IOException;
import java.util.List;

public interface CompanyService {

    CompanyRegisterResponse saveCompany(CompanyRequest company, List<MultipartFile> image) throws IOException;

    Boolean updateCompanyInfo(CompanyRequest company, List<MultipartFile> image);

    User addUser(CompanyUserRequest userRequest);

    Boolean deleteUser(Long id);

    Boolean addDeliveryRange(String cnpj, DeliveryRangeRequest deliveryRangeRequest);

    Boolean deleteAllDeliveryRanges(String cnpj);

    Company findCompanyInfo(String cnpj);

    Boolean closeForBusiness(CompanyRequest companyRequest);

    Boolean openForBusiness(CompanyRequest companyRequest);

    void scheduledOpenForBusiness();

    Company findOne( Long id );

    String updateBusinessHour(String cnpj, List<BusinessHour> businessHours);

    Boolean deleteAllBusinessHours(String cnpj);

    CompanyOrdersResponse findOrders(CompanyOrderRequest companyOrderRequest);

    Boolean moveOrderToNextStatus(MoveOrderStatusRequest request);

    Page<Product> productSearch(ProductRequest request);

    OrderDetailsResponse findOrderDetails(Long orderId);

    List<User> findUsers(String cnpj);

    DashBoardResponse dashboard(DashboardRequest request);


}
