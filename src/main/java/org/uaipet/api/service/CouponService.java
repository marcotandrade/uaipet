package org.uaipet.api.service;

import org.uaipet.api.data.model.Coupon;
import org.uaipet.api.dto.request.CouponRequest;
import org.uaipet.api.dto.response.CouponResponse;

public interface CouponService {

    Coupon createCoupon(CouponRequest couponRequest);

    CouponResponse couponIsValid(String code);

    void inactiveCoupon();

}
