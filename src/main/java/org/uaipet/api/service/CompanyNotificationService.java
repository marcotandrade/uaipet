package org.uaipet.api.service;

import org.uaipet.api.dto.response.CompanyNotificationResponse;
import org.uaipet.api.dto.response.ProcessedResponse;

public interface CompanyNotificationService {

    CompanyNotificationResponse getNotifications(String cnpj);

    ProcessedResponse setNotificationAsSeen(Long orderId);


}
