package org.uaipet.api.service;

import org.uaipet.api.data.model.ProductBrand;

import java.util.List;

public interface ProductBrandService {

    List< ProductBrand > findAll();

    ProductBrand findOne(Long id);

    ProductBrand save(ProductBrand productBrand);

    void update(Long id, ProductBrand productBrand);

}
