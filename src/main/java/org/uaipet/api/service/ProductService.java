package org.uaipet.api.service;

import org.springframework.web.multipart.MultipartFile;
import org.uaipet.api.data.model.Product;
import org.uaipet.api.dto.ProductDTO;
import org.uaipet.api.dto.ProductSearchDTO;
import org.uaipet.api.dto.request.ProductRequest;
import org.uaipet.api.dto.request.PurchaseRequest;

import java.util.List;


public interface ProductService {

    ProductSearchDTO search (ProductRequest request);

    Product findOne ( Long id );

    ProductDTO findOneById(Long id );

    Product save ( String cnpj, ProductRequest product, List<MultipartFile> image  );

    void update (  Long id, String cnpj, ProductRequest product, List<MultipartFile> image  );

    Boolean purchase(PurchaseRequest purchaseRequest);

    ProductSearchDTO productsSpotlight();

    Boolean deleteProduct(Long id);


}
