package org.uaipet.api.service;

import org.uaipet.api.dto.MailDTO;


public interface EmailService {

    void sendSimpleMessage(MailDTO mailDTO) ;

}
