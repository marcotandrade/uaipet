package org.uaipet.api.service.impl;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import org.uaipet.api.service.S3StorageService;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class S3StorageServiceImpl implements S3StorageService {

    private static final Logger log = LoggerFactory.getLogger(S3StorageService.class);


    @Value("${cloud.aws.s3.bucket.name}")
    private String s3bucket;

    private final AmazonS3 amazonS3;

    @Transactional(rollbackFor = Exception.class)
    @Override
    public ArrayList<String> uploadImage(List<MultipartFile> images, String imageName, String folderName) throws IOException {
        log.info(String.format("M: uploadImageFile - %s images - %s - %s", images.size(), imageName, folderName));
        ArrayList<String> urlList = new ArrayList<>();
        Integer count = 1;
        for (MultipartFile document : images) {
            ObjectMetadata metadata = new ObjectMetadata();
            metadata.setContentType(document.getContentType());
            metadata.setContentLength(document.getSize());

            InputStream inputStream = document.getInputStream();

            String date = LocalDateTime.now().toString();
            log.info(String.format("s3bucket %s - fileName %s ", s3bucket, document.getName() +"-"+ imageName));
            log.info(String.format("new fileName %s ", imageName + document.getName() + count.toString() + date));
            String key = formatS3Key(folderName,imageName + document.getName() + count.toString() + date);
            PutObjectRequest request = new PutObjectRequest(s3bucket, key, inputStream, metadata)
                    .withCannedAcl(CannedAccessControlList.PublicRead);

            amazonS3.putObject(request);
            URL url = amazonS3.getUrl(s3bucket, key);
            log.info(String.format("URL returned: %s", url.toExternalForm()));
            urlList.add(url.toExternalForm());
            count++;
        }
        return urlList;
    }

    @Override
    public Boolean deleteImage() {
        return null;
    }

    private String formatS3Key(String folderName, String name) {
        return  folderName + name.replaceAll("\\s+", "");
    }
}
