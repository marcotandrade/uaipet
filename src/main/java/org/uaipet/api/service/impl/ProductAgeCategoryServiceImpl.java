package org.uaipet.api.service.impl;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;
import org.uaipet.api.data.model.ProductAgeCategory;
import org.uaipet.api.data.repository.ProductAgeCategoryRepository;
import org.uaipet.api.service.ProductAgeCategoryService;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
public class ProductAgeCategoryServiceImpl implements ProductAgeCategoryService {

    private final ProductAgeCategoryRepository repository;

    @Autowired
    public ProductAgeCategoryServiceImpl(ProductAgeCategoryRepository repository){
        this.repository = repository;
    }


    @Override
    public List<ProductAgeCategory> findAll() {
        return this.repository.findAll();
    }

    @Override
    public ProductAgeCategory findOne(Long id) {

        Optional< ProductAgeCategory > product = this.repository.findById(id);

        if(!product.isPresent()){
            throw new EmptyResultDataAccessException(String.format("Categoria de idade não encontado com o id %s", id), 1);
        }

        return product.get();
    }

    @Override
    @Transactional( rollbackOn = Exception.class )
    public ProductAgeCategory save(ProductAgeCategory ageCategory) {

        return this.repository.save( ageCategory );
    }

    @Override
    @Transactional( rollbackOn = Exception.class )
    public void update(Long id, ProductAgeCategory ageCategory) {

        ProductAgeCategory ageCategorySave = this.findOne( id );
        //copia as propriedades do objeto 1 para o 2 ignorando o ID
        BeanUtils.copyProperties( ageCategory, ageCategorySave, "id" );
        this.repository.save( ageCategorySave );
    }
}
