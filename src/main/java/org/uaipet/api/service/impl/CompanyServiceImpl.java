package org.uaipet.api.service.impl;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpHeaders;
import org.springframework.scheduling.annotation.Async;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import org.uaipet.api.config.security.JWTUtil;
import org.uaipet.api.config.security.UserSS;
import org.uaipet.api.data.enums.UserRoleEnum;
import org.uaipet.api.data.enums.UserTypeEnum;
import org.uaipet.api.data.model.Address;
import org.uaipet.api.data.model.BusinessHour;
import org.uaipet.api.data.model.Company;
import org.uaipet.api.data.model.Product;
import org.uaipet.api.data.model.Sale;
import org.uaipet.api.data.model.SaleItems;
import org.uaipet.api.data.model.SaleStatus;
import org.uaipet.api.data.model.User;
import org.uaipet.api.data.repository.BusinessHourRepository;
import org.uaipet.api.data.repository.CompanyRepository;
import org.uaipet.api.data.repository.DeliveryRangeRepository;
import org.uaipet.api.data.repository.ProductRepository;
import org.uaipet.api.data.repository.SaleItemsRepository;
import org.uaipet.api.data.repository.SaleRepository;
import org.uaipet.api.data.repository.SaleStatusRepository;
import org.uaipet.api.data.repository.SearchIndexRepository;
import org.uaipet.api.data.repository.UserRepository;
import org.uaipet.api.dto.MailDTO;
import org.uaipet.api.dto.OrderProductsDTO;
import org.uaipet.api.dto.request.CompanyOrderRequest;
import org.uaipet.api.dto.request.CompanyRequest;
import org.uaipet.api.dto.request.CompanyUserRequest;
import org.uaipet.api.dto.request.DashboardRequest;
import org.uaipet.api.dto.request.DeliveryRangeRequest;
import org.uaipet.api.dto.request.MoveOrderStatusRequest;
import org.uaipet.api.dto.request.ProductRequest;
import org.uaipet.api.dto.request.PushNotificationRequest;
import org.uaipet.api.dto.request.RoleRequest;
import org.uaipet.api.dto.response.CompanyOrdersResponse;
import org.uaipet.api.dto.response.CompanyRegisterResponse;
import org.uaipet.api.dto.response.DashBoardResponse;
import org.uaipet.api.dto.response.DashboardProductsProjectionResponse;
import org.uaipet.api.dto.response.DashboardSalesProjectionResponse;
import org.uaipet.api.dto.response.OrderDetailsResponse;
import org.uaipet.api.dto.response.OrderResponse;
import org.uaipet.api.service.CompanyService;
import org.uaipet.api.service.EmailService;
import org.uaipet.api.service.PushNotificationService;
import org.uaipet.api.service.S3StorageService;
import org.uaipet.api.service.SaleStatusService;
import org.uaipet.api.service.SearchIndexService;
import org.uaipet.api.service.UserService;
import org.uaipet.api.service.exception.CompanyAlreadyExistsException;
import org.uaipet.api.service.exception.CompanyNotFoundException;
import org.uaipet.api.service.exception.EmailAlreadyExistsException;
import org.uaipet.api.service.exception.GenericException;
import org.uaipet.api.service.exception.SaleNotFoundException;
import org.uaipet.api.service.exception.UserNotFoundForCompanyException;
import org.uaipet.api.service.exception.WrongAuthorizationTokenFormatException;
import org.uaipet.api.utils.Builder;
import org.uaipet.api.utils.RandomGenerator;

import java.io.IOException;
import java.math.BigDecimal;
import java.time.Month;
import java.util.*;
import java.time.DayOfWeek;
import java.time.LocalDateTime;
import java.util.stream.Collectors;

import static org.uaipet.api.data.repository.ProductSpecifications.companyIn;
import static org.uaipet.api.data.repository.ProductSpecifications.nameLike;
import static org.uaipet.api.data.repository.ProductSpecifications.notDeleted;


@RequiredArgsConstructor
@Service
public class CompanyServiceImpl implements CompanyService {

    private static final Logger log = LoggerFactory.getLogger(CompanyServiceImpl.class);
    private static final String UAI_PET_ORG_EMAIL = "noreply@uaipetapp.com.br";
    public static final String EMPRESA_NAO_ENCONTRADA = "Empresa não encontrada";

    private final CompanyRepository repository;
    private final SaleRepository saleRepository;
    private final SaleItemsRepository saleItemsRepository;
    private final SaleStatusRepository saleStatusRepository;
    private final ProductRepository productRepository;
    private final S3StorageService s3StorageService;
    private final EmailService emailService;
    private final SearchIndexService searchIndexService;
    private final Builder builder;
    private final UserRepository userRepository;
    private final DeliveryRangeRepository deliveryRangeRepository;
    private final BusinessHourRepository businessHourRepository;
    private final SearchIndexRepository searchIndexRepository;
    private final SaleStatusService saleStatusService;
    private final UserService userService;
    private final BCryptPasswordEncoder encoder;
    private final JWTUtil jwtUtil;
    private final PushNotificationService pushNotificationService;


    @Override
    public CompanyRegisterResponse saveCompany(@NonNull CompanyRequest companyRequest, List<MultipartFile> image) throws IOException {
        try {
            log.info(String.format("saveCompany - %s", companyRequest.toString()));
            companyRequest.getUser().setRole(RoleRequest.builder().name(UserRoleEnum.COMPANY_ADMIN).build());
            companyRequest.getUser().setType(UserTypeEnum.COMPANY);
            companyRequest.getUser().setValidationCode(RandomGenerator.generateRandomIntegerCode());
            companyRequest.getUser().setPassword(encoder.encode(companyRequest.getUser().getPassword()));
            Company company = builder.buildCompany(companyRequest);
            ArrayList<String> images = s3StorageService.uploadImage(image, company.getSocialContractName() + " - " + company.getCnpj(), "company-images/");
            String semicolonSeparatedImg = StringUtils.join(images, ";");
            company.setImage(semicolonSeparatedImg);
            Company persistedCompany = repository.save(company);

            log.info("sending email");
            Map<String, Object> companyInfo = new HashMap<>();
            companyInfo.put("cnpj", persistedCompany.getCnpj());
            companyInfo.put("name", persistedCompany.getName());
            companyInfo.put("tokenValue", persistedCompany.getUserList().get(0).getValidationCode());

            MailDTO mailInfo = MailDTO.builder()
                    .mailTo(persistedCompany.getUserList().get(0).getEmail())
                    .mailFrom(UAI_PET_ORG_EMAIL)
                    .name(persistedCompany.getName())
                    .props(companyInfo)
                    .templateName("company-registration-email")
                    .subject("Loja cadastrada com sucesso")
                    .build();

            emailService.sendSimpleMessage(mailInfo);
            List<User> users = userRepository.findByCompany(persistedCompany.getCnpj());
            HttpHeaders headers = generateToken(users.get(0));

            return CompanyRegisterResponse.builder()
                    .company(persistedCompany)
                    .headers(headers)
                    .build();
        } catch (DataIntegrityViolationException ex) {
            throw new CompanyAlreadyExistsException();
        }
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public Boolean updateCompanyInfo(CompanyRequest companyRequest, List<MultipartFile> image) {
        log.info(String.format("updateCompanyInfo - %s", companyRequest.toString()));
        Boolean response;
        try {
            Company company = validateRequest();
            updateInfo(companyRequest, image, company);
            if(companyRequest.getAddress() != null){
                company.setAddress(builder.buildAddress(companyRequest.getAddress(), true));
                searchIndexRepository.deleteByCompany(company);
            }
            Company persistedCompany = repository.save(company);
            if(companyRequest.getAddress() != null) {
                searchIndexService.save(null, persistedCompany.getAddress(), company);
            }
            response = true;
        } catch (WrongAuthorizationTokenFormatException | CompanyNotFoundException ex) {
            throw ex;
        } catch (Exception ex) {
            log.error(String.format("Error updating company - %s", ex.getMessage()));
            throw new GenericException();
        }
        return response;
    }

    @Transactional
    public void updateAddress(CompanyRequest companyRequest, Company company) {
        if(companyRequest.getAddress() != null){
            company.setAddress(builder.buildAddress(companyRequest.getAddress(), true));
            searchIndexRepository.deleteByCompany(company);
        }
    }

    private void updateInfo(CompanyRequest companyRequest, List<MultipartFile> image, Company company) throws IOException {
        if(companyRequest.getName() != null){
            company.setName(companyRequest.getName());
        }
        if(companyRequest.getPhone() != null){
            company.setPhone(companyRequest.getPhone());
        }
        if(companyRequest.getSocialContractName() != null){
            company.setSocialContractName(companyRequest.getSocialContractName());
        }
        if(!image.isEmpty()){
            ArrayList<String> images = s3StorageService.uploadImage(image, company.getSocialContractName() + "-" + company.getCnpj(), "company-images/");
            String semicolonSeparatedImg = StringUtils.join(images, ";");
            company.setImage(semicolonSeparatedImg);
        }
    }

    private Company validateRequest() {
        UserSS userToken = userService.authenticated();
        if (null == userToken.getCnpj()) {
            throw new WrongAuthorizationTokenFormatException();
        }
        Company company = repository.findOneByCnpj(userToken.getCnpj());
        if (null == company) {
            throw new CompanyNotFoundException();
        }
        return company;
    }

    @Override
    public User addUser(CompanyUserRequest userRequest) {
        try {
            log.info(String.format("addUser - %s", userRequest.toString()));
            Company company = repository.findOneByCnpj(userRequest.getCompanyCnpj());
            if (company == null) {
                throw new CompanyNotFoundException("Empresa não encontrada para o cnpj informado");
            }
            userService.requestUserIsAssociatedWithCompany(company.getId());
            User user = builder.buildCompanyUser(userRequest);
            user.setCompany(company);
            user.setPassword(encoder.encode(userRequest.getPassword()));
            return userRepository.save(user);
        } catch (DataIntegrityViolationException ex){
            log.error(ex.getMessage());
            throw new EmailAlreadyExistsException();
        } catch (Exception ex){
            throw new GenericException();
        }

    }

    @Transactional
    @Override
    public Boolean deleteUser(Long id) {
        try{
            log.info(String.format("deleteUser -  %s", id.toString()));
            userRepository.deleteById(id);
            return true;
        }catch (Exception ex){
            log.error(String.format("Error deleting user - %s", ex.getMessage()));
            return false;
        }
    }

    @Override
    public Boolean addDeliveryRange(String cnpj, DeliveryRangeRequest deliveryRangeRequest) {
        log.info(String.format("addDeliveryRange -  %s", deliveryRangeRequest.toString()));

        Company company = repository.findOneByCnpj(cnpj);
        userService.requestUserIsAssociatedWithCompany(company.getId());
        if (Objects.nonNull(company)) {
            deliveryRangeRequest.getDeliveryRangeList().forEach(x -> {
                x.setCompany(company);
            });
            company.setDeliveryRanges(deliveryRangeRequest.getDeliveryRangeList());
            company.setMinimumOrderAmount(StringUtils.isNotBlank(deliveryRangeRequest.getMinimumAmountOrder())
                    ? deliveryRangeRequest.getMinimumAmountOrder()
                    : null);
            company.setAmountFreeFreight(StringUtils.isNotBlank(deliveryRangeRequest.getAmountFreeFreight())
                    ? deliveryRangeRequest.getAmountFreeFreight()
                    : null);
            repository.save(company);
            searchIndexService.save(null, company.getAddress(), company);
            return true;
        }
        throw new CompanyNotFoundException(EMPRESA_NAO_ENCONTRADA);
    }

    @Override
    public Boolean deleteAllDeliveryRanges(String cnpj) {
        try {
            log.info(String.format("deleteAllDeliveryRanges -  %s", cnpj));
            Company company = repository.findOneByCnpj(cnpj);
            if (Objects.nonNull(company)) {
                deliveryRangeRepository.deleteInBatch(company.getDeliveryRanges());
                return true;
            } else {
                throw new CompanyNotFoundException();
            }
        } catch (Exception ex) {
            log.error(String.format("error trying to delete delivery ranges - %s", ex.getMessage()));
            throw new GenericException();
        }
    }

    @Override
    public Company findCompanyInfo(String cnpj) {
        log.info(String.format("findCompanyInfo -  %s", cnpj));
        Company company = repository.findOneByCnpj(cnpj);
        userService.requestUserIsAssociatedWithCompany(company.getId());
        if (Objects.nonNull(company)) {
            return company;
        } else {
            throw new CompanyNotFoundException("Empresa não encontrada para o cnpj informado");
        }
    }

    @Override
    public Company findOne(Long id) {
        Optional<Company> merchant = this.repository.findById(id);
        if (!merchant.isPresent()) {
            throw new EmptyResultDataAccessException(String.format("Empresa não encontada com o id %s", id), 1);
        }
        userService.requestUserIsAssociatedWithCompany(merchant.get().getId());
        return merchant.get();
    }

    @Override
    public Boolean closeForBusiness(CompanyRequest companyRequest) {
        log.info(String.format("closeForBusiness -  %s", companyRequest.toString()));
        Company company = repository.findOneByCnpj(companyRequest.getCnpj());
        userService.requestUserIsAssociatedWithCompany(company.getId());
        if (Objects.nonNull(company)) {
            DayOfWeek day = LocalDateTime.now().getDayOfWeek();
            company.getBusinessHours().forEach(bh -> {
                if (bh.getDayOfWeek().equalsIgnoreCase(day.toString())) {
                    bh.setActive(false);
                }
            });
            repository.save(company);
            return true;
        }
        throw new CompanyNotFoundException(EMPRESA_NAO_ENCONTRADA);
    }

    @Override
    public Boolean openForBusiness(CompanyRequest companyRequest) {
        log.info(String.format("openForBusiness -  %s", companyRequest.toString()));
        Company company = repository.findOneByCnpj(companyRequest.getCnpj());
        userService.requestUserIsAssociatedWithCompany(company.getId());
        if (Objects.nonNull(company)) {
            DayOfWeek day = LocalDateTime.now().getDayOfWeek();
            company.getBusinessHours().forEach(bh -> {
                if (bh.getDayOfWeek().equalsIgnoreCase(day.toString())) {
                    bh.setActive(true);
                }
            });
            repository.save(company);
            return true;
        }
        throw new CompanyNotFoundException(EMPRESA_NAO_ENCONTRADA);
    }

    @Override
    public String updateBusinessHour(String cnpj, List<BusinessHour> businessHours) {
        log.info(String.format("updateBusinessHour - cnpj: %s; businessHours %s", cnpj, businessHours.toString()));
        try {
            Company company = repository.findOneByCnpj(cnpj);
            userService.requestUserIsAssociatedWithCompany(company.getId());
            if (Objects.nonNull(company)) {
                businessHours.forEach(bh -> {
                    bh.setCompany(company);
                });
                company.setBusinessHours(businessHours);
                repository.save(company);
                return "business hours updated";
            }
            throw new CompanyNotFoundException(EMPRESA_NAO_ENCONTRADA);
        } catch (Exception ex) {
            log.error(String.format("Error trying to save business hours - %s", ex.getMessage()));
            throw new GenericException();
        }
    }

    @Override
    public Boolean deleteAllBusinessHours(String cnpj) {
        log.info(String.format("deleteAllBusinessHours - cnpj: %s", cnpj));
        try {
            Company company = repository.findOneByCnpj(cnpj);
            if (Objects.nonNull(company)) {
                businessHourRepository.deleteInBatch(company.getBusinessHours()
                        .stream().collect(Collectors.toList()));
                return true;
            } else {
                throw new CompanyNotFoundException();
            }
        } catch (Exception ex) {
            log.error(String.format("Error trying to delete business hours - %s", ex.getMessage()));
            throw new GenericException();
        }
    }

    @Override
    public void scheduledOpenForBusiness() {
        log.info("scheduledOpenForBusiness");
        List<BusinessHour> businessHours = businessHourRepository.findByActiveFalse();
        businessHours.forEach(bh -> {
            bh.setActive(true);
        });
        businessHourRepository.saveAll(businessHours);
    }

    @Override
    public CompanyOrdersResponse findOrders(CompanyOrderRequest companyOrderRequest) {
        log.info(String.format("findOrders - %s", companyOrderRequest.toString()));
        Pageable pageable = getPageable(companyOrderRequest.getDirection(), companyOrderRequest.getSortBy(),
                companyOrderRequest.getPageNumber(), companyOrderRequest.getPageSize());
        CompanyOrdersResponse orderList = CompanyOrdersResponse.builder().build();
        List<OrderResponse> orderResponses = new ArrayList<>();
        try {
            Page<Sale> sales = getSales(companyOrderRequest, pageable);
            createResponse(orderList, orderResponses, sales);
            return orderList;
        } catch (Exception ex) {
            log.error(ex.getMessage());
            throw new GenericException();
        }
    }

    private Page<Sale> getSales(CompanyOrderRequest companyOrderRequest, Pageable pageable) {
        long startTime = System.currentTimeMillis();
        Company company = repository.findOneByCnpj(companyOrderRequest.getCnpj());
        if(company == null){
            throw new CompanyNotFoundException();
        }
        userService.requestUserIsAssociatedWithCompany(company.getId());
        List<String> statusList = new ArrayList<>();
        companyOrderRequest.getStatus().forEach(st -> statusList.add(st.getDescription().toUpperCase()));
        Page<Sale> sales = saleRepository.findAllOrders(company.getId(),
                statusList, pageable);
        log.debug(String.format("findOrders finalized findAll query in %s", System.currentTimeMillis() - startTime));
        return sales;
    }

    private void createResponse(CompanyOrdersResponse orderList, List<OrderResponse> orderResponses, Page<Sale> sales) {
        long startTimeResponse = System.currentTimeMillis();
        sales.forEach(sl -> {
            Long maxId = sl.getSaleStatusList().stream()
                    .map(st -> st.getId())
                    .max(Long::compareTo).get();

            Optional<SaleStatus> saleStatus = saleStatusRepository.findById(maxId);

            String totalAmount = new BigDecimal(sl.getAmount()).subtract(new BigDecimal(sl.getCouponAmount()))
                    .compareTo(new BigDecimal("0.0")) < 0 ? new BigDecimal("0.0").toString() :
                    new BigDecimal(sl.getAmount()).subtract(new BigDecimal(sl.getCouponAmount())).toString();

            orderResponses.add(OrderResponse.builder()
                    .amount(sl.getAmount())
                    .totalAmount(totalAmount)
                    .couponAmount(sl.getCouponAmount())
                    .date(sl.getCreatedDate().toString())
                    .storeName(sl.getCompany().getName())
                    .saleId(sl.getId())
                    .orderStatus(saleStatus.orElse(null).getStatus().getDescription())
                    .customerPhone(sl.getUser().getPhone())
                    .customerName(sl.getUser().getName())
                    .freightValue(sl.getFreightAmount())
                    .paymentMethod(convertToUserReadablePayMethod(sl.getPaymentMethod().getPaymentMethod().getDescription()))
                    .changeFor(convertToUserReadableChange(sl.getPaymentMethod().getChange()))
                    .build());

        });

        orderList.setOrderResponse(orderResponses);
        orderList.setEmpty(sales.isEmpty());
        orderList.setFirst(sales.isFirst());
        orderList.setLast(sales.isLast());
        orderList.setSize(sales.getSize());
        orderList.setTotalPages(sales.getTotalPages());
        orderList.setTotalElements(sales.getTotalElements());
        log.debug(String.format("findOrders finalized object response in %s", System.currentTimeMillis() - startTimeResponse));
    }


    @Override
    public Boolean moveOrderToNextStatus(MoveOrderStatusRequest moveOrderStatusRequest) {
        log.info(String.format("moveOrderToNextStatus - moveOrderStatusRequest: %s", moveOrderStatusRequest.toString()));
        Company company = repository.findOneByCnpj(moveOrderStatusRequest.getCnpj());
        userService.requestUserIsAssociatedWithCompany(company.getId());
        Optional<User> user = company.getUserList().stream()
                .filter(us -> us.getEmail().equalsIgnoreCase(moveOrderStatusRequest.getUserEmail()))
                .findAny();
        user.ifPresentOrElse(us -> {
            Optional<Sale> sale = saleRepository.findById(moveOrderStatusRequest.getSaleId());
            sale.ifPresentOrElse(sl1 -> {
                        saleStatusService.moveOrderStatus(sl1, moveOrderStatusRequest.getIsCanceling(),
                                moveOrderStatusRequest.getCancelingDescription());
                        sendPushUpdate(moveOrderStatusRequest, sl1);
                    },
                    () -> sale.orElseThrow(SaleNotFoundException::new));
        }, () -> user.orElseThrow(UserNotFoundForCompanyException::new));

        return true;
    }

    @Async("asyncTaskExecutor")
    public void sendPushUpdate(MoveOrderStatusRequest moveOrderStatusRequest, Sale sl1) {
        log.info("sendPushUpdate");
        String title;
        String text;
        if(moveOrderStatusRequest.getIsCanceling()){
            title = "Pedido Cancelado";
            text = "Desculpe, seu pedido foi cancelado pois: " + moveOrderStatusRequest.getCancelingDescription();
        } else {
            title = "Pedido Atualizado";
            text = "O status da sua compra foi atualizado";
        }

        List<String> expoList = new ArrayList<>();
        expoList.add(sl1.getUser().getExpoToken());
        PushNotificationRequest pushNotificationRequest = PushNotificationRequest.builder()
                .allUsers(false)
                .expoToken(expoList)
                .title(title)
                .text(text)
                .build();

        pushNotificationService.sendNotification(pushNotificationRequest, false);
    }

    @Override
    public Page<Product> productSearch(ProductRequest request) {
        Pageable pageable = getPageable(request.getDirection(), request.getSortBy(), request.getPageNumber(), request.getPageSize());
        Company company = repository.findOneByCnpj(request.getCnpj());
        if (company == null) {
            throw new CompanyNotFoundException("Empresa não encontrada para o cnpj informado");
        }
        try {
            ArrayList<Company> compList = new ArrayList<>();
            compList.add(company);
            Page<Product> response = productRepository.findAll(Specification.where(
                    nameLike(request.getName()).and(companyIn(compList)).and(notDeleted())), pageable);
            response.forEach(pr -> pr.setCompany(null));
            return response;
        } catch (Exception ex) {
            log.error(ex.getMessage());
            throw new GenericException();
        }
    }

    @Override
    public OrderDetailsResponse findOrderDetails(Long saleId) {
        try {
            Optional<Sale> sale = saleRepository.findById(saleId);
            List<SaleItems> saleItems = saleItemsRepository.findBySale(sale.orElseThrow(SaleNotFoundException::new));
            List<OrderProductsDTO> productDetails = new ArrayList<>();
            saleItems.forEach(sli -> {
                productDetails.add(OrderProductsDTO.builder()
                        .productName(sli.getProductName())
                        .productAmount(sli.getAmount())
                        .productValue(sli.getProduct().getPrice())
                        .productQuantity(sli.getProductQtt().toString())
                        .productMeasurement(sli.getProduct().getMeasurementValue() + " " + sli.getProduct().getProductMeasurementCategory().getName())
                        .productImage(sli.getProduct().getImage())
                        .build());
            });

            Long maxId = sale.get().getSaleStatusList().stream()
                    .map(st -> st.getId())
                    .max(Long::compareTo).get();

            Optional<SaleStatus> saleStatus = saleStatusRepository.findById(maxId);

            Address address = sale.get().getAddress();
            User user = sale.get().getUser();

            String totalAmount = new BigDecimal(sale.get().getAmount()).subtract(new BigDecimal(sale.get().getCouponAmount()))
                    .compareTo(new BigDecimal("0.0")) < 0 ? new BigDecimal("0.0").toString() :
                    new BigDecimal(sale.get().getAmount()).subtract(new BigDecimal(sale.get().getCouponAmount())).toString();

            return OrderDetailsResponse.builder()
                    .saleId(sale.get().getId())
                    .couponAmount(sale.get().getCouponAmount())
                    .orderStatus(saleStatus.orElse(null).getStatus().getDescription())
                    .productsFromOrder(productDetails)
                    .customerName(user.getName())
                    .customerPhone(user.getPhone())
                    .customerCPF(user.getCpf())
                    .street(address.getStreet())
                    .number(address.getNumber())
                    .district(address.getDistrict())
                    .complement(address.getComplement())
                    .freightAmount(sale.get().getFreightAmount())
                    .totalAmount(totalAmount)
                    .paymentMethod(convertToUserReadablePayMethod(sale.get().getPaymentMethod().getPaymentMethod().getDescription()))
                    .changeFor(convertToUserReadableChange(sale.get().getPaymentMethod().getChange()))
                    .build();
        } catch (SaleNotFoundException ex){
            throw ex;
        } catch (Exception ex){
            log.error(String.format("Error geting order details - %s", ex.getMessage()));
            throw new GenericException();
        }
    }


    @Override
    public List<User> findUsers(String cnpj) {
        log.info(String.format("findUsers - cnpj: %s", cnpj));
        UserSS userToken = userService.authenticated();
        List<User> response = userService.findUsersByCompanyCnpj(cnpj);
        response.removeIf(us -> us.getEmail().equalsIgnoreCase(userToken.getUsername()));
        response.forEach(resp -> {
            resp.setCompany(null);
        });
        return response;
    }

    @Override
    public DashBoardResponse dashboard(DashboardRequest request) {
        try {
            Month month = extractMonth(request.getMonth().toLowerCase());
            int year = LocalDateTime.now().getYear();
            LocalDateTime.now().getMonth();
            int initialDay = 1;
            int finalDay = month.maxLength();
            LocalDateTime initialDate = LocalDateTime.of(year, month, initialDay, 0, 0, 0);
            LocalDateTime finalDate = LocalDateTime.of(year, month, finalDay, 23, 59, 59);

            DashboardSalesProjectionResponse dashboardSalesResponse = saleRepository.getSalesDashboard(request.getCnpj(), initialDate, finalDate);
            List<DashboardProductsProjectionResponse> dashboardProductsResponses = saleRepository.getProductsDashboard(request.getCnpj(), initialDate, finalDate);
            return DashBoardResponse.builder()
                    .products(dashboardProductsResponses)
                    .values(dashboardSalesResponse)
                    .build();
        } catch (Exception ex){
            log.error(String.format("Error getting dashboard info - %s", ex.getMessage()));
            throw new GenericException();
        }
    }

    private Month extractMonth(String month) {
        switch (month) {
            case "janeiro":
                return Month.JANUARY;
            case "fevereiro":
                return Month.FEBRUARY;
            case "março":
                return Month.MARCH;
            case "abril":
                return Month.APRIL;
            case "maio":
                return Month.MAY;
            case "junho":
                return Month.JUNE;
            case "julho":
                return Month.JULY;
            case "agosto":
                return Month.AUGUST;
            case "setembro":
                return Month.SEPTEMBER;
            case "outubro":
                return Month.OCTOBER;
            case "novembro":
                return Month.NOVEMBER;
            case "dezembro":
                return Month.DECEMBER;
        }
        return null;
    }

    private Pageable getPageable(String direction, String sortBy, int pageNumber, int pageSize) {
        Pageable pageable = null;
        if (sortBy != null && direction != null &&
                (direction.equalsIgnoreCase("ASC") || direction.equalsIgnoreCase("DESC"))) {
            pageable = PageRequest.of(pageNumber, pageSize,
                    Sort.by(Sort.Direction.valueOf(direction), sortBy.toLowerCase()));
        } else if (sortBy != null) {
            pageable = PageRequest.of(pageNumber, pageSize, Sort.by(sortBy.toLowerCase()));
        } else {
            pageable = PageRequest.of(pageNumber, pageSize);
        }
        return pageable;
    }


    private String convertToUserReadablePayMethod(String description) {
        if (description.equalsIgnoreCase("card")) {
            return "Cartão de Credito";
        } else {
            return "Dinheiro";
        }
    }


    private String convertToUserReadableChange(String change) {
        if (change != null && !change.isBlank()) {
            return change;
        } else {
            return null;
        }
    }

    private HttpHeaders generateToken(User persistedUser) {
        ArrayList<String> strImg = new ArrayList<>();
        if(persistedUser.getImage() != null) {
            for (String s : persistedUser.getImage().split(";")) {
                strImg.add(s);
            }
        }
        List<String> roleList = new ArrayList<>();
        roleList.add(persistedUser.getUserRoleList().get(0).getRole().getName());
        Collection<? extends GrantedAuthority> authorities = (Collection<GrantedAuthority>)(Collection<?>) roleList;
        String cnpj = persistedUser.getCompany() != null ? persistedUser.getCompany().getCnpj() : null;
        String token = jwtUtil.generateToken(persistedUser.getEmail(), authorities,
                persistedUser.getId(), persistedUser.getName(), strImg, cnpj, persistedUser.getStatus());
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization","Bearer " + token);
        headers.add("access-control-expose-headers", "Authorization");
        return headers;
    }
}
