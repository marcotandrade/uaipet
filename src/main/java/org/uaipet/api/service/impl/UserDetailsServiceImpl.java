package org.uaipet.api.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.uaipet.api.config.security.UserSS;
import org.uaipet.api.data.model.User;
import org.uaipet.api.data.model.UserRole;
import org.uaipet.api.data.repository.UserRepository;
import org.uaipet.api.data.repository.UserRoleRepository;
import org.uaipet.api.service.exception.EmailNotValidatedException;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class UserDetailsServiceImpl implements UserDetailsService {

    private final UserRepository userRepository;
    private final UserRoleRepository userRoleRepository;

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        User user = userRepository.findByEmail(email);
        if (user == null) {
            throw new UsernameNotFoundException(email);
        }

        if(!user.getStatus().equalsIgnoreCase("valid")){
            throw new EmailNotValidatedException("Email not validated by user");
        }

        ArrayList<String> strImg = new ArrayList<>();
        if(user.getImage() != null) {
            for (String s : user.getImage().split(";")) {
                strImg.add(s);
            }
        }

        List<UserRole> roleEntityList = userRoleRepository.findByUser(user);
        List<GrantedAuthority> authorities = roleEntityList.stream()
                .map(role -> new SimpleGrantedAuthority(role.getRole().getName()))
                .collect(Collectors.toList());
        String cnpj = null;
        if(user.getCompany() != null){
            cnpj = user.getCompany().getCnpj();
        }

        return new UserSS(user.getId(), user.getName(),  user.getEmail(), user.getPassword(), strImg, authorities,
                user.getId(), cnpj, user.getStatus());
    }
}
