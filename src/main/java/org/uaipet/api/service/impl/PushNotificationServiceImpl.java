package org.uaipet.api.service.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Slice;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.client.*;
import org.uaipet.api.config.security.UserSS;
import org.uaipet.api.data.model.User;
import org.uaipet.api.data.repository.UserRepository;
import org.uaipet.api.dto.ExpoPushDTO;
import org.uaipet.api.dto.request.PushNotificationRequest;
import org.uaipet.api.service.PushNotificationService;
import org.uaipet.api.service.UserService;
import org.uaipet.api.service.exception.AuthorizationException;
import org.uaipet.api.service.exception.GenericException;
import org.uaipet.api.service.exception.UserNotFoundException;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class PushNotificationServiceImpl implements PushNotificationService {

    private static final Logger LOGGER = LoggerFactory.getLogger(PushNotificationServiceImpl.class);

    @Value("${coupon.authorized.user}")
    private String authorizedUser;

    @Value("${expo.push.api.host.baseurl}")
    private String expoPushUrl;

    private final String URI_SEND_PUSH = "/send";

    private final UserService userService;
    private final UserRepository userRepository;
    private final RestTemplate restTemplate;


    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    public PushNotificationServiceImpl(RestTemplateBuilder builder, UserRepository userRepository, UserService userService) {
        this.restTemplate = builder.build();
        this.userRepository = userRepository;
        this.userService =  userService;
    }

    @Override
    public void sendNotification(PushNotificationRequest pushNotificationRequest, Boolean isToValidate) {
        LOGGER.info(String.format("sendNotification - PushNotificationRequest: %s", pushNotificationRequest.toString()));


        UserSS tokenUser = userService.authenticated();
        if (isToValidate && tokenUser != null && !authorizedUser.contains(tokenUser.getUsername())) {
            LOGGER.info("user unauthorized to send push");
            throw new AuthorizationException(null);
        }

        if (!pushNotificationRequest.getAllUsers()) {
            if (pushNotificationRequest.getExpoToken().isEmpty()) {
                LOGGER.info("ExpoToken list cannot be empty");
                throw new GenericException("Error trying to send push notification");
            }
            List<ExpoPushDTO> expoPushDTOS = new ArrayList<>();

            for (String expoTk : pushNotificationRequest.getExpoToken()) {
                User user = userRepository.findFirstByExpoToken(expoTk);
                if(null == user ){
                    throw new UserNotFoundException();
                }
                ExpoPushDTO expoPushDTO = ExpoPushDTO.builder().to(new ArrayList<>()).body(null).title(null).build();
                expoPushDTO.getTo().add(expoTk);
                if(user.getName()!= null) {
                    expoPushDTO.setBody(pushNotificationRequest.getText().replace("%name%", user.getName()));
                } else {
                    expoPushDTO.setBody(pushNotificationRequest.getText());
                }
                expoPushDTO.setTitle(pushNotificationRequest.getTitle());
                expoPushDTO.setSound("default");
                expoPushDTO.setBadge(1);
                expoPushDTOS.add(expoPushDTO);
            }
            sendPushMessage(expoPushDTOS);
        } else {
            int page = 0;
            int pageSize = 2;
            boolean hasNext;
            do {
                Slice<User> users = null;
                if(pushNotificationRequest.getAllUsers() && pushNotificationRequest.getAllUsersRegistered()){
                    users = userRepository.findByExpoTokenNotNullAndEmailNotNull(PageRequest.of(page, pageSize));
                } else if(pushNotificationRequest.getAllUsers() && !pushNotificationRequest.getAllUsersRegistered()) {
                    users = userRepository.findByExpoTokenNotNullAndEmailIsNull(PageRequest.of(page, pageSize));
                }

                List<ExpoPushDTO> expoPushDTOS = new ArrayList<>();

                for (User user : users) {
                    ExpoPushDTO expoPushDTO = ExpoPushDTO.builder().to(new ArrayList<>()).build();
                    expoPushDTO.getTo().add(user.getExpoToken());
                    if(user.getName()!= null && pushNotificationRequest.getText().contains("%name%")) {
                        expoPushDTO.setBody(pushNotificationRequest.getText().replace("%name%", user.getName()));
                    } else {
                        expoPushDTO.setBody(pushNotificationRequest.getText());
                    }
                    expoPushDTO.setTitle(pushNotificationRequest.getTitle());
                    expoPushDTO.setSound("default");
                    expoPushDTO.setBadge(1);
                    expoPushDTOS.add(expoPushDTO);
                }
                try {
                    sendPushMessage(expoPushDTOS);
                } catch (Exception ex){
                    LOGGER.info("Error executin sendPushMessage");
                    LOGGER.info(ex.getMessage());
                }
                page++;
                hasNext = users.hasNext();
            } while (hasNext);
        }
    }


    private void sendPushMessage(List<ExpoPushDTO> expoPushDTOS) {
        LOGGER.info(String.format("sendPushMessage - expoPushDTOS: %s", expoPushDTOS.toString()));
        try {
            String jsonRequest = objectMapper.writeValueAsString(expoPushDTOS);
            LOGGER.info(String.format("jsonRequest: %s", jsonRequest));
            Object sentPushes = restTemplate.postForEntity(expoPushUrl + URI_SEND_PUSH, expoPushDTOS, ExpoPushDTO.class);
            LOGGER.info(String.format("sentPushes: %s", sentPushes.toString()));
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage());
            LOGGER.error("Erro ao enviar push notification");
        }


    }


}
