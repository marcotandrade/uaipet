package org.uaipet.api.service.impl;

import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.uaipet.api.config.security.UserSS;
import org.uaipet.api.data.enums.CouponTypeEnum;
import org.uaipet.api.data.model.Coupon;
import org.uaipet.api.data.model.User;
import org.uaipet.api.data.repository.CouponRepository;
import org.uaipet.api.data.repository.UserRepository;
import org.uaipet.api.dto.request.CouponRequest;
import org.uaipet.api.dto.response.CouponResponse;
import org.uaipet.api.service.CouponService;
import org.uaipet.api.service.UserService;
import org.uaipet.api.service.exception.AuthorizationException;
import org.uaipet.api.service.exception.CouponNotFoundException;
import org.uaipet.api.utils.RandomGenerator;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@RequiredArgsConstructor
@Service
public class CouponServiceImpl implements CouponService {

    private static final Logger log = LoggerFactory.getLogger(CouponServiceImpl.class);

    private final CouponRepository couponRepository;
    private final UserRepository userRepository;

    @Value("${coupon.validity}")
    private long couponValidityDays;

    @Value("${coupon.authorized.user}")
    private String authorizedUser;

    private final UserService userService;

    @Transactional(rollbackFor = Exception.class)
    @Override
    public Coupon createCoupon(CouponRequest couponRequest) {
        log.info("createCoupon");

        UserSS tokenUser = userService.authenticated();
        if (!authorizedUser.contains(tokenUser.getUsername())) {
            throw new AuthorizationException(null);
        }
        Coupon coupon;
        List<Coupon> couponList = new ArrayList<>();

        if (couponRequest.getAllUsers()) {
            List<User> users = userRepository.findAllByEmailNotNull();

            users.forEach(us -> {
                couponList.add(Coupon.builder()
                        .active(true)
                        .code(RandomGenerator.generateRandomIntegerCode())
                        .expirationDate(LocalDateTime.now().plusDays(couponRequest.getValidityDays()))
                        .value(couponRequest.getCouponValue())
                        .rule(couponRequest.getRule())
                        .description(couponRequest.getDescription())
                        .type(CouponTypeEnum.SINGLE_USE)
                        .user(us)
                        .build());
            });
            couponRepository.saveAll(couponList);
            return null;
        }

        User user = userRepository.findByEmail(couponRequest.getEmail());

        if (couponRequest.getType().equalsIgnoreCase(CouponTypeEnum.SINGLE_USE.name)) {
            coupon = Coupon.builder()
                    .active(true)
                    .code(RandomGenerator.generateRandomIntegerCode())
                    .expirationDate(LocalDateTime.now().plusDays(couponValidityDays))
                    .value(couponRequest.getCouponValue())
                    .rule(couponRequest.getRule())
                    .description(couponRequest.getDescription())
                    .type(CouponTypeEnum.SINGLE_USE)
                    .user(user)
                    .build();
        } else {
            coupon = Coupon.builder()
                    .active(true)
                    .code(couponRequest.getCode().toUpperCase())
                    .expirationDate(LocalDateTime.now().plusMonths(1))
                    .rule(couponRequest.getRule())
                    .description(couponRequest.getDescription())
                    .type(CouponTypeEnum.MULTIPLE_USE)
                    .usageCount(0)
                    .discountPercentage(couponRequest.getDiscountPercentage())
                    .maxAmountAllowed(couponRequest.getMaxAmountAllowed())
                    .user(user)
                    .build();
        }

        return couponRepository.save(coupon);
    }

    @Override
    public CouponResponse couponIsValid(String code) {
        Coupon coupon = couponRepository.findByCodeAndActiveTrue(code);
        if(null == coupon){
            throw new CouponNotFoundException();
        }

        return CouponResponse.builder()
                .percentage(coupon.getDiscountPercentage())
                .active(coupon.getActive())
                .build();
    }

    @Override
    public void inactiveCoupon() {
        log.info("inactiveCoupon");
        List<Coupon> couponList = couponRepository.findByActiveTrueAndExpirationDateBefore(LocalDateTime.now());
        couponList.forEach(cp -> cp.setActive(false));
        couponRepository.saveAll(couponList);
    }

}
