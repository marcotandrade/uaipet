package org.uaipet.api.service.impl;

import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.uaipet.api.data.enums.SaleStatusEnum;
import org.uaipet.api.data.model.CompanyNotification;
import org.uaipet.api.data.model.SaleStatus;
import org.uaipet.api.data.repository.CompanyNotificationRepository;
import org.uaipet.api.data.repository.projection.NotificationProjection;
import org.uaipet.api.data.repository.SaleStatusRepository;
import org.uaipet.api.dto.CompanyNotificationDTO;
import org.uaipet.api.dto.response.CompanyNotificationResponse;
import org.uaipet.api.dto.response.ProcessedResponse;
import org.uaipet.api.service.CompanyNotificationService;
import org.uaipet.api.service.exception.GenericException;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class CompanyNotificationServiceImpl implements CompanyNotificationService {

    private static final Logger log = LoggerFactory.getLogger(CompanyNotificationServiceImpl.class);

    private final CompanyNotificationRepository companyNotificationRepository;
    private final SaleStatusRepository saleStatusRepository;


    @Override
    public CompanyNotificationResponse getNotifications(String cnpj) {
        log.info(String.format("getNotifications - cnpj:%s", cnpj));
        List<NotificationProjection> projectionList = companyNotificationRepository.findCompanyNotifications(cnpj);
        if(!projectionList.isEmpty()) {
            List<CompanyNotificationDTO> notificationList = new ArrayList<>();
            projectionList.forEach( item -> notificationList.add(CompanyNotificationDTO.builder()
                    .id(item.getNotificationId().longValue())
                    .customerName(item.getCustomerName())
                    .orderId(item.getOrderId().longValue())
                    .viewed(item.getViewed())
                    .createdAt(item.getCreatedAt())
                    .build()));

            return CompanyNotificationResponse.builder()
                    .totalNotifications(projectionList.get(0).getTotalNotification().intValue())
                    .notifications(notificationList)
                    .build();
        } else {
            return CompanyNotificationResponse.builder().totalNotifications(0).build();
        }
    }

    @Transactional( rollbackOn = Exception.class )
    @Override
    public ProcessedResponse setNotificationAsSeen(Long orderId) {
        ProcessedResponse processedResponse = ProcessedResponse.builder().processed(false).build();
        try {
            Optional<SaleStatus> saleStatus = saleStatusRepository.findBySaleIdAndStatus(orderId, SaleStatusEnum.WAITING_CONFIRMATION);
            saleStatus.ifPresent(order -> {
                CompanyNotification companyNotification = companyNotificationRepository.findBySaleStatusAndViewedFalse(order);
                if(null != companyNotification) {
                    companyNotification.setViewed(true);
                    companyNotificationRepository.save(companyNotification);
                    processedResponse.setProcessed(true);
                }
            });
        } catch (Exception ex){
            throw new GenericException(ex.getMessage());
        }
        return processedResponse;
    }
}
