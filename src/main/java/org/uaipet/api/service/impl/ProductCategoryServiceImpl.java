package org.uaipet.api.service.impl;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;
import org.uaipet.api.data.model.ProductCategory;
import org.uaipet.api.data.repository.ProductCategoryRepository;
import org.uaipet.api.service.ProductCategoryService;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
public class ProductCategoryServiceImpl implements ProductCategoryService {

    private final ProductCategoryRepository repository;

    @Autowired
    public ProductCategoryServiceImpl(ProductCategoryRepository repository){
        this.repository = repository;
    }


    @Override
    public List<ProductCategory> findAll() {
        return this.repository.findAll();
    }

    @Override
    public ProductCategory findOne(Long id) {

        Optional< ProductCategory > product = this.repository.findById(id);

        if(!product.isPresent()){
            throw new EmptyResultDataAccessException(String.format("Categoria de Produto não encontado com o id %s", id), 1);
        }

        return product.get();
    }

    @Override
    @Transactional( rollbackOn = Exception.class )
    public ProductCategory save(ProductCategory product) {

        return this.repository.save( product );
    }

    @Override
    @Transactional( rollbackOn = Exception.class )
    public void update(Long id, ProductCategory product) {

        ProductCategory productSave = this.findOne( id );
        //copia as propriedades do objeto 1 para o 2 ignorando o ID
        BeanUtils.copyProperties( product, productSave, "id" );
        this.repository.save( productSave );
    }
}
