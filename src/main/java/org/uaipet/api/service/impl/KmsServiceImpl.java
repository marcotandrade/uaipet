package org.uaipet.api.service.impl;

import com.amazonaws.services.kms.AWSKMS;
import com.amazonaws.services.kms.model.DecryptRequest;
import com.amazonaws.services.kms.model.DecryptResult;
import com.amazonaws.services.kms.model.EncryptRequest;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.uaipet.api.service.KmsService;
import org.uaipet.api.service.exception.GenericException;

import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.GeneralSecurityException;
import java.util.Base64;

@Service
@RequiredArgsConstructor
public class KmsServiceImpl implements KmsService {

    private static final Logger log = LoggerFactory.getLogger(KmsServiceImpl.class);

    private final AWSKMS awskms;

    @Value("${aws.cloud.kms.key}")
    private String keyId;

    @Override
    public ByteBuffer encode(String stringValue, String keyId) {
        try {
            ByteBuffer plaintext = ByteBuffer.wrap(stringValue.getBytes(StandardCharsets.UTF_8));
            EncryptRequest req = new EncryptRequest().withKeyId(keyId).withPlaintext(plaintext);
             return awskms.encrypt(req).getCiphertextBlob();
        } catch (Exception ex) {
            log.error(ex.getMessage());
            throw new GenericException();
        }
    }

    @Override
    public String decode(String cypherText, String keyId) {
        try {
            ByteBuffer cipherTextBlob = ByteBuffer.wrap(Base64.getDecoder().decode(cypherText));
            DecryptRequest req = new DecryptRequest().withCiphertextBlob(cipherTextBlob);
            DecryptResult result = awskms.decrypt(req);
            if (!result.getKeyId().equals(keyId)) {
                throw new GeneralSecurityException("decryption failed: wrong key id");
            }
            return Charset.forName("UTF-8").decode(result.getPlaintext()).toString();
        } catch (Exception ex) {
            log.error(ex.getMessage());
            throw new GenericException();
        }
    }

}
