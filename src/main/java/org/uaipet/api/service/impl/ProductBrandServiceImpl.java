package org.uaipet.api.service.impl;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;
import org.uaipet.api.data.model.ProductBrand;
import org.uaipet.api.data.repository.ProductBrandRepository;
import org.uaipet.api.service.ProductBrandService;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
public class ProductBrandServiceImpl implements ProductBrandService {

    private final ProductBrandRepository repository;

    @Autowired
    public ProductBrandServiceImpl(ProductBrandRepository repository){
        this.repository = repository;
    }


    @Override
    public List<ProductBrand> findAll() {
        return this.repository.findAllByOrderByName();
    }

    @Override
    public ProductBrand findOne(Long id) {

        Optional< ProductBrand > product = this.repository.findById(id);

        if(!product.isPresent()){
            throw new EmptyResultDataAccessException(String.format("Marca não encontado com o id %s", id), 1);
        }

        return product.get();
    }

    @Override
    @Transactional( rollbackOn = Exception.class )
    public ProductBrand save(ProductBrand productBrand) {

        return this.repository.save( productBrand );
    }

    @Override
    @Transactional( rollbackOn = Exception.class )
    public void update(Long id, ProductBrand productBrand) {

        ProductBrand productSave = this.findOne( id );
        //copia as propriedades do objeto 1 para o 2 ignorando o ID
        BeanUtils.copyProperties( productBrand, productSave, "id" );
        this.repository.save( productSave );
    }
}
