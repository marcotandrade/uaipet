package org.uaipet.api.service.impl;

import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MaxUploadSizeExceededException;
import org.springframework.web.multipart.MultipartFile;
import org.uaipet.api.config.security.UserSS;
import org.uaipet.api.data.enums.CouponTypeEnum;
import org.uaipet.api.data.enums.SaleStatusEnum;
import org.uaipet.api.data.enums.UserStatusEnum;
import org.uaipet.api.data.model.*;
import org.uaipet.api.data.repository.AddressRepository;
import org.uaipet.api.data.repository.CompanyNotificationRepository;
import org.uaipet.api.data.repository.CouponRepository;
import org.uaipet.api.data.repository.DeliveryRangeRepository;
import org.uaipet.api.data.repository.ProductRepository;
import org.uaipet.api.data.repository.SaleRepository;
import org.uaipet.api.data.repository.SearchIndexRepository;
import org.uaipet.api.dto.ProductDTO;
import org.uaipet.api.dto.ProductQuantityDTO;
import org.uaipet.api.dto.ProductSearchDTO;
import org.uaipet.api.dto.request.ProductRequest;
import org.uaipet.api.dto.request.PurchaseRequest;
import org.uaipet.api.dto.request.PushNotificationRequest;
import org.uaipet.api.service.*;
import org.uaipet.api.service.exception.AddressNotFoundForUserException;
import org.uaipet.api.service.exception.AddressNotInRangeException;
import org.uaipet.api.service.exception.CompanyClosedException;
import org.uaipet.api.service.exception.EmailNotValidatedException;
import org.uaipet.api.service.exception.GenericException;
import org.uaipet.api.service.exception.ImageMaxSizeException;
import org.uaipet.api.service.exception.OrderMinimalAmountException;
import org.uaipet.api.service.exception.ProductException;
import org.uaipet.api.service.exception.ProductImageException;
import org.uaipet.api.service.exception.ProductNotAvailableException;
import org.uaipet.api.service.exception.ProductNotFoundException;
import org.uaipet.api.service.exception.UserNotFoundException;
import org.uaipet.api.utils.Builder;
import org.uaipet.api.utils.CheckDate;
import org.uaipet.api.utils.RandomGenerator;

import javax.transaction.Transactional;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.apache.commons.lang3.StringUtils.*;
import static org.apache.commons.lang3.StringUtils.isNotEmpty;
import static org.uaipet.api.data.repository.ProductSpecifications.*;

@Service
@RequiredArgsConstructor
public class ProductServiceImpl implements ProductService {

    private static final Logger LOGGER = LoggerFactory.getLogger(ProductServiceImpl.class);

    private final ProductRepository repository;
    private final DeliveryRangeRepository deliveryRangeRepository;
    private final SaleRepository saleRepository;
    private final AddressRepository addressRepository;
    private final CouponRepository couponRepository;
    private final SearchIndexRepository searchIndexRepository;
    private final CompanyNotificationRepository companyNotificationRepository;
    private final CompanyService companyService;
    private final ProductBrandService brandService;
    private final ProductCategoryService categoryService;
    private final ProductAnimalCategoryService animalCategoryService;
    private final ProductMeasurementCategoryService measurementCategoryService;
    private final UserService userService;
    private final S3StorageService s3StorageService;
    private final Builder builder;
    private final PushNotificationService pushNotificationService;

    @Override
    public ProductSearchDTO search(ProductRequest request) {
        LOGGER.info(String.format("search - %s", request.toString()));
        Pageable pageable = getPageable(request);
        Page<Product> page = null;
        User user = User.builder().build();
        if (request.getUserEmail() != null) {
            user = userService.findUserFromEmail(request.getUserEmail());
            Optional<Address> address = user.getAddressList().stream().filter(Address::getDefaultAddress).findAny();
            List<SearchIndex> searchIndexes = searchIndexRepository.findByUserAndUserAddress(user, address.orElseThrow(AddressNotFoundForUserException::new));
            if (!searchIndexes.isEmpty()) {
                List<Company> compList = new ArrayList<>();
                searchIndexes.forEach(idx -> compList.add(idx.getCompany()));
                page = this.repository.findAll(Specification.where(
                        nameLike(request.getName())
                                .and(notDeleted())
                                .and(companyIn(compList))
                                .and(ageCategoryEquals(request))
                                .and(animalCategoryEquals(request))
                                .and(productCategoryEquals(request))
                                .and(productBrandEquals(request))), pageable);
            } else {
                throw new AddressNotInRangeException();
            }
        } else {
            page = repository.findAll(Specification.where(
                    nameLike(request.getName())
                            .and(notDeleted())
                            .and(ageCategoryEquals(request))
                            .and(animalCategoryEquals(request))
                            .and(productCategoryEquals(request))
                            .and(productBrandEquals(request))), pageable);
        }
        return builder.buildProductSearchDTO(page, user);
    }

    private Pageable getPageable(ProductRequest request) {
        Pageable pageable = null;
        if(request.getSortBy() != null && request.getDirection() != null &&
                (request.getDirection().equalsIgnoreCase("ASC" ) || request.getDirection().equalsIgnoreCase("DESC"))){
            pageable = PageRequest.of(request.getPageNumber(), request.getPageSize(),
                    Sort.by(Sort.Direction.valueOf(request.getDirection().toUpperCase()) ,request.getSortBy().toLowerCase()));
        } else if(request.getSortBy() != null) {
            pageable = PageRequest.of(request.getPageNumber(), request.getPageSize(), Sort.by(request.getSortBy().toLowerCase()));
        } else {
            pageable =  PageRequest.of(request.getPageNumber(), request.getPageSize());
        }
        return pageable;
    }

    @Override
    public Product findOne(Long id) {
        Optional< Product > product = this.repository.findById(id);

        if(!product.isPresent()){
            throw new EmptyResultDataAccessException(String.format("Produto não encontado com o id %s", id), 1);
        }
        return product.get();
    }

    @Override
    public ProductDTO findOneById(Long id) {
        LOGGER.info(String.format("findOneById - %s", id.toString()));
        UserSS userSS = userService.authenticated();
        User user = null;
        if(null != userSS){
            user = userService.findUserFromEmail(userSS.getUsername());
            if(null == user){
                throw new UserNotFoundException();
            }
        }

        Optional<Product> product = repository.findById(id);
        ProductDTO productDTO = builder.buildProductDTO(product.orElseThrow(ProductNotFoundException::new), user == null ? null : user);
        return productDTO;
    }

    @Override
    @Transactional( rollbackOn = Exception.class )
    public Product save(String cnpj, ProductRequest request, List<MultipartFile> image ) {
        try {

            Product product = builder.buildProduct( request );
            Company company = this.companyService.findCompanyInfo( cnpj );
            product.setCompany(company);

            validationProduct(product);

            ArrayList<String> images = s3StorageService.uploadImage(image,
            request.getMeasurementValue() + request.getName() + company.getCnpj(),"product-images/");
            product.setImage(join(images, ";"));

            return this.repository.save( product );
        } catch (IOException e) {
            LOGGER.error("Erro ao salvar imagem do produto" + e.getMessage());
            throw new ProductImageException("Erro para salvar imagem do produto, tente novamente");
        } catch (MaxUploadSizeExceededException ex){
            LOGGER.error("Tamanho da imagem maior do que o permitido" + ex.getMessage());
            throw new ImageMaxSizeException();
        } catch (Exception ex){
            LOGGER.error("Erro ao salvar imagem do produto" + ex.getMessage());
            throw new GenericException();
        }

    }

    @Override
    @Transactional( rollbackOn = Exception.class )
    public void update(Long id, String cnpj, ProductRequest request, List<MultipartFile> image) {

        try {
            Company company = this.companyService.findCompanyInfo( cnpj );
            Product productSave = this.findOne( id );
            Product product = builder.buildProduct( request );
            String date = LocalDateTime.now().toString();
            ArrayList<String> images = s3StorageService.uploadImage(image, request.getMeasurementValue() + request.getName() + company.getCnpj() + date, "product-images/");
            String filesUploaded = join(images, ";");
            product.setImage(formatFiles(request, filesUploaded));
            product.setCompany(company);


            //copia as propriedades do objeto 1 para o 2 ignorando o ID
            BeanUtils.copyProperties( product, productSave, "id" );

            validationProduct(productSave);

            this.repository.save( productSave );

        } catch (IOException e){

            LOGGER.error("Erro ao atulizar imagem do produto" + e.getMessage());
            throw new ProductImageException("Erro para atualizar imagem do produto, tente novamente");

        }
    }

    private String formatFiles(ProductRequest request, String filesUploaded) {
        if(isNotEmpty(request.getImages()) && isNotEmpty(filesUploaded)) {
            return String.join(";", request.getImages(), filesUploaded);
        } else if(isEmpty(request.getImages())) {
            return filesUploaded;
        } else {
            return request.getImages();
        }
    }

    public void validationProduct(Product product){
        ProductCategory category = this.categoryService.findOne(product.getProductCategory().getId());
        ProductBrand brand = this.brandService.findOne(product.getProductBrand().getId());
        ProductAnimalCategory animalCategory = this.animalCategoryService.findOne(product.getProductAnimalCategory().getId());
        ProductMeasurementCategory measurementCategory = this.measurementCategoryService.findOne(product.getProductMeasurementCategory().getId());

        if(Objects.isNull(category)) {
            throw new ProductException("Categoria é obrigatoria");
        }

        if(Objects.isNull(brand)) {
            throw new ProductException("Marca do produto é obrigatoria");
        }

        if(Objects.isNull(animalCategory)) {
            throw new ProductException("Categoria de animal é obrigatoria");
        }

        if(Objects.isNull(measurementCategory)) {
            throw new ProductException("Categoria de medição é obrigatoria");
        }

        product.setProductCategory(category);
        product.setProductBrand(brand);
        product.setProductAnimalCategory(animalCategory);
        product.setProductMeasurementCategory(measurementCategory);

    }

    @Override
    public Boolean purchase(PurchaseRequest purchaseRequest) {
        LOGGER.info(String.format("purchase - %s", purchaseRequest.toString()));
        if (purchaseValidation(purchaseRequest)) {
            try {
                createTransaction(purchaseRequest);
            } catch (Exception ex) {
                LOGGER.error(ex.getMessage());
                throw new GenericException();
            }
            LOGGER.info("purchase succeeded");
            return true;
        }
        LOGGER.error("purchase failed");
        return false;
    }


    @Override
    public ProductSearchDTO productsSpotlight() {
        LOGGER.info("productsSpotlight");
        UserSS userSS = userService.authenticated();
        Pageable pageable = getSpotLightPageable();
        if(null == userSS){
            LOGGER.info(userSS.getUsername());
            LOGGER.info("productsSpotlight before query");
            Page<Product> page = repository.findAll(Specification.where(notDeleted())
                    .and(onSpotlight())
                    .and(isAvailable()), pageable);
        LOGGER.info("productsSpotlight after query");
        return builder.buildProductSearchDTO(page, null);
        }
        User user = userService.findUserFromEmail(userSS.getUsername());
        Optional<Address> ad = user.getAddressList().stream().filter(Address::getDefaultAddress).findFirst();
        List<SearchIndex> searchIndices = searchIndexRepository.findByUserAndUserAddress(user, ad.orElseThrow(AddressNotFoundForUserException::new));
        List<Company> companies = new ArrayList<>();
                searchIndices.forEach(s -> companies.add(s.getCompany()));
        LOGGER.info("productsSpotlight before query");
        Page<Product> page = repository.findAll(Specification.where(companyIn(companies)
                .and(notDeleted())
                .and(onSpotlight())
                .and(isAvailable())), pageable);
        LOGGER.info("productsSpotlight after query");
        return builder.buildProductSearchDTO(page, user);
    }

    @Override
    public Boolean deleteProduct(Long id) {
        try {
            LOGGER.info(String.format("deleteProduct - %s", id));
            Product product = repository.findById(id).orElseThrow(ProductNotFoundException::new);
            product.setDeleted(true);
            repository.save(product);
            return true;
        } catch (ProductNotFoundException ex){
            throw ex;
        } catch (Exception ex) {
            LOGGER.info(String.format("deleteProduct - %s", ex.getMessage()));
            throw new GenericException();
        }
    }

    private void createTransaction(PurchaseRequest purchaseRequest) {
        LOGGER.info("createTransaction");
        ArrayList saleStatusList = new ArrayList();
        ArrayList saleItemsList = new ArrayList();

        Coupon coupon = null;
        if(purchaseRequest.getCouponCode() != null){
            coupon = couponRepository.findByCodeAndUserAndActiveTrueAndExpirationDateAfter(purchaseRequest.getCouponCode().toUpperCase(),
                    purchaseRequest.getUser(), LocalDateTime.now());
            if(null == coupon){
                coupon = couponRepository.findByCodeAndActiveTrueAndExpirationDateAfter(purchaseRequest.getCouponCode().toUpperCase(), LocalDateTime.now());
            }
        }

        Optional<DeliveryRange> deliveryRange = calcFreightValue(purchaseRequest);

        PaymentMethod paymentMethod = PaymentMethod.builder()
                .change(purchaseRequest.getChange())
                .paymentMethod(purchaseRequest.getPaymentMethod())
                .build();


       String amount = calcProductAmount(purchaseRequest, deliveryRange).toString();
       Company company = purchaseRequest.getCompany();
        Sale sale = Sale.builder()
                .amount(amount)
                .freightAmount(validateFreightAmount(deliveryRange.get().getFreightValue(),
                        company.getAmountFreeFreight(),
                        amount))
                .company(company)
                .user(purchaseRequest.getUser())
                .address(addressRepository.findById(purchaseRequest.getShipAddressId()).get())
                .paymentMethod(paymentMethod)
                .build();

        SaleStatus saleStatus = SaleStatus.builder()
                .status(SaleStatusEnum.WAITING_CONFIRMATION)
                .sale(sale)
                .build();

        purchaseRequest.getPList().forEach(item -> {
            Optional<Product> product = repository.findById(item.getProductId());
            product.ifPresent(value -> {
                SaleItems saleItems = SaleItems.builder()
                    .productName(value.getName())
                    .amount(getProductAmount(item))
                    .productQtt(item.getQuantity())
                    .product(value)
                    .sale(sale)
                    .build();
                saleItemsList.add(saleItems);
            });
        });
        saleStatusList.add(saleStatus);
        sale.setSaleStatusList(saleStatusList);
        sale.setSaleItemsList(saleItemsList);
        BigDecimal couponRemainingValue = createRemainingCouponValue(purchaseRequest, coupon);
        sale.setCouponAmount(getUsedValue(coupon, couponRemainingValue, amount));
        sale.setCoupon(coupon);
        saleRepository.save(sale);
        updateExistingCoupon(coupon);

        CompanyNotification companyNotification = CompanyNotification.builder()
                .saleStatus(sale.getSaleStatusList().get(0))
                .viewed(false)
                .build();
        companyNotificationRepository.save(companyNotification);
        invalidateMultipleUseCoupon(coupon);
        if(!company.getCnpj().equalsIgnoreCase("98329832938293")){
            pushNotificationService.sendNotification(getAdminTokens(sale.getAmount()), false);
        }
        LOGGER.info("createTransaction finished");
    }

    private PushNotificationRequest getAdminTokens(String saleAmount) {

        List<String> adminList = new ArrayList<>();


        //marco
        adminList.add("ExponentPushToken[TbHkbdHKauyQ6bWC7VHKn1]");
        //leon
        adminList.add("ExponentPushToken[8pnt_zGewKJRtT8Tr8Tdcr]");
        adminList.add("ExponentPushToken[as2aFrEqfSTvJDn5ElfjL_]");
        adminList.add("ExponentPushToken[t9H4kuBl6i62RS1UbfzFlV]");
        //fabio
        adminList.add("ExponentPushToken[9JnqcgJ3wWLJHO0TP7Wp8O]");
        adminList.add("ExponentPushToken[sO217eMwAaB85zwgYeTyZr]");



         return PushNotificationRequest.builder()
                .allUsers(false)
                .title("Alerta de Venda!")
                .text("Venda feita no valor de R$ "+ saleAmount)
                .expoToken(adminList)
                .build();
    }

    private void invalidateMultipleUseCoupon(Coupon coupon) {
        if(null!= coupon && null != coupon.getMaxAmountAllowed()) {
            BigDecimal maxAmount = new BigDecimal(coupon.getMaxAmountAllowed());
            String amountStringValue = saleRepository.getSaleAmountWithCoupon(coupon.getId());
            BigDecimal saleAmountWithCoupon = new BigDecimal(amountStringValue);

            if (saleAmountWithCoupon.compareTo(maxAmount) > 0) {
                coupon.setActive(false);
                couponRepository.save(coupon);
            }
        }
    }

    private String validateFreightAmount(String freightValue, String amountFreeFreight, String amount) {
        BigDecimal bdAmount = new BigDecimal(amount);
        BigDecimal bdAmountFreeFreight = new BigDecimal(amountFreeFreight == null ? "0.0" : amountFreeFreight);
        if(amountFreeFreight != null && bdAmount.compareTo(bdAmountFreeFreight) > 0){
            //free freight
            return null;
        }else{
            return freightValue;
        }
    }


    private Optional<DeliveryRange> calcFreightValue(PurchaseRequest purchaseRequest) {
        List<DeliveryRange> deliveryRangeList = purchaseRequest.getCompany().getDeliveryRanges()
                .stream().sorted(Comparator.comparing(DeliveryRange::getDistance)).collect(Collectors.toList());

        return deliveryRangeList.stream().filter(dlr -> purchaseRequest.getSearchIndex().getDistance() <= dlr.getDistance() * 1000).findAny();
    }

    private void updateExistingCoupon(Coupon coupon) {
        if(coupon != null) {
            if (coupon.getType().equals(CouponTypeEnum.SINGLE_USE)) {
                coupon.setActive(false);
                coupon.setExpirationDate(LocalDateTime.now());
            } else {
                coupon.setUsageCount(coupon.getUsageCount() + 1);
            }
            couponRepository.save(coupon);
        }
    }

    private String getUsedValue(Coupon coupon, BigDecimal couponRemainingValue, String saleAmount) {
        BigDecimal value = new BigDecimal("0.0");
        if(coupon != null &&  coupon.getType().equals(CouponTypeEnum.SINGLE_USE)) {
            if (couponRemainingValue.compareTo(new BigDecimal("0.0")) < 0) {
                return new BigDecimal(coupon.getValue()).toString();
            } else {
                return new BigDecimal(coupon.getValue()).subtract(couponRemainingValue).toString();
            }
        } else if(coupon != null &&  coupon.getType().equals(CouponTypeEnum.MULTIPLE_USE)){
            BigDecimal sale = new BigDecimal(saleAmount);
            BigDecimal intPercentage = new BigDecimal(coupon.getDiscountPercentage());
            BigDecimal divisor = new BigDecimal(100);
            BigDecimal percentage = intPercentage.divide(divisor, 2, RoundingMode.CEILING);
            BigDecimal discount = sale.multiply(percentage).setScale(2, RoundingMode.FLOOR);
            return discount.toString();
        }
        return value.toString();
    }


    private BigDecimal createRemainingCouponValue(PurchaseRequest purchaseRequest, Coupon coupon) {
        BigDecimal couponRemainingValue =  calcCouponAmount(purchaseRequest, coupon);
        if(couponRemainingValue.compareTo(new BigDecimal("0.0")) > 0){
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
            String formattedDate = LocalDateTime.now().plusDays(30).format(formatter);

            Coupon remainingCoupon = Coupon.builder()
                    .active(true)
                    .expirationDate(LocalDateTime.now().plusDays(30))
                    .value(couponRemainingValue.toString())
                    .user(purchaseRequest.getUser())
                    .code(RandomGenerator.generateRandomIntegerCode())
                    .description("Você não usou todo o seu cupom!")
                    .rule(String.format("Aproveite o seu novo cupom de R$%s para fazer compras até dia %s",
                            couponRemainingValue.toString() , formattedDate))
                    .build();

            List<String> expoToken = new ArrayList<>();
            expoToken.add(purchaseRequest.getUser().getExpoToken());
            PushNotificationRequest pushNotificationRequest = PushNotificationRequest.builder()
                    .title("Você não usou todo o seu cupom!")
                    .text(String.format("Aproveite o seu novo cupom de R$%s para fazer compras até dia %s",
                            remainingCoupon.getValue() , formattedDate))
                    .expoToken(expoToken)
                    .allUsers(false)
                    .build();
            try{
                pushNotificationService.sendNotification(pushNotificationRequest, false);
            } catch (Exception ex){
                LOGGER.info(ex.getMessage());
            }
            couponRepository.save(remainingCoupon);
        }
        return couponRemainingValue;
    }


    private BigDecimal calcProductAmount(PurchaseRequest request, Optional<DeliveryRange> deliveryRange) {
        BigDecimal purchaseAmount = new BigDecimal("0.00");
        for (ProductQuantityDTO productQuantity : request.getPList()) {
            Optional<Product> pr = repository.findById(productQuantity.getProductId());
            if (pr.isPresent()) {
                purchaseAmount = purchaseAmount.add((new BigDecimal(pr.get().getPrice()).multiply(new BigDecimal(productQuantity.getQuantity()))));
            }
        }

        if(deliveryRange.isPresent()) {
            if (request.getCompany().getAmountFreeFreight() != null && !deliveryRange.get().getFreeFreight() &&
                    purchaseAmount.compareTo(new BigDecimal(request.getCompany().getAmountFreeFreight())) < 0) {
                purchaseAmount = purchaseAmount.add(new BigDecimal(deliveryRange.get().getFreightValue()));
            }
        }
        return purchaseAmount;
    }

    private BigDecimal calcCouponAmount(PurchaseRequest request, Coupon coupon) {
        BigDecimal productAmount = new BigDecimal("0.0");
        if(coupon != null && coupon.getType().equals(CouponTypeEnum.SINGLE_USE)){
            for (ProductQuantityDTO productQuantity : request.getPList()) {
                Optional<Product> pr = repository.findById(productQuantity.getProductId());
                if (pr.isPresent()) {
                    productAmount = productAmount.add(new BigDecimal(pr.get().getPrice()).multiply(new BigDecimal(productQuantity.getQuantity())));
                }
            }
            return new BigDecimal(coupon.getValue()).subtract(productAmount).compareTo(new BigDecimal("0.0")) > 0
                    ? new BigDecimal(coupon.getValue()).subtract(productAmount)
                    : new BigDecimal("0") ;
        }
        return productAmount;
    }

    private String getProductAmount(ProductQuantityDTO request) {
        BigDecimal purchaseAmount = new BigDecimal("0.0");
            Optional<Product> pr = repository.findById(request.getProductId());
            if (pr.isPresent()) {
                purchaseAmount = purchaseAmount.add((new BigDecimal(pr.get().getPrice()).multiply(new BigDecimal(request.getQuantity()))));
            }
        return purchaseAmount.toString();
    }

    private boolean productAvailable(PurchaseRequest request) {
        for (ProductQuantityDTO prd : request.getPList()) {
            boolean exists = repository.existsByIdAndStockCountGreaterThanEqual(prd.getProductId(), prd.getQuantity());
            if (!exists) {
                throw new ProductException("Ops! estamos sem estoque deste este produto");
            } else {
                return true;
            }
        }
        return false;
    }

    private void isOnDeliveryRange(PurchaseRequest purchaseRequest) {
        User user = userService.findUserFromEmail(purchaseRequest.getUserEmail());
        if(user != null ) {
           if(user.getStatus().equalsIgnoreCase(UserStatusEnum.NOT_VALID.getStatus())){
              throw new EmailNotValidatedException();
            }

            Optional<Address> address = addressRepository.findById(purchaseRequest.getShipAddressId());
            SearchIndex searchIndex = searchIndexRepository.findFirstByUserAndCompanyAndUserAddress(user, purchaseRequest.getCompany(),
                    address.orElseThrow(AddressNotFoundForUserException::new));
            if(searchIndex == null){
                throw new AddressNotInRangeException();
            }
            purchaseRequest.setSearchIndex(searchIndex);
            purchaseRequest.setUser(user);
        }else{
            throw new UserNotFoundException();
        }
    }

    //validates if the list of products are from the same store
    private Boolean purchaseValidation(PurchaseRequest request) {
        LOGGER.debug("productsFromSameStore");
        Company company = repository.findById(request.getPList().get(0).getProductId()).get().getCompany();
        Long merchantId = company.getId();
        BigDecimal purchaseAmount = new BigDecimal("0.00");

        for(ProductQuantityDTO item : request.getPList()){
            Product product = repository.findById(item.getProductId()).orElseThrow(ProductNotFoundException::new);
            if(!merchantId.equals(product.getCompany().getId())){
                LOGGER.error("List the of purchase must have itens from the same store");
                throw new ProductException("List the of purchase must have itens from the same store");
            }
            if(!product.getAvailable()){
                LOGGER.error("product not available for purchase");
                throw new ProductNotAvailableException();
            }

            purchaseAmount = purchaseAmount.add((new BigDecimal(product.getPrice()).multiply(new BigDecimal(item.getQuantity()))));
        }
        checkMinimalAmount(purchaseAmount, company);
        request.setCompany(company);
        isOnDeliveryRange(request);
        if(!CheckDate.checkIfTheCompanyIsOpen(company.getBusinessHours())){
            throw new CompanyClosedException();
        }
        return true;
    }

    private void checkMinimalAmount(BigDecimal purchaseAmount, Company company) {
        if(purchaseAmount.compareTo(new BigDecimal(company.getMinimumOrderAmount())) < 0){
            LOGGER.debug("Order minimal amount not achieved");
            throw new OrderMinimalAmountException();
        }
    }


    //count down the product stock before the purchase
    private void productCountDown(PurchaseRequest request) {
        LOGGER.debug("productCountDown");
        for (ProductQuantityDTO prd : request.getPList()) {
            repository.countDownProductStockQtt(prd.getQuantity(), prd.getProductId());
        }
    }

    private void productCountDownRollback(PurchaseRequest request) {
        for (ProductQuantityDTO prd : request.getPList()) {
            repository.rollbackStockCountDown(prd.getQuantity(), prd.getProductId());
        }
    }

    private Pageable getSpotLightPageable() {
        return PageRequest.of(0, 6);
    }


}
