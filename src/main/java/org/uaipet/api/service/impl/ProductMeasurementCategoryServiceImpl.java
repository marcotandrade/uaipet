package org.uaipet.api.service.impl;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;
import org.uaipet.api.data.model.ProductMeasurementCategory;
import org.uaipet.api.data.repository.ProductMeasurementCategoryRepository;
import org.uaipet.api.service.ProductMeasurementCategoryService;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
public class ProductMeasurementCategoryServiceImpl implements ProductMeasurementCategoryService {

    private final ProductMeasurementCategoryRepository repository;

    @Autowired
    public ProductMeasurementCategoryServiceImpl(ProductMeasurementCategoryRepository repository){
        this.repository = repository;
    }


    @Override
    public List<ProductMeasurementCategory> findAll() {
        return this.repository.findAll();
    }

    @Override
    public ProductMeasurementCategory findOne(Long id) {

        Optional< ProductMeasurementCategory > product = this.repository.findById(id);

        if(!product.isPresent()){
            throw new EmptyResultDataAccessException(String.format("Categoria de medição do produto não encontado com o id %s", id), 1);
        }

        return product.get();
    }

    @Override
    @Transactional( rollbackOn = Exception.class )
    public ProductMeasurementCategory save(ProductMeasurementCategory product) {

        return this.repository.save( product );
    }

    @Override
    @Transactional( rollbackOn = Exception.class )
    public void update(Long id, ProductMeasurementCategory product) {

        ProductMeasurementCategory productSave = this.findOne( id );
        //copia as propriedades do objeto 1 para o 2 ignorando o ID
        BeanUtils.copyProperties( product, productSave, "id" );
        this.repository.save( productSave );
    }
}
