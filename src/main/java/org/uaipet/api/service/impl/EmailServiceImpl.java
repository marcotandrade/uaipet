package org.uaipet.api.service.impl;

import lombok.AllArgsConstructor;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.InputStreamSource;
import org.springframework.core.io.Resource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring5.SpringTemplateEngine;
import org.uaipet.api.dto.MailDTO;
import org.uaipet.api.service.EmailService;

import javax.activation.MimetypesFileTypeMap;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;

@AllArgsConstructor
@Service
public class EmailServiceImpl implements EmailService {

    private static final Logger log = LoggerFactory.getLogger(EmailServiceImpl.class);

    private final JavaMailSender emailSender;

    private final SpringTemplateEngine templateEngine;

    private final Environment environment;

    @Async("asyncTaskExecutor")
    @Override
    public void sendSimpleMessage(MailDTO mail) {
        long startTime = System.currentTimeMillis();
        if(Arrays.toString(environment.getActiveProfiles()).contains("qa") ||
                Arrays.toString(environment.getActiveProfiles()).contains("prod")) {
            log.debug(String.format("sendSimpleMessage initialized - %s ", mail.toString()));
            try {
                MimeMessage mimeMessage = emailSender.createMimeMessage();
                MimeMessageHelper helper = new MimeMessageHelper(mimeMessage,
                        MimeMessageHelper.MULTIPART_MODE_MIXED_RELATED, StandardCharsets.UTF_8.name());

                Context context = new Context();
                context.setVariables(mail.getProps());
                String html = templateEngine.process(mail.getTemplateName(), context);

                helper.setTo(mail.getMailTo());
                helper.setText(html, true);
                helper.setSubject(mail.getSubject());
                helper.setFrom(mail.getMailFrom());

                try {

                    File file = new File("src/main/resources/mail-templates/image/uaipetorg.jpeg");
                    FileInputStream input = new FileInputStream(file);
                    MultipartFile multipartFile = new MockMultipartFile("uaipetorg",
                            file.getName(), "image/jpeg", IOUtils.toByteArray(input));
                    final InputStreamSource imageSource = new ByteArrayResource(multipartFile.getBytes());
                    helper.addInline(multipartFile.getName(),
                            imageSource, multipartFile.getContentType());
                } catch (IOException e) {
                    log.error("Error trying to add image on e-mail");
                }

                emailSender.send(mimeMessage);

                log.debug(String.format("sendSimpleMessage finalized in %s", System.currentTimeMillis() - startTime));
            } catch (MessagingException ex){
                ex.printStackTrace();
            }
        }
    }

}
