package org.uaipet.api.service.impl;

import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Slice;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.uaipet.api.data.model.Address;
import org.uaipet.api.data.model.Company;
import org.uaipet.api.data.model.SearchIndex;
import org.uaipet.api.data.model.User;
import org.uaipet.api.data.repository.AddressRepository;
import org.uaipet.api.data.repository.CompanyRepository;
import org.uaipet.api.data.repository.SearchIndexRepository;
import org.uaipet.api.data.repository.UserRepository;
import org.uaipet.api.integration.client.GeoCodingClient;
import org.uaipet.api.service.KmsService;
import org.uaipet.api.service.SearchIndexService;
import org.uaipet.api.service.exception.GenericException;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.OptionalInt;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import static java.util.Comparator.comparingInt;
import static java.util.stream.Collectors.collectingAndThen;
import static java.util.stream.Collectors.toCollection;

@RequiredArgsConstructor
@Service
public class SearchIndexServiceImpl implements SearchIndexService {

    private static final Logger log = LoggerFactory.getLogger(SearchIndexServiceImpl.class);

    private final SearchIndexRepository searchIndexRepository;
    private final CompanyRepository companyRepository;
    private final AddressRepository addressRepository;
    private final KmsService kmsService;
    private final UserRepository userRepository;
    private final GeoCodingClient geoCodingClient;

    @Value("${cloud.google.geo-coding-api.key}")
    private String googleApiKey;

    @Value("${page.size.searchindex}")
    private Integer pageSize;

    @Value("${aws.cloud.kms.key}")
    private String keyId;

    @Transactional(rollbackFor = Exception.class)
    @Async("asyncTaskExecutor")
    @Override
    public void save(Address userAddress, Address storeAddress, Company company) {
        log.info("save search_index");
        List<SearchIndex> searchIndex = new ArrayList<>();

        try {
            if (userAddress != null) {
                log.info("userAddress - " + userAddress.toString());
                List<Company> companies = companyRepository.findByAddressCityAndAddressState(userAddress.getCity(), userAddress.getState());
                companies.forEach(comp -> {
                    Long routeDistance = getAddressDistance(comp.getAddress(), userAddress);
                    OptionalInt maxDistance = comp.getDeliveryRanges().stream().mapToInt(x -> x.getDistance()).max();
                    if (maxDistance.isPresent() && maxDistance.getAsInt() > routeDistance.intValue() / 1000) {
                        SearchIndex index = SearchIndex.builder()
                                .company(comp)
                                .user(userAddress.getUser())
                                .userAddress(userAddress)
                                .distance(routeDistance)
                                .build();
                        searchIndex.add(index);
                    }
                });
                log.info("Saving current search index list");
                searchIndexRepository.saveAll(searchIndex);
            } else {
                log.info("storeAddress - " + storeAddress.toString());
                int page = 0;
                boolean hasNext;
                do {
                    Slice<User> users = userRepository.findByAddressListCityAndAddressListState(storeAddress.getCity(),
                            storeAddress.getState(), PageRequest.of(page, pageSize));

                    users.forEach(us -> {
                        List<Address> addressList = addressRepository.findByUser(us);
                        addressList.forEach(ad -> {
                            Long routeDistance = getAddressDistance(storeAddress, ad);
                            OptionalInt maxDistance = company.getDeliveryRanges().stream().mapToInt(x -> x.getDistance()).max();
                            if (maxDistance.isPresent() && maxDistance.getAsInt() > routeDistance.intValue() / 1000) {
                                SearchIndex index = SearchIndex.builder()
                                        .company(company)
                                        .user(us)
                                        .userAddress(ad)
                                        .distance(routeDistance)
                                        .build();
                                searchIndex.add(index);
                            }
                        });
                    });
                    page++;
                    hasNext = users.hasNext();
                } while (hasNext);
                log.info("Saving current search index list");
                List<SearchIndex> listWithoutDuplicates = searchIndex.stream()
                        .filter(distinctByKey(s -> s.getUserAddress().getId()))
                        .collect(Collectors.toList());
                searchIndexRepository.saveAll(listWithoutDuplicates);
            }
        } catch (Exception ex) {
            log.error("Error creating search index list", ex);
        }
        log.info("Finished");
    }

    public static <T> Predicate<T> distinctByKey(Function<? super T, ?> keyExtractor) {
        Map<Object, Boolean> seen = new ConcurrentHashMap<>();
        return t -> seen.putIfAbsent(keyExtractor.apply(t), Boolean.TRUE) == null;
    }

    private Long getAddressDistance(Address originAddress, Address destinationAddress) {
        try {
            String originApiFormat = originAddress.getStreet().concat("+").concat(originAddress.getNumber().toString())
                    .concat("+").concat(originAddress.getDistrict()).concat("+").concat(originAddress.getCity());

            String destinationApiFormat = destinationAddress.getStreet().concat("+").concat(destinationAddress.getNumber().toString())
                    .concat("+").concat(destinationAddress.getDistrict()).concat("+").concat(destinationAddress.getCity().toString());


            Object distanceMatrixResponse = geoCodingClient.fetchDistanceInfo(originApiFormat, destinationApiFormat,
                    kmsService.decode(googleApiKey, keyId));
            if (distanceMatrixResponse != null) {
                Object rows = ((LinkedHashMap) distanceMatrixResponse).get("rows");
                Object elements = ((LinkedHashMap) ((ArrayList) rows).get(0)).get("elements");
                Object distance = ((LinkedHashMap) ((ArrayList) elements).get(0)).get("distance");
                Integer distanceInMeters = (Integer) ((LinkedHashMap) distance).get("value");
                return distanceInMeters.longValue();
            }
            return null;
        } catch (Exception ex){
            log.error(ex.getMessage());
            throw new GenericException();
        }
    }

    @Override
    public void deleteAddressRecord(Long addressId) {
        log.info(String.format("deleteAddressRecord - addressId: %s", addressId));
        searchIndexRepository.deleteByUserAddressId(addressId);

    }
}
