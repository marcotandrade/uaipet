package org.uaipet.api.service.impl;

import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.uaipet.api.data.model.City;
import org.uaipet.api.data.repository.CityRepository;
import org.uaipet.api.dto.request.AddressRequest;
import org.uaipet.api.service.CityService;
import org.uaipet.api.service.exception.CityNotConfiguredException;
import org.uaipet.api.service.exception.GenericException;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class CityServiceImpl implements CityService {

    private static final Logger log = LoggerFactory.getLogger(CityService.class);

    private final CityRepository cityRepository;


    @Override
    public City findCity(AddressRequest addressRequest, Boolean blockRegister) {
        try {
            log.info("findCity");
            Optional<City> city = cityRepository.findByCityIgnoreCaseAndStateIgnoreCase(addressRequest.getCity(), addressRequest.getState().getName());
            if (city.isEmpty() && blockRegister) {
                log.error("city is empty and register is blocked");
                throw new CityNotConfiguredException();
            }
            if (city.isEmpty()) {
                log.info(String.format("creating city - addressRequest %s", addressRequest.toString()));
                return cityRepository.save(City.builder()
                        .city(addressRequest.getCity())
                        .state(addressRequest.getState().getName())
                        .onBusiness(false)
                        .build());
            }
            // bloqueia cadastro da loja, caso um cliente tenho cadastrado cidade primeiro
            if (!city.get().getOnBusiness() && blockRegister) {
                throw new CityNotConfiguredException();
            }

            return city.orElseThrow(CityNotConfiguredException::new);
        } catch (CityNotConfiguredException ex){
            throw ex;
        } catch (Exception ex){
            throw new GenericException();
        }
    }
}
