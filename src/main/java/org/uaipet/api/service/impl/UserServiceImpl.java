package org.uaipet.api.service.impl;

import com.google.code.geocoder.model.GeocodeResponse;
import com.google.code.geocoder.model.GeocoderStatus;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpHeaders;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import org.uaipet.api.config.security.JWTUtil;
import org.uaipet.api.config.security.UserSS;
import org.uaipet.api.data.enums.UserRoleEnum;
import org.uaipet.api.data.enums.UserStatusEnum;
import org.uaipet.api.data.enums.UserTypeEnum;
import org.uaipet.api.data.model.Address;
import org.uaipet.api.data.model.Company;
import org.uaipet.api.data.model.Coupon;
import org.uaipet.api.data.model.Sale;
import org.uaipet.api.data.model.SaleItems;
import org.uaipet.api.data.model.SaleStatus;
import org.uaipet.api.data.model.User;
import org.uaipet.api.data.model.UserRole;
import org.uaipet.api.data.repository.AddressRepository;
import org.uaipet.api.data.repository.CompanyRepository;
import org.uaipet.api.data.repository.ProductRepository;
import org.uaipet.api.data.repository.SaleItemsRepository;
import org.uaipet.api.data.repository.SaleRepository;
import org.uaipet.api.data.repository.SaleStatusRepository;
import org.uaipet.api.data.repository.SearchIndexRepository;
import org.uaipet.api.data.repository.UserRepository;
import org.uaipet.api.data.repository.projection.InfluencerSalesProjection;
import org.uaipet.api.dto.MailDTO;
import org.uaipet.api.dto.OrderProductsDTO;
import org.uaipet.api.dto.request.AddressRequest;
import org.uaipet.api.dto.request.UserRequest;
import org.uaipet.api.dto.request.ValidationRequest;
import org.uaipet.api.dto.response.CompanyResponse;
import org.uaipet.api.dto.response.InfluencerSalesResponse;
import org.uaipet.api.dto.response.RegisterResponse;
import org.uaipet.api.dto.response.ReportProblemResponse;
import org.uaipet.api.dto.response.UserOrdersResponse;
import org.uaipet.api.integration.client.GeoCodingClient;
import org.uaipet.api.service.EmailService;
import org.uaipet.api.service.KmsService;
import org.uaipet.api.service.S3StorageService;
import org.uaipet.api.service.SearchIndexService;
import org.uaipet.api.service.UserService;
import org.uaipet.api.service.exception.AddressNotFoundForUserException;
import org.uaipet.api.service.exception.AuthorizationException;
import org.uaipet.api.service.exception.EmailAlreadyExistsException;
import org.uaipet.api.service.exception.EmailValidationException;
import org.uaipet.api.service.exception.GenericException;
import org.uaipet.api.service.exception.IncorrectOldPasswordException;
import org.uaipet.api.service.exception.MandatoryPropertyNotFoundException;
import org.uaipet.api.service.exception.UserNotFoundException;
import org.uaipet.api.utils.Builder;
import org.uaipet.api.utils.RandomGenerator;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Service
public class UserServiceImpl implements UserService {

    private static final Logger log = LoggerFactory.getLogger(UserServiceImpl.class);
    public static final String UAI_PET_ORG_EMAIL = "noreply@uaipetapp.com.br";

    @Value("${cloud.google.geo-coding-api.key}")
    private String googleApiKey;

    @Value("${aws.cloud.kms.key}")
    private String keyId;

    private final UserRepository userRepository;
    private final AddressRepository addressRepository;
    private final SaleRepository saleRepository;
    private final SaleItemsRepository saleItemsRepository;
    private final SaleStatusRepository saleStatusRepository;
    private final SearchIndexRepository searchIndexRepository;
    private final CompanyRepository companyRepository;
    private final ProductRepository productRepository;
    private final S3StorageService s3StorageService;
    private final EmailService emailService;
    private final GeoCodingClient geoCodingClient;
    private final SearchIndexService searchIndexService;
    private final KmsService kmsService;
    private final JWTUtil jwtUtil;
    private final BCryptPasswordEncoder encoder;
    private final Builder builder;

    @Transactional(rollbackFor = Exception.class)
    @Override
    public RegisterResponse saveUser(UserRequest userRequest, List<MultipartFile> image) {
        log.info(String.format("saveUser - %s", userRequest.toString()));
            userRequest.setAddress(setAddressLatLng(userRequest.getAddress()));
            userRequest.setPassword(encoder.encode(userRequest.getPassword()));
            userRequest.setCpf(userRequest.getCpf());
            User user = builder.buildUser(userRequest);
            user.setValidationCode(RandomGenerator.generateRandomIntegerCode());
            ArrayList<String> s3ImageUrl = new ArrayList<>();
            try {
                RegisterResponse response = RegisterResponse.builder().build();

                if (!image.isEmpty()) {
                    s3ImageUrl = s3StorageService.uploadImage(image, user.getEmail() + "-pic", "customer-images/");
                    user.setImage(s3ImageUrl.isEmpty() ? null : s3ImageUrl.get(0));
                }
                User persistedUser = userRepository.save(user);

                if (persistedUser.getEmail() != null) {
                    Map<String, Object> userInfo = new HashMap<>();
                    userInfo.put("name", persistedUser.getName());
                    userInfo.put("tokenValue", persistedUser.getValidationCode());

                    MailDTO mailInfo = MailDTO.builder()
                            .mailTo(persistedUser.getEmail())
                            .mailFrom(UAI_PET_ORG_EMAIL)
                            .name(persistedUser.getName())
                            .props(userInfo)
                            .templateName("registration-email")
                            .subject("Validação de cadastro")
                            .build();

                    HttpHeaders headers = generateToken(persistedUser);

                    response = RegisterResponse.builder()
                            .headers(headers)
                            .user(persistedUser)
                            .build();

                    emailService.sendSimpleMessage(mailInfo);
                    searchIndexService.save(persistedUser.getAddressList().get(0), null, null);
                }
                log.info("saveUser - finished");
                return response;
            } catch (EmailAlreadyExistsException ex) {
                throw ex;
            } catch (DataIntegrityViolationException ex){
                throw new EmailAlreadyExistsException();
            } catch (Exception ex) {
                log.error(ex.getMessage());
                throw new GenericException();
            }
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public Boolean validateUserEmail(ValidationRequest validationRequest) {
        log.info("validateUserEmail");
        Boolean response = false;
            User user = userRepository.findByEmail(validationRequest.getEmail());
            if (user.getValidationCode().equalsIgnoreCase(validationRequest.getCode())) {
                response = true;
                user.setValidationCode(null);
                user.setStatus(UserStatusEnum.VALID.getStatus());
                userRepository.save(user);
            }

        if(response){
            return true;
        }else{
            throw new EmailValidationException();
        }
    }

    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    @Override
    public Address saveOrUpdateAddress(AddressRequest addressRequest) {
        log.info("saveOrUpdateAddress");
        try {
            User user = userRepository.findByEmail(addressRequest.getUserEmail());
            requestUserIdEqualsTokenUserId(user.getId());
            user.getAddressList().forEach(ad -> ad.setDefaultAddress(false));
            Address address = Address.builder()
                    .street(addressRequest.getStreet())
                    .number(addressRequest.getNumber())
                    .district(addressRequest.getDistrict())
                    .state(addressRequest.getState())
                    .city(addressRequest.getCity())
                    .zipCode(addressRequest.getZipCode())
                    .defaultAddress(true)
                    .complement(addressRequest.getComplement())
                    .id(addressRequest.getId())
                    .deleted(false)
                    .build();

            AddressRequest response = setAddressLatLng(addressRequest);
            address.setLatitude(response.getLatitude());
            address.setLongitude(response.getLongitude());
            address.setUser(user);
            Address persistedAddress = addressRepository.save(address);
            if(addressRequest.getId() != null){
                searchIndexService.deleteAddressRecord(addressRequest.getId());
            }
            searchIndexService.save(persistedAddress, null, null);
            return persistedAddress;
        } catch (Exception ex){
            log.error(ex.getMessage());
            throw new GenericException();
        }
    }

    @Override
    public User findUser(UserRequest userRequest) {
        Optional<User> user = userRepository.findById(userRequest.getId());
        user.ifPresent(user1 -> {
            requestUserIdEqualsTokenUserId(user1.getId());
            if (user1.getCompany() != null) {
                user1.getCompany().setBusinessHours(null);
                user1.getCompany().setAddress(null);
                user1.getCompany().setFee(null);
                user1.getCompany().setDeliveryRanges(null);
                user1.getCompany().setImage(null);
                user1.getCompany().setUserList(null);
                user1.getCompany().setMonthlyFee(null);
            }
            List<Address> adList = user.get().getAddressList().stream().filter(ad -> !ad.getDeleted()).collect(Collectors.toList());
            List<Coupon> cpList = user.get().getCouponList().stream().filter(Coupon::getActive).collect(Collectors.toList());
            user.get().setAddressList(adList);
            user.get().setCouponList(cpList);
        });
        return user.orElseThrow(UserNotFoundException::new);
    }

    @Override
    public User findUserFromEmail(String email) {
        User user = userRepository.findByEmail(email);
        if(user != null) {
            return user;
        }
        return null;
    }

    @Override
    public User findUserFromEmailAndId(UserRequest userRequest) {
        User user = userRepository.findByEmailAndId(userRequest.getEmail(), userRequest.getId());
        if(user != null) {
            return user;
        }
        return null;
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public String deleteAddress(Long id, String email) {
        findUserFromEmail(email);
        Address address = addressRepository.findByIdAndUserEmail(id, email).orElseThrow(AddressNotFoundForUserException::new);
        try {
            searchIndexRepository.deleteByUserAddress(address);
            address.setDeleted(true);
            addressRepository.save(address);
        } catch (Exception ex){
            log.error(ex.getMessage());
            throw new GenericException();
        }
        return "Endereço deletado com sucesso!";
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public User updateUser(UserRequest userRequest, List<MultipartFile> image) throws IOException {
        if (userRequest != null && userRequest.getId() != null) {
            User user = userRepository.findById(userRequest.getId())
                    .orElseThrow(UserNotFoundException::new);
            requestUserIdEqualsTokenUserId(user.getId());
            try {
                updateUserInfo(userRequest, image, user);
                return userRepository.save(user);
            } catch (IncorrectOldPasswordException ex){
                throw ex;
            } catch (Exception ex) {
                log.error(ex.getMessage());
                throw new GenericException();
            }
        } else {
            throw new MandatoryPropertyNotFoundException("Campo obrigatório não informado: [Id de usuário e senha]");
        }
    }

    private void updateUserInfo(UserRequest userRequest, List<MultipartFile> image, User user) throws IOException {

        if (!image.isEmpty()) {
            user.setImage(s3StorageService.uploadImage(image, user.getEmail() + "-pic", "customer-images/").get(0));
        }
        if (userRequest.getName()!= null && !userRequest.getName().equalsIgnoreCase(user.getName())) {
            user.setName(userRequest.getName());
        }
        if (userRequest.getPhone()!= null && !userRequest.getPhone().equalsIgnoreCase(user.getPhone())) {
            user.setPhone(userRequest.getPhone());
        }
        if (userRequest.getCpf()!= null &&  !userRequest.getCpf().equalsIgnoreCase(user.getCpf())) {
            user.setCpf(userRequest.getCpf());
        }
        if (userRequest.getEmail()!= null &&  !userRequest.getEmail().equalsIgnoreCase(user.getEmail())) {
            user.setEmail(userRequest.getEmail());
        }
        if (userRequest.getOldPassword() != null && userRequest.getPassword() != null) {
            if(encoder.matches(userRequest.getOldPassword(), user.getPassword())){
                user.setPassword(encoder.encode(userRequest.getPassword()));
            } else {
                throw new IncorrectOldPasswordException();
            }
        }
    }

    @Override
    public List<UserOrdersResponse> findUserOrders(String email) {
        List<UserOrdersResponse> orderList = new ArrayList<>();
        try {
            User user = userRepository.findByEmail(email);
            requestUserIdEqualsTokenUserId(user.getId());
            List<Sale> sales = saleRepository.findByUser(user);
            sales.forEach(sl -> {
                List<SaleItems> saleItems = saleItemsRepository.findBySale(sl);
                List<OrderProductsDTO> productDetails = new ArrayList<>();
                saleItems.forEach(sli -> {
                    productDetails.add(OrderProductsDTO.builder()
                            .productName(sli.getProductName())
                            .productValue(sli.getAmount())
                            .productQuantity(sli.getProductQtt().toString())
                            .productMeasurement(sli.getProduct().getMeasurementValue() +" "
                                    + sli.getProduct().getProductMeasurementCategory().getName())
                            .productImage(sli.getProduct().getImage())
                            .build());
                });

                Long maxId = sl.getSaleStatusList().stream()
                        .map(SaleStatus::getId)
                        .max(Long::compareTo).orElseThrow(GenericException::new);

                Optional<SaleStatus> saleStatus = saleStatusRepository.findById(maxId);

                CompanyResponse companyResponse = CompanyResponse.builder()
                                            .name(sl.getCompany().getName())
                                            .phone(sl.getCompany().getPhone())
                                            .street(sl.getCompany().getAddress().getStreet())
                                            .number(sl.getCompany().getAddress().getNumber().toString())
                                            .district(sl.getCompany().getAddress().getDistrict())
                                            .build();

                BigDecimal amount = new BigDecimal(sl.getAmount()).subtract(new BigDecimal(sl.getCouponAmount()));
                String finalAmount = amount.compareTo(new BigDecimal("0.0")) < 0 ? "0.0" : amount.toString();
                orderList.add(UserOrdersResponse.builder()
                        .amount(finalAmount)
                        .date(sl.getCreatedDate().toString())
                        .paymentMethod(sl.getPaymentMethod().getPaymentMethod().getDescription())
                        .street(sl.getAddress().getStreet() + " " + sl.getAddress().getNumber().toString())
                        .company(companyResponse)
                        .productsFromOrder(productDetails)
                        .saleId(sl.getId())
                        .orderStatus(saleStatus.orElse(null).getStatus().getDescription())
                        .build());
            });

            return orderList.stream().sorted(Comparator.comparing(UserOrdersResponse::getSaleId).reversed()).collect(Collectors.toList());
        } catch (Exception ex){
            throw new GenericException();
        }
    }

    @Override
    public ReportProblemResponse reportProblem(String saleId) {
        ReportProblemResponse reportProblemResponse = ReportProblemResponse.builder().build();
        try{
            Optional<Sale> sale = saleRepository.findById(Long.valueOf(saleId));
            if(sale.isPresent()){
                reportProblemResponse = ReportProblemResponse.builder()
                        .storePhone(sale.get().getCompany().getPhone())
                        .storeName(sale.get().getCompany().getName())
                        .street(sale.get().getCompany().getAddress().getStreet())
                        .number(sale.get().getCompany().getAddress().getNumber().toString())
                        .district(sale.get().getCompany().getAddress().getDistrict())
                        .build();
            }
        } catch (Exception ex){
            throw new GenericException();
        }
        return reportProblemResponse;
    }

    @Override
    public UserSS authenticated() {
        try {
            return (UserSS) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        }
        catch (Exception e) {
            return null;
        }
    }

    @Override
    public void requestUserIdEqualsTokenUserId(Long requestUserId){
        UserSS user = authenticated();
        if (Objects.isNull(user) ||  !Objects.equals(requestUserId, user.getId())) {
            throw new AuthorizationException("Acesso negado");
        }
    }

    @Override
    public Boolean requestUserIsAssociatedWithCompany(Long companyId){
        log.info(String.format("requestUserIsAssociatedWithCompany - %s", companyId));
        UserSS userSs = authenticated();
        Optional<Company> merchant = companyRepository.findById(companyId);
        log.info(String.format("merchant - %s", merchant.get().getCnpj()));
        Optional<Boolean> response = merchant.map(mer -> {
            if(mer.getUserList().stream().noneMatch(user -> user.getId().equals(userSs.getId()))) {
                throw new AuthorizationException("Acesso negado");
            }
            return true;
        });
        log.info(String.format("requestUserIsAssociatedWithCompany - %s", response.get()));
        return response.get();

    }


    @Override
    public List<User> findUsersByCompanyCnpj(String cnpj) {
        log.info(String.format("findUsersByCompanyCnpj - cnpj: %s", cnpj));
        List<User> userList = userRepository.findByCompany(cnpj);
        if(userList == null){
           throw new UserNotFoundException();
        }
        return userList;
    }

    @Override
    public RegisterResponse saveUserIncomplete(UserRequest userRequest) {
        return executeIncompleteRegister(userRequest);
    }

    @Override
    public List<InfluencerSalesResponse> getSalesInfoInfluencerCupom(String cpf) {
       try {
           List<InfluencerSalesResponse> influencerSalesResponseList = new ArrayList<>();
           List<InfluencerSalesProjection> projectionList = userRepository.getCouponSale(cpf);
           projectionList.forEach(item -> {
               influencerSalesResponseList.add(InfluencerSalesResponse.toInfluencerSalesResponse(item));
               log.info("added - " + item.getProfit() +" - "+ item.getCustomerName());
           });
           log.info(influencerSalesResponseList.toString());
           return influencerSalesResponseList;
       }catch (Exception ex){
           log.error("error executing getSalesInfoInfluencerCupom");
           throw new GenericException();
       }
    }

    private AddressRequest setAddressLatLng(AddressRequest addressRequest) {
        if(addressRequest != null) {
            String apiRequest = getAddressOnApiFormat(addressRequest);
            GeocodeResponse geocodeResponse = geoCodingClient.fetchGeoLocationInfo(apiRequest, kmsService.decode(googleApiKey, keyId));
            if (GeocoderStatus.OK.equals(geocodeResponse.getStatus())) {
                addressRequest.setLatitude(geocodeResponse.getResults().get(0).getGeometry().getLocation().getLat().doubleValue());
                addressRequest.setLongitude(geocodeResponse.getResults().get(0).getGeometry().getLocation().getLng().doubleValue());
            }
        }
        return addressRequest;
    }

    private String getAddressOnApiFormat(AddressRequest userAddress) {
        return userAddress.getStreet().concat("+").concat(userAddress.getNumber().toString())
                .concat("+").concat(userAddress.getCity());
    }

    private HttpHeaders generateToken(User persistedUser) {
        ArrayList<String> strImg = new ArrayList<>();
        if(persistedUser.getImage() != null) {
            for (String s : persistedUser.getImage().split(";")) {
                strImg.add(s);
            }
        }
        List<String> roleList = new ArrayList<>();
        roleList.add(persistedUser.getUserRoleList().get(0).getRole().getName());
        Collection<? extends GrantedAuthority> authorities = (Collection<GrantedAuthority>)(Collection<?>) roleList;
        String cnpj = persistedUser.getCompany() != null ? persistedUser.getCompany().getCnpj() : null;
        String token = jwtUtil.generateToken(persistedUser.getEmail(), authorities,
                persistedUser.getId(), persistedUser.getName(), strImg, cnpj, persistedUser.getStatus());
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization","Bearer " + token);
        headers.add("access-control-expose-headers", "Authorization");
        return headers;
    }

    private RegisterResponse executeIncompleteRegister(UserRequest userRequest) {
        User user = User.builder()
                .expoToken(userRequest.getExpoToken())
                .type(UserTypeEnum.CUSTOMER)
                .build();

        List<UserRole> userRoleList = new ArrayList<>();
        UserRole userRole = UserRole.builder()
                    .role(UserRoleEnum.USER)
                    .user(user)
                    .build();

        userRoleList.add(userRole);

        user.setUserRoleList(userRoleList);
        User persistedUser = userRepository.save(user);

        return RegisterResponse.builder()
                .user(persistedUser)
                .build();
    }


}
