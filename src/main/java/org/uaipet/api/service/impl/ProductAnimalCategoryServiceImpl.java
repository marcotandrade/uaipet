package org.uaipet.api.service.impl;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;
import org.uaipet.api.data.model.ProductAnimalCategory;
import org.uaipet.api.data.repository.ProductAnimalCategoryRepository;
import org.uaipet.api.service.ProductAnimalCategoryService;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
public class ProductAnimalCategoryServiceImpl implements ProductAnimalCategoryService {

    private final ProductAnimalCategoryRepository repository;

    @Autowired
    public ProductAnimalCategoryServiceImpl(ProductAnimalCategoryRepository repository){
        this.repository = repository;
    }


    @Override
    public List<ProductAnimalCategory> findAll() {
        return this.repository.findAll();
    }

    @Override
    public ProductAnimalCategory findOne(Long id) {

        Optional< ProductAnimalCategory > product = this.repository.findById(id);

        if(!product.isPresent()){
            throw new EmptyResultDataAccessException(String.format("Animal não encontado com o id %s", id), 1);
        }

        return product.get();
    }

    @Override
    @Transactional( rollbackOn = Exception.class )
    public ProductAnimalCategory save(ProductAnimalCategory product) {

        return this.repository.save( product );
    }

    @Override
    @Transactional( rollbackOn = Exception.class )
    public void update(Long id, ProductAnimalCategory product) {

        ProductAnimalCategory productSave = this.findOne( id );
        //copia as propriedades do objeto 1 para o 2 ignorando o ID
        BeanUtils.copyProperties( product, productSave, "id" );
        this.repository.save( productSave );
    }
}
