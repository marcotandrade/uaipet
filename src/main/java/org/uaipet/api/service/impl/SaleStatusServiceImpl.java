package org.uaipet.api.service.impl;

import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.uaipet.api.data.enums.SaleStatusEnum;
import org.uaipet.api.data.model.Sale;
import org.uaipet.api.data.model.SaleStatus;
import org.uaipet.api.data.repository.SaleStatusRepository;
import org.uaipet.api.service.SaleStatusService;
import org.uaipet.api.service.exception.ForbiddenStatusChangeException;
import org.uaipet.api.service.exception.SaleNotFoundException;

import java.util.Optional;

import static org.uaipet.api.data.enums.SaleStatusEnum.CANCELED;
import static org.uaipet.api.data.enums.SaleStatusEnum.FINISHED;
import static org.uaipet.api.data.enums.SaleStatusEnum.PREPARING_PRODUCT;
import static org.uaipet.api.data.enums.SaleStatusEnum.SENT_TO_DELIVERY;
import static org.uaipet.api.data.enums.SaleStatusEnum.WAITING_CONFIRMATION;

@RequiredArgsConstructor
@Service
public class SaleStatusServiceImpl implements SaleStatusService {

    private static final Logger log = LoggerFactory.getLogger(SaleStatusServiceImpl.class);

    private final SaleStatusRepository saleStatusRepository;

    @Transactional(rollbackFor = Exception.class)
    @Override
    public Boolean moveOrderStatus(Sale sale, Boolean isCanceling, String description) {
        log.info("moveOrderStatus");
        Optional<Long> maxId = sale.getSaleStatusList().stream()
                .map(SaleStatus::getId)
                .max(Long::compareTo);

        Optional<SaleStatus> saleStatus = sale.getSaleStatusList().stream()
                .filter(st -> st.getId().equals(maxId.get()))
                .findFirst();

        saleStatus.ifPresentOrElse(saleSt -> createNextStatus(saleSt, isCanceling, description),
                () -> saleStatus.orElseThrow(SaleNotFoundException::new));
        return true;
    }


    void createNextStatus(SaleStatus saleStatus, Boolean isCanceling, String description){
        if(isCanceling) {
            if(saleStatus.getStatus().equals(WAITING_CONFIRMATION)) {
                moveToSaleStatus(saleStatus, CANCELED, description);
            } else {
                throw new ForbiddenStatusChangeException();
            }
        } else {
            switch (saleStatus.getStatus()) {
                case WAITING_CONFIRMATION:
                    log.info("");
                    moveToSaleStatus(saleStatus, PREPARING_PRODUCT,null);
                    break;
                case PREPARING_PRODUCT:
                    log.info("");
                    moveToSaleStatus(saleStatus, SENT_TO_DELIVERY,null);
                    break;
                case SENT_TO_DELIVERY:
                    log.info("");
                    moveToSaleStatus(saleStatus, FINISHED,null);
                    break;
                default:
                    break;
            }
        }
    }

    void moveToSaleStatus(SaleStatus saleStatus, SaleStatusEnum stEnum, String description){
        SaleStatus status = SaleStatus.builder()
                .sale(saleStatus.getSale())
                .status(stEnum)
                .description(description)
                .build();

        saleStatusRepository.save(status);
    }

}
