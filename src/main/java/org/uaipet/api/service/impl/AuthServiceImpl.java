package org.uaipet.api.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.uaipet.api.data.model.User;
import org.uaipet.api.data.repository.UserRepository;
import org.uaipet.api.dto.MailDTO;
import org.uaipet.api.service.AuthService;
import org.uaipet.api.service.EmailService;
import org.uaipet.api.service.UserService;
import org.uaipet.api.service.exception.UserNotFoundException;
import org.uaipet.api.utils.RandomGenerator;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Random;

@RequiredArgsConstructor
@Service
public class AuthServiceImpl implements AuthService {

    private static final String UAI_PET_ORG_EMAIL = "noreply@uaipetapp.com.br";

    private final UserService service;
    private final UserRepository userRepository;
    private final BCryptPasswordEncoder encoder;
    private final EmailService emailService;

    private Random rand = new Random();

    public void     sendNewPassword(String email) {

        User user  = this.service.findUserFromEmail(email);

        if (null == user) {
            throw new UserNotFoundException();
        }

        String newPass = newPassword();
        user.setPassword(encoder.encode(newPass));

        userRepository.save(user);

        Map<String, Object> userinfo = new HashMap<>();
        userinfo.put("newPass", newPass);
        userinfo.put("name", user.getName());

        MailDTO mailDTO = MailDTO.builder()
                .mailFrom(UAI_PET_ORG_EMAIL)
                .mailTo(user.getEmail())
                .subject("Nova senha")
                .templateName("user-new-password")
                .props(userinfo)
                .build();

        emailService.sendSimpleMessage(mailDTO);
    }

    private String newPassword() {
        return RandomGenerator.generateRandomIntegerCode();
    }

    private char randomChar() {
        int opt = rand.nextInt(3);
        if (opt == 0) {
            return (char) (rand.nextInt(10) + 48);
        }
        else if (opt == 1) {
            return (char) (rand.nextInt(26) + 65);
        }
        else {
            return (char) (rand.nextInt(26) + 97);
        }
    }
}
