package org.uaipet.api.service.exception;

public class CompanyAlreadyExistsException extends RuntimeException {

    public CompanyAlreadyExistsException() {
        super();
    }

    public CompanyAlreadyExistsException(String message) {
        super(message);
    }
}
