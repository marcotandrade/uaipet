package org.uaipet.api.service.exception;

public class WrongAuthorizationTokenFormatException extends RuntimeException {

    public WrongAuthorizationTokenFormatException() {
        super();
    }
}
