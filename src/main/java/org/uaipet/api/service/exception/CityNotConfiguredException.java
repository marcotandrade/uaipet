package org.uaipet.api.service.exception;


import java.util.function.Supplier;

public class CityNotConfiguredException extends RuntimeException {

    public CityNotConfiguredException() {
        super();
    }

    public CityNotConfiguredException(String msg) {
        super(msg);
    }

    public CityNotConfiguredException(String msg, Throwable cause) {
        super(msg, cause);
    }

}