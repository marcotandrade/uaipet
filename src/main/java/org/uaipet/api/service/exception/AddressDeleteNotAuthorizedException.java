package org.uaipet.api.service.exception;

public class AddressDeleteNotAuthorizedException extends RuntimeException{
    public AddressDeleteNotAuthorizedException() {
        super();
    }

    public AddressDeleteNotAuthorizedException(String message) {
        super(message);
    }
}
