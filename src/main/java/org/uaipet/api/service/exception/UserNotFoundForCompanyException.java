package org.uaipet.api.service.exception;

public class UserNotFoundForCompanyException extends RuntimeException {

    public UserNotFoundForCompanyException() {
        super();
    }
}
