package org.uaipet.api.service.exception;

public class AddressNotFoundForUserException extends RuntimeException{
    public AddressNotFoundForUserException() {
        super();
    }

    public AddressNotFoundForUserException(String message) {
        super(message);
    }
}
