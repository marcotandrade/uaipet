package org.uaipet.api.service.exception;

import java.util.function.Supplier;

public class CompanyException extends RuntimeException {

    public CompanyException() {
        super();
    }

    public CompanyException(String message) {
        super(message);
    }
}
