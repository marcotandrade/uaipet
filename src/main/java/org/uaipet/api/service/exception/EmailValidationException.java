package org.uaipet.api.service.exception;

public class EmailValidationException extends RuntimeException {
    public EmailValidationException() {
        super();
    }

    public EmailValidationException(String message) {
        super(message);
    }
}
