package org.uaipet.api.service.exception;

public class ProductNotAvailableException extends RuntimeException {
    public ProductNotAvailableException() {
        super();
    }

    public ProductNotAvailableException(String message) {
        super(message);
    }
}
