package org.uaipet.api.service.exception;

public class ProductException extends RuntimeException {
    public ProductException() {
        super();
    }

    public ProductException(String message) {
        super(message);
    }
}
