package org.uaipet.api.service.exception;

public class UserNotFoundException extends RuntimeException {

    public UserNotFoundException() {
        super();
    }
}
