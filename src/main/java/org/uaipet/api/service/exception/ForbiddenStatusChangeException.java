package org.uaipet.api.service.exception;

public class ForbiddenStatusChangeException extends RuntimeException{

    public ForbiddenStatusChangeException() {
        super();
    }
}
