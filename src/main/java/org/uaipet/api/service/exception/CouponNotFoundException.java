package org.uaipet.api.service.exception;

public class CouponNotFoundException extends RuntimeException {

    public CouponNotFoundException() {
        super();
    }

    public CouponNotFoundException(String message) {
        super(message);
    }
}
