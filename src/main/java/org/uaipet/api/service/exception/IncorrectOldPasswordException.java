package org.uaipet.api.service.exception;

public class IncorrectOldPasswordException extends RuntimeException {

    public IncorrectOldPasswordException() {
        super();
    }
}
