package org.uaipet.api.service.exception.handler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.uaipet.api.service.exception.*;

import javax.servlet.http.HttpServletRequest;

@ControllerAdvice
public class ResourceExceptionHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger(ResourceExceptionHandler.class);

    @Autowired
    private MessageSource messageSource;

    @ExceptionHandler(EmailAlreadyExistsException.class)
    public ResponseEntity EmailAlreadyExistsException(EmailAlreadyExistsException ex, HttpServletRequest request) {

        LOGGER.error("CustomerException: " + ex.getMessage());

        return ResponseEntity
                .status(HttpStatus.UNAUTHORIZED)
                .body(StandardError.builder()
                        .timestamp(System.currentTimeMillis())
                        .status(HttpStatus.UNAUTHORIZED.value())
                        .message(messageSource.getMessage("email.already.exists", null, LocaleContextHolder.getLocale()))
                        .error(ex.getMessage())
                        .path(request.getRequestURI())
                        .build()
                );

    }

    @ExceptionHandler(GenericException.class)
    public ResponseEntity GenericException(GenericException ex, HttpServletRequest request) {

        LOGGER.error("GenericException: " + ex.getMessage());

        return ResponseEntity
                .status(HttpStatus.INTERNAL_SERVER_ERROR)
                .body(StandardError.builder()
                        .timestamp(System.currentTimeMillis())
                        .status(HttpStatus.INTERNAL_SERVER_ERROR.value())
                        .message(messageSource.getMessage("generic.error.message", null, LocaleContextHolder.getLocale()))
                        .error(ex.getMessage())
                        .path(request.getRequestURI())
                        .build()
                );

    }


    @ExceptionHandler(ConversionException.class)
    public ResponseEntity ConversionException(ConversionException ex, HttpServletRequest request) {

        LOGGER.error("ConversionException: " + ex.getMessage());

        return ResponseEntity
                .status(HttpStatus.INTERNAL_SERVER_ERROR)
                .body(StandardError.builder()
                        .timestamp(System.currentTimeMillis())
                        .status(HttpStatus.INTERNAL_SERVER_ERROR.value())
                        .message(messageSource.getMessage("converting.error.message", null, LocaleContextHolder.getLocale()))
                        .error(ex.getMessage())
                        .path(request.getRequestURI())
                        .build()
                );

    }

    @ExceptionHandler(AddressNotFoundForUserException.class)
    public ResponseEntity AddressNotFoundForUserException(AddressNotFoundForUserException ex, HttpServletRequest request) {

        LOGGER.error("AddressNotFoundForUserException: " + ex.getMessage());

        return ResponseEntity
                .status(HttpStatus.UNAUTHORIZED)
                .body(StandardError.builder()
                        .timestamp(System.currentTimeMillis())
                        .status(HttpStatus.UNAUTHORIZED.value())
                        .message(messageSource.getMessage("address.not.found", null, LocaleContextHolder.getLocale()))
                        .error(ex.getMessage())
                        .path(request.getRequestURI())
                        .build()
                );

    }

    @ExceptionHandler(AddressDeleteNotAuthorizedException.class)
    public ResponseEntity AddressDeleteNotAuthorizedException(AddressDeleteNotAuthorizedException ex, HttpServletRequest request) {

        LOGGER.error("AddressDeleteNotAuthorizedException: " + ex.getMessage());

        return ResponseEntity
                .status(HttpStatus.UNAUTHORIZED)
                .body(StandardError.builder()
                        .timestamp(System.currentTimeMillis())
                        .status(HttpStatus.UNAUTHORIZED.value())
                        .message(messageSource.getMessage("address.deletion.not.authorized", null, LocaleContextHolder.getLocale()))
                        .error(ex.getMessage())
                        .path(request.getRequestURI())
                        .build()
                );

    }

    @ExceptionHandler({EmptyResultDataAccessException.class})
    public ResponseEntity<Object> handleEmptyResultDataAccessException(EmptyResultDataAccessException ex, HttpServletRequest request) {

        LOGGER.error("EmptyResultDataAccessException: " + ex.getMessage());

        return ResponseEntity
                .status(HttpStatus.NOT_FOUND)
                .body(StandardError.builder()
                        .timestamp(System.currentTimeMillis())
                        .status(HttpStatus.NOT_FOUND.value())
                        .message(messageSource.getMessage("record-not-found", null, LocaleContextHolder.getLocale()))
                        .error(ex.getMessage())
                        .path(request.getRequestURI())
                        .build()
                );
    }

    @ExceptionHandler(ProductImageException.class)
    public ResponseEntity ProductImageException(ProductImageException ex, HttpServletRequest request) {

        LOGGER.error("ProductImageException: " + ex.getMessage());

        return ResponseEntity
                .status(HttpStatus.INTERNAL_SERVER_ERROR)
                .body(StandardError.builder()
                        .timestamp(System.currentTimeMillis())
                        .status(HttpStatus.INTERNAL_SERVER_ERROR.value())
                        .message(messageSource.getMessage("product.image.exception", null, LocaleContextHolder.getLocale()))
                        .error(ex.getMessage())
                        .path(request.getRequestURI())
                        .build()
                );

    }

    @ExceptionHandler(ProductException.class)
    public ResponseEntity ProductException(ProductException ex, HttpServletRequest request) {

        LOGGER.error("ProductImageException: " + ex.getMessage());

        return ResponseEntity
                .status(HttpStatus.UNAUTHORIZED)
                .body(StandardError.builder()
                        .timestamp(System.currentTimeMillis())
                        .status(HttpStatus.UNAUTHORIZED.value())
                        .message(ex.getMessage())
                        .error(ex.getMessage())
                        .path(request.getRequestURI())
                        .build()
                );
    }


    @ExceptionHandler(CompanyNotFoundException.class)
    public ResponseEntity CompanyNotFoundException(CompanyNotFoundException ex, HttpServletRequest request) {

        LOGGER.error("CompanyNotFoundException: " + ex.getMessage());

        return ResponseEntity
                .status(HttpStatus.NOT_FOUND)
                .body(StandardError.builder()
                        .timestamp(System.currentTimeMillis())
                        .status(HttpStatus.NOT_FOUND.value())
                        .message(ex.getMessage())
                        .error(ex.getMessage())
                        .path(request.getRequestURI())
                        .build()
                );
    }


    @ExceptionHandler(CompanyAlreadyExistsException.class)
    public ResponseEntity CompanyAlreadyExistsException(CompanyAlreadyExistsException ex, HttpServletRequest request) {

        LOGGER.error("CompanyAlreadyExistsException: " + ex.getMessage());

        return ResponseEntity
                .status(HttpStatus.UNAUTHORIZED)
                .body(StandardError.builder()
                        .timestamp(System.currentTimeMillis())
                        .status(HttpStatus.UNAUTHORIZED.value())
                        .message(messageSource.getMessage("company.already.exists", null, LocaleContextHolder.getLocale()))
                        .error(ex.getMessage())
                        .path(request.getRequestURI())
                        .build()
                );
    }

    @ExceptionHandler(CompanyException.class)
    public ResponseEntity CompanyException(CompanyException ex, HttpServletRequest request) {

        LOGGER.error("CompanyException: " + ex.getMessage());

        return ResponseEntity
                .status(HttpStatus.UNAUTHORIZED)
                .body(StandardError.builder()
                        .timestamp(System.currentTimeMillis())
                        .status(HttpStatus.UNAUTHORIZED.value())
                        .message(ex.getMessage())
                        .error(ex.getMessage())
                        .path(request.getRequestURI())
                        .build()
                );
    }


    @ExceptionHandler(MandatoryPropertyNotFoundException.class)
    public ResponseEntity MandatoryPropertyNotFoundException(MandatoryPropertyNotFoundException ex, HttpServletRequest request) {

        LOGGER.error("MandatoryPropertyNotFoundException: " + ex.getMessage());

        return ResponseEntity
                .status(HttpStatus.UNAUTHORIZED)
                .body(StandardError.builder()
                        .timestamp(System.currentTimeMillis())
                        .status(HttpStatus.UNAUTHORIZED.value())
                        .message(ex.getMessage())
                        .error(ex.getMessage())
                        .path(request.getRequestURI())
                        .build()
                );
    }

    @ExceptionHandler(UserNotFoundForCompanyException.class)
    public ResponseEntity UserNotFoundForCompanyException(UserNotFoundForCompanyException ex, HttpServletRequest request) {

        LOGGER.error("UserNotFoundForCompanyException: " + ex.getMessage());

        return ResponseEntity
                .status(HttpStatus.NOT_FOUND)
                .body(StandardError.builder()
                        .timestamp(System.currentTimeMillis())
                        .status(HttpStatus.NOT_FOUND.value())
                        .message(messageSource.getMessage("user.not.found.for.company", null, LocaleContextHolder.getLocale()))
                        .error(ex.getMessage())
                        .path(request.getRequestURI())
                        .build()
                );
    }

    @ExceptionHandler(SaleNotFoundException.class)
    public ResponseEntity SaleNotFoundException(SaleNotFoundException ex, HttpServletRequest request) {

        LOGGER.error("SaleNotFoundException: " + ex.getMessage());

        return ResponseEntity
                .status(HttpStatus.NOT_FOUND)
                .body(StandardError.builder()
                        .timestamp(System.currentTimeMillis())
                        .status(HttpStatus.NOT_FOUND.value())
                        .message(messageSource.getMessage("sale.not.found.for.company", null, LocaleContextHolder.getLocale()))
                        .error(ex.getMessage())
                        .path(request.getRequestURI())
                        .build()
                );
    }

    @ExceptionHandler(ForbiddenStatusChangeException.class)
    public ResponseEntity ForbiddenStatusChangeException(ForbiddenStatusChangeException ex, HttpServletRequest request) {

        LOGGER.error("ForbiddenStatusChangeException: " + ex.getMessage());

        return ResponseEntity
                .status(HttpStatus.FORBIDDEN)
                .body(StandardError.builder()
                        .timestamp(System.currentTimeMillis())
                        .status(HttpStatus.FORBIDDEN.value())
                        .message(messageSource.getMessage("status.change.forbidden", null, LocaleContextHolder.getLocale()))
                        .error(ex.getMessage())
                        .path(request.getRequestURI())
                        .build()
                );
    }


    @ExceptionHandler(UserNotFoundException.class)
    public ResponseEntity UserNotFoundException(UserNotFoundException ex, HttpServletRequest request) {

        LOGGER.error("UserNotFoundException: " + ex.getMessage());

        return ResponseEntity
                .status(HttpStatus.NOT_FOUND)
                .body(StandardError.builder()
                        .timestamp(System.currentTimeMillis())
                        .status(HttpStatus.NOT_FOUND.value())
                        .message(messageSource.getMessage("user.not.found", null, LocaleContextHolder.getLocale()))
                        .error(ex.getMessage())
                        .path(request.getRequestURI())
                        .build()
                );
    }

    @ExceptionHandler(AddressNotInRangeException.class)
    public ResponseEntity AddressNotInRangeException(AddressNotInRangeException ex, HttpServletRequest request) {

        LOGGER.error("AddressNotInRangeException: " + ex.getMessage());

        return ResponseEntity
                .status(HttpStatus.NOT_FOUND)
                .body(StandardError.builder()
                        .timestamp(System.currentTimeMillis())
                        .status(HttpStatus.NOT_FOUND.value())
                        .message(messageSource.getMessage("address.out.of.delivery.range", null, LocaleContextHolder.getLocale()))
                        .error(ex.getMessage())
                        .path(request.getRequestURI())
                        .build()
                );
    }

    @ExceptionHandler(AuthorizationException.class)
    public ResponseEntity AuthorizationException(AuthorizationException ex, HttpServletRequest request) {

        LOGGER.error("AuthorizationException: " + ex.getMessage());

        return ResponseEntity
                .status(HttpStatus.FORBIDDEN)
                .body(StandardError.builder()
                        .timestamp(System.currentTimeMillis())
                        .status(HttpStatus.FORBIDDEN.value())
                        .message(messageSource.getMessage("forbidden.access", null, LocaleContextHolder.getLocale()))
                        .error(ex.getMessage())
                        .path(request.getRequestURI())
                        .build()
                );
    }



    @ExceptionHandler(EmailNotValidatedException.class)
    public ResponseEntity EmailNotValidatedException(EmailNotValidatedException ex, HttpServletRequest request) {

        LOGGER.error("EmailNotValidatedException: " + ex.getMessage());

        return ResponseEntity
                .status(HttpStatus.UNAUTHORIZED)
                .body(StandardError.builder()
                        .timestamp(System.currentTimeMillis())
                        .status(HttpStatus.UNAUTHORIZED.value())
                        .message(messageSource.getMessage("email.not.validated", null, LocaleContextHolder.getLocale()))
                        .error(ex.getMessage())
                        .path(request.getRequestURI())
                        .build()
                );
    }



    @ExceptionHandler(IncorrectOldPasswordException.class)
    public ResponseEntity IncorrectOldPasswordException(IncorrectOldPasswordException ex, HttpServletRequest request) {

        LOGGER.error("IncorrectOldPasswordException: " + ex.getMessage());

        return ResponseEntity
                .status(HttpStatus.UNAUTHORIZED)
                .body(StandardError.builder()
                        .timestamp(System.currentTimeMillis())
                        .status(HttpStatus.UNAUTHORIZED.value())
                        .message(messageSource.getMessage("last.password.is.incorrect", null, LocaleContextHolder.getLocale()))
                        .error(ex.getMessage())
                        .path(request.getRequestURI())
                        .build()
                );
    }

    @ExceptionHandler(WrongAuthorizationTokenFormatException.class)
    public ResponseEntity WrongAuthorizationTokenFormatException(WrongAuthorizationTokenFormatException ex, HttpServletRequest request) {

        LOGGER.error("WrongAuthorizationTokenFormatException: " + ex.getMessage());

        return ResponseEntity
                .status(HttpStatus.UNAUTHORIZED)
                .body(StandardError.builder()
                        .timestamp(System.currentTimeMillis())
                        .status(HttpStatus.UNAUTHORIZED.value())
                        .message(messageSource.getMessage("authorization.without.cnpj", null, LocaleContextHolder.getLocale()))
                        .error(ex.getMessage())
                        .path(request.getRequestURI())
                        .build()
                );
    }

    @ExceptionHandler(CityNotConfiguredException.class)
    public ResponseEntity CityNotConfiguredException(CityNotConfiguredException ex, HttpServletRequest request) {

        LOGGER.error("CityNotConfiguredException: " + ex.getMessage());

        return ResponseEntity
                .status(HttpStatus.UNAUTHORIZED)
                .body(StandardError.builder()
                        .timestamp(System.currentTimeMillis())
                        .status(HttpStatus.UNAUTHORIZED.value())
                        .message(messageSource.getMessage("city.not.configured", null, LocaleContextHolder.getLocale()))
                        .error(ex.getMessage())
                        .path(request.getRequestURI())
                        .build()
                );
    }
    @ExceptionHandler(ProductNotFoundException.class)
    public ResponseEntity ProductNotFoundException(ProductNotFoundException ex, HttpServletRequest request) {

        LOGGER.error("ProductNotFoundException: " + ex.getMessage());

        return ResponseEntity
                .status(HttpStatus.NOT_FOUND)
                .body(StandardError.builder()
                        .timestamp(System.currentTimeMillis())
                        .status(HttpStatus.NOT_FOUND.value())
                        .message(messageSource.getMessage("product.not.found", null, LocaleContextHolder.getLocale()))
                        .error(ex.getMessage())
                        .path(request.getRequestURI())
                        .build()
                );
    }

    @ExceptionHandler(EmailValidationException.class)
    public ResponseEntity EmailValidationException(EmailValidationException ex, HttpServletRequest request) {

        LOGGER.error("EmailValidationException: " + ex.getMessage());

        return ResponseEntity
                .status(HttpStatus.UNAUTHORIZED)
                .body(StandardError.builder()
                        .timestamp(System.currentTimeMillis())
                        .status(HttpStatus.UNAUTHORIZED.value())
                        .message(messageSource.getMessage("incorret.email.validation", null, LocaleContextHolder.getLocale()))
                        .error(ex.getMessage())
                        .path(request.getRequestURI())
                        .build()
                );
    }


    @ExceptionHandler(ImageMaxSizeException.class)
    public ResponseEntity ImageMaxSizeException(ImageMaxSizeException ex, HttpServletRequest request) {

        LOGGER.error("ImageMaxSizeException: " + ex.getMessage());

        return ResponseEntity
                .status(HttpStatus.UNAUTHORIZED)
                .body(StandardError.builder()
                        .timestamp(System.currentTimeMillis())
                        .status(HttpStatus.UNAUTHORIZED.value())
                        .message(messageSource.getMessage("image.too.large", null, LocaleContextHolder.getLocale()))
                        .error(ex.getMessage())
                        .path(request.getRequestURI())
                        .build()
                );
    }

    @ExceptionHandler(ProductNotAvailableException.class)
    public ResponseEntity ProductNotAvailableException(ProductNotAvailableException ex, HttpServletRequest request) {

        LOGGER.error("ProductNotAvailableException: " + ex.getMessage());

        return ResponseEntity
                .status(HttpStatus.UNAUTHORIZED)
                .body(StandardError.builder()
                        .timestamp(System.currentTimeMillis())
                        .status(HttpStatus.UNAUTHORIZED.value())
                        .message(messageSource.getMessage("product.unavailable", null, LocaleContextHolder.getLocale()))
                        .error(ex.getMessage())
                        .path(request.getRequestURI())
                        .build()
                );
    }


    @ExceptionHandler(OrderMinimalAmountException.class)
    public ResponseEntity OrderMinimalAmountException(OrderMinimalAmountException ex, HttpServletRequest request) {

        LOGGER.error("OrderMinimalAmountException: " + ex.getMessage());

        return ResponseEntity
                .status(HttpStatus.UNAUTHORIZED)
                .body(StandardError.builder()
                        .timestamp(System.currentTimeMillis())
                        .status(HttpStatus.UNAUTHORIZED.value())
                        .message(messageSource.getMessage("order.minimum.amount.not.achieved", null, LocaleContextHolder.getLocale()))
                        .error(ex.getMessage())
                        .path(request.getRequestURI())
                        .build()
                );
    }

    @ExceptionHandler(CouponNotFoundException.class)
    public ResponseEntity CouponNotFoundException(CouponNotFoundException ex, HttpServletRequest request) {

        LOGGER.error("CouponNotFoundException: " + ex.getMessage());

        return ResponseEntity
                .status(HttpStatus.NOT_FOUND)
                .body(StandardError.builder()
                        .timestamp(System.currentTimeMillis())
                        .status(HttpStatus.NOT_FOUND.value())
                        .message(messageSource.getMessage("coupon.not.found", null, LocaleContextHolder.getLocale()))
                        .error(ex.getMessage())
                        .path(request.getRequestURI())
                        .build()
                );
    }

    @ExceptionHandler(CompanyClosedException.class)
    public ResponseEntity CompanyClosedException(CompanyClosedException ex, HttpServletRequest request) {

        LOGGER.error("CompanyClosedException: " + ex.getMessage());

        return ResponseEntity
                .status(HttpStatus.UNAUTHORIZED)
                .body(StandardError.builder()
                        .timestamp(System.currentTimeMillis())
                        .status(HttpStatus.UNAUTHORIZED.value())
                        .message(messageSource.getMessage("company.closed", null, LocaleContextHolder.getLocale()))
                        .error(ex.getMessage())
                        .path(request.getRequestURI())
                        .build()
                );
    }




}
