package org.uaipet.api.service.exception;

public class AddressNotInRangeException extends RuntimeException {
    public AddressNotInRangeException() {
        super();
    }
}
