package org.uaipet.api.service.exception;

public class MandatoryPropertyNotFoundException extends RuntimeException {
    public MandatoryPropertyNotFoundException() {
        super();
    }

    public MandatoryPropertyNotFoundException(String message) {
        super(message);
    }
}
