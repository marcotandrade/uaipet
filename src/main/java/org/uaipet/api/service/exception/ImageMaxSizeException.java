package org.uaipet.api.service.exception;

public class ImageMaxSizeException extends RuntimeException{
    public ImageMaxSizeException() { super();
    }

    public ImageMaxSizeException(String message) {
        super(message);
    }
}
