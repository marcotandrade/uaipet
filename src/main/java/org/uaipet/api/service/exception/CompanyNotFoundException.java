package org.uaipet.api.service.exception;

public class CompanyNotFoundException extends RuntimeException {

    public CompanyNotFoundException() {
        super();
    }

    public CompanyNotFoundException(String message) {
        super(message);
    }
}
