package org.uaipet.api.service.exception;

public class EmailNotValidatedException extends RuntimeException {
    public EmailNotValidatedException() {
        super();
    }

    public EmailNotValidatedException(String message) {
        super(message);
    }
}
