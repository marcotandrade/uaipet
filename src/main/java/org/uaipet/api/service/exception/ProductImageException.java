package org.uaipet.api.service.exception;

public class ProductImageException extends RuntimeException{
    public ProductImageException() {
        super();
    }

    public ProductImageException(String message) {
        super(message);
    }
}
