package org.uaipet.api.service;

import org.uaipet.api.data.model.ProductAnimalCategory;

import java.util.List;

public interface ProductAnimalCategoryService {

    List< ProductAnimalCategory > findAll();

    ProductAnimalCategory findOne(Long id);

    ProductAnimalCategory save(ProductAnimalCategory productAnimalCategory);

    void update(Long id, ProductAnimalCategory productAnimalCategory);

}
