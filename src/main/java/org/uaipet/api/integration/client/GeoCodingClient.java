package org.uaipet.api.integration.client;

import com.google.code.geocoder.model.GeocodeResponse;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient(value = "GeoCoding", url = "${cloud.google.geo-coding-api.host}")
public interface GeoCodingClient {

    @RequestMapping(method = RequestMethod.GET, path = "${cloud.google.geo-coding-api-address.url}")
    GeocodeResponse fetchGeoLocationInfo(@PathVariable("fullAddress") String address, @PathVariable("apiKey") String apiKey);

    @RequestMapping(method = RequestMethod.GET, path = "${cloud.google.geo-coding-api-address-distance.url}")
    Object fetchDistanceInfo(@PathVariable("origins") String originAddress,
                                               @PathVariable("destinations") String destinationAddress,
                                               @PathVariable("apiKey") String apiKey);

}
