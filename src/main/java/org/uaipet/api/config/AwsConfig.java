package org.uaipet.api.config;

import com.amazonaws.AmazonClientException;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.auth.EnvironmentVariableCredentialsProvider;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.kms.AWSKMS;
import com.amazonaws.services.kms.AWSKMSClient;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
public class AwsConfig {

    @Value("${cloud.aws.accessKey}")
    private String accessKey;

    @Value("${cloud.aws.secretKey}")
    private String secretKey;

    @Value("${cloud.aws.region}")
    private String region;


    @Bean
    @Profile({"qa", "dev"})
    public AWSCredentials awsCredentials() {
        return new BasicAWSCredentials(accessKey,secretKey);
    }

    @Bean
    @Profile({"qa","dev"})
    public AmazonS3 amazonS3(AWSCredentials awsCredentials){
        return AmazonS3ClientBuilder.standard()
                .withCredentials(new AWSStaticCredentialsProvider(awsCredentials()))
                .withRegion(region).build();
    }

    @Bean
    @Profile({"qa","dev"})
    public AWSKMS kmsDevConfig(AWSCredentials awsCredentials){
        return AWSKMSClient.builder()
                .withCredentials(new AWSStaticCredentialsProvider(awsCredentials()))
                .withRegion(Regions.SA_EAST_1)
                .build();
    }

    @Bean
    @Profile("prod")
    public AWSCredentials awsCredentialsProd() {
        AWSCredentials credentials = null;
        try {
            credentials = new EnvironmentVariableCredentialsProvider().getCredentials();
        } catch (Exception e) {
            throw new AmazonClientException("Cannot Load Credentials");
        }
        return credentials;
    }

    @Bean
    @Profile("prod")
    public AmazonS3 amazonS3ProdConfig(AWSCredentials awsCredentials){
        return AmazonS3ClientBuilder.standard()
                .withCredentials(new AWSStaticCredentialsProvider(awsCredentialsProd()))
                .withRegion(Regions.SA_EAST_1)
                .build();
    }


    @Bean
    @Profile("prod")
    public AWSKMS kmsProdConfig(AWSCredentials awsCredentials){
        return AWSKMSClient.builder()
                .withCredentials(new AWSStaticCredentialsProvider(awsCredentialsProd()))
                .withRegion(Regions.SA_EAST_1)
                .build();
    }

}
