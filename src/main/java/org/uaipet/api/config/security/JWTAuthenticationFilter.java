package org.uaipet.api.config.security;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.uaipet.api.dto.CredentialsDTO;

import javax.servlet.FilterChain;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

public class JWTAuthenticationFilter extends UsernamePasswordAuthenticationFilter {

    private AuthenticationManager authenticationManager;

    private JWTUtil jwtUtil;

    public JWTAuthenticationFilter(AuthenticationManager authenticationManager, JWTUtil jwtUtil) {
        setAuthenticationFailureHandler(new JWTAuthenticationFailureHandler());
        this.authenticationManager = authenticationManager;
        this.jwtUtil = jwtUtil;
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest req,
                                                HttpServletResponse res) throws AuthenticationException {

        try {
            CredentialsDTO creds = new ObjectMapper()
                    .readValue(req.getInputStream(), CredentialsDTO.class);

            UsernamePasswordAuthenticationToken authToken = new UsernamePasswordAuthenticationToken(creds.getEmail(),
                    creds.getPassword(), new ArrayList<>());

            return authenticationManager.authenticate(authToken);
        }
        catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    protected void successfulAuthentication(HttpServletRequest req,
                                            HttpServletResponse res,
                                            FilterChain chain,
                                            Authentication auth) {

        String username = ((UserSS) auth.getPrincipal()).getUsername();
        Long userId = ((UserSS) auth.getPrincipal()).getUserId();
        String name = ((UserSS) auth.getPrincipal()).getName();
        String cnpj = ((UserSS) auth.getPrincipal()).getCnpj();
        String status = ((UserSS) auth.getPrincipal()).getStatus();
        ArrayList<String> image = ((UserSS) auth.getPrincipal()).getImage();
        Collection<? extends GrantedAuthority> authorities = ((UserSS) auth.getPrincipal()).getAuthorities();

        String token = jwtUtil.generateToken(username, authorities, userId, name, image, cnpj, status);
        res.addHeader("Authorization", "Bearer " + token);
    }

    private class JWTAuthenticationFailureHandler implements AuthenticationFailureHandler {

        @Override
        public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response, AuthenticationException exception)
                throws IOException {
            String exceptionMessage;
            if(exception instanceof BadCredentialsException){
                exceptionMessage = "Email e/ou senha incorretos!";
            }else{
                exceptionMessage = exception.getMessage();
            }
            response.setStatus(401);
            response.setContentType("application/json");
            response.getWriter().append(json(exceptionMessage));
        }

        private String json(String errorMessage) {
            long date = new Date().getTime();
            return "{\"timestamp\": " + date + ", "
                    + "\"status\": 401, "
                    + "\"error\": \"Not authorized\", "
                    + "\"message\": \"" + errorMessage + "\","
                    + "\"path\": \"/login\"}";
        }
    }
}