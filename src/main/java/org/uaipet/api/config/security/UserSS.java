package org.uaipet.api.config.security;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.uaipet.api.data.enums.UserRoleEnum;

import java.util.ArrayList;
import java.util.Collection;

public class UserSS implements UserDetails {
    private static final long serialVersionUID = 1L;

    private Long id;
    private String name;
    private String email;
    private String password;
    private String cnpj;
    private String status;
    private ArrayList<String> image;
    private Collection<? extends GrantedAuthority> authorities;
    private Long userId;

    public UserSS() {
    }

    public UserSS(Long id, String name, String email, String password, ArrayList<String> image,
                  Collection<? extends GrantedAuthority> profiles, Long userId, String cnpj, String status) {
        super();
        this.id = id;
        this.name = name;
        this.email = email;
        this.password = password;
        this.image = image;
        this.authorities = profiles;
        this.userId = userId;
        this.cnpj = cnpj;
        this.status = status;
    }

    public Long getId() {
        return id;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return email;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    public Long getUserId() {
        return userId;
    }

    public String getName() {
        return name;
    }

    public String getCnpj() {
        return cnpj;
    }

    public String getStatus(){
        return status;
    }

    public ArrayList<String> getImage() {
        return image;
    }

    public boolean hasRole(UserRoleEnum profile) {
        return getAuthorities().contains(new SimpleGrantedAuthority(profile.getName()));
    }
}