package org.uaipet.api.config.security;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.CredentialsExpiredException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Component;
import org.uaipet.api.service.KmsService;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

@Component
@RequiredArgsConstructor
public class JWTUtil {

    @Value("${jwt.secret}")
    private String secret;

    @Value("${jwt.expiration}")
    private Long expiration;

    @Value("${aws.cloud.kms.key}")
    private String keyId;

    private final KmsService kmsService;

    public String generateToken(String username, Collection<? extends GrantedAuthority> authorities,
                                Long userId, String name, ArrayList<String> image, String cnpj, String status) {

        String decodedSecret = kmsService.decode(secret, keyId);
        return Jwts.builder()
                .claim("email", username)
                .claim("authorities", authorities)
                .claim("userId", userId)
                .claim("name", name)
                .claim("image", image)
                .claim("cnpj", cnpj)
                .setExpiration(new Date(System.currentTimeMillis() + expiration))
                .signWith(SignatureAlgorithm.HS512, decodedSecret.getBytes())
                .compact();
    }


    public boolean validToken(String token) {
        Claims claims = getClaims(token);
        if (claims != null) {
            String email = claims.get("email").toString();
            Date expirationDate = claims.getExpiration();
            Date now = new Date(System.currentTimeMillis());
            if (email != null && expirationDate != null && now.before(expirationDate)) {
                return true;
            }
        }
        return false;
    }

    public String getUsername(String token) {
        Claims claims = getClaims(token);
        if (claims != null) {
            return claims.get("email").toString();
        }
        return null;
    }

    private Claims getClaims(String token) throws ExpiredJwtException {
        try {
            String decodedSecret = kmsService.decode(secret, keyId);
            return Jwts.parser().setSigningKey(decodedSecret.getBytes()).parseClaimsJws(token).getBody();
        } catch (ExpiredJwtException ex){
            throw ex;
        } catch (Exception e) {
            return null;
        }
    }
}