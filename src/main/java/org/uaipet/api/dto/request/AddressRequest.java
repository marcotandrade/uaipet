package org.uaipet.api.dto.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import org.uaipet.api.data.enums.StateEnum;


@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class AddressRequest {

    private Long id;
    @NonNull
    private String  street;
    @NonNull
    private String city;
    @NonNull
    private Integer number;
    private String complement;
    private String district;
    private Integer zipCode;
    private StateEnum state;
    private Double latitude;
    private Double longitude;
    private String userEmail;

}
