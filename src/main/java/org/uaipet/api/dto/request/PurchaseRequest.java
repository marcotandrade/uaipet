package org.uaipet.api.dto.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NonNull;
import lombok.ToString;
import org.uaipet.api.data.enums.PaymentMethodEnum;
import org.uaipet.api.data.model.Company;
import org.uaipet.api.data.model.SearchIndex;
import org.uaipet.api.data.model.User;
import org.uaipet.api.dto.ProductQuantityDTO;

import java.io.Serializable;
import java.util.List;

@Data
@ToString
public class PurchaseRequest implements Serializable {

    @JsonProperty("productList")
    private List<ProductQuantityDTO> pList;
    private Long shipAddressId;
    @NonNull
    private String userEmail;
    private Company company;
    private String couponCode;
    private SearchIndex searchIndex;
    private User user;
    @NonNull
    private PaymentMethodEnum paymentMethod;
    private String change;

}
