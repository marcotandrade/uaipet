package org.uaipet.api.dto.request;

import lombok.Data;

@Data
public class KmsDecryptRequest {

    private String cypherText;

}
