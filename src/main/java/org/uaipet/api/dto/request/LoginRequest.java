package org.uaipet.api.dto.request;

import lombok.Data;
import lombok.ToString;

@ToString
@Data
public class LoginRequest {

    private String email;
    private String password;
}
