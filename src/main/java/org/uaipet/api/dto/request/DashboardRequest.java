package org.uaipet.api.dto.request;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class DashboardRequest {

    private String cnpj;
    private String month;

}
