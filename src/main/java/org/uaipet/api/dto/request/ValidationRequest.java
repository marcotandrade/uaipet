package org.uaipet.api.dto.request;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class ValidationRequest {

    private String code;
    private String email;

}
