package org.uaipet.api.dto.request;

import lombok.Builder;
import lombok.Data;
import lombok.NonNull;
import lombok.ToString;

@Data
@ToString
@Builder
public class MoveOrderStatusRequest {

    @NonNull
    private String userEmail;
    @NonNull
    private String cnpj;
    @NonNull
    private Long saleId;
    @NonNull
    private Boolean isCanceling;
    private String cancelingDescription;


}
