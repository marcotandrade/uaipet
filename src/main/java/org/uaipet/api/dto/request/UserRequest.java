package org.uaipet.api.dto.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.uaipet.api.data.enums.UserTypeEnum;


@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
public class UserRequest {

    private Long id;
    private String name;
    private String email;
    private String password;
    private String oldPassword;
    private UserTypeEnum type;
    private String validationCode;
    private String cpf;
    private String phone;
    private String expoToken;
    @JsonProperty
    private AddressRequest address;
    @JsonProperty
    private RoleRequest role;

}
