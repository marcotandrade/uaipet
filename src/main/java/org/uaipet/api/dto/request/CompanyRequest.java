package org.uaipet.api.dto.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;
import lombok.ToString;

@Data
@Builder
@ToString
public class CompanyRequest {

    private String cnpj;
    private String image;
    private String phone;
    private String name;
    private String socialContractName;
    @JsonProperty
    private AddressRequest address;
    @JsonProperty
    private UserRequest user;
}
