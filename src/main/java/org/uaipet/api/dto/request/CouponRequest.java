package org.uaipet.api.dto.request;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class CouponRequest {

    private String email;
    private String couponValue;
    private String rule;
    private String description;
    private String code;
    private String type;
    private Boolean allUsers;
    private Integer validityDays;
    private Integer discountPercentage;
    private String maxAmountAllowed;


}
