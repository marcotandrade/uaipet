package org.uaipet.api.dto.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import org.uaipet.api.data.model.BusinessHour;

import java.util.List;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class BusinessHoursRequest {
    private List<BusinessHour> businessHours;
}
