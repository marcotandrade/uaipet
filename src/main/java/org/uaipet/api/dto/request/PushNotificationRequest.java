package org.uaipet.api.dto.request;

import lombok.Builder;
import lombok.Data;
import lombok.ToString;

import java.util.List;

@Data
@Builder
@ToString
public class PushNotificationRequest {

    private List<String> expoToken;
    private String title;
    private String text;
    private Boolean allUsers;
    private Boolean allUsersRegistered;
    private String user;

}
