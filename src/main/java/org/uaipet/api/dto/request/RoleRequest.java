package org.uaipet.api.dto.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.uaipet.api.data.enums.UserRoleEnum;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class RoleRequest {

    private UserRoleEnum name;
}
