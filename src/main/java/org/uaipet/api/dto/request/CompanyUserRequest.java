package org.uaipet.api.dto.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.uaipet.api.data.enums.UserTypeEnum;

@Data
public class CompanyUserRequest {

    private Long id;
    private String name;
    private String email;
    private String password;
    private UserTypeEnum type;
    private String validationCode;
    private String cpf;
    private String phone;
    @JsonProperty
    private RoleRequest role;
    private String companyCnpj;
}
