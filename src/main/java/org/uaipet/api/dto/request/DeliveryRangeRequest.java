package org.uaipet.api.dto.request;

import lombok.Data;
import lombok.ToString;
import org.uaipet.api.data.model.DeliveryRange;

import java.util.List;

@ToString
@Data
public class DeliveryRangeRequest {
    private List<DeliveryRange> deliveryRangeList;
    private String minimumAmountOrder;
    private String amountFreeFreight;
}
