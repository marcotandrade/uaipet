package org.uaipet.api.dto.request;

import lombok.Data;
import lombok.NonNull;
import lombok.ToString;
import org.uaipet.api.data.enums.SaleStatusEnum;

import java.util.List;

@Data
@ToString
public class CompanyOrderRequest {

    @NonNull
    private String cnpj;
    @NonNull
    private int pageNumber;
    @NonNull
    private int pageSize;
    @NonNull
    private List<SaleStatusEnum> status;
    private String sortBy;
    private String direction;

}
