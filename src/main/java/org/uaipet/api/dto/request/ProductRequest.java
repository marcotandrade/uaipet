package org.uaipet.api.dto.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

@Data
@Builder
@ToString
public class ProductRequest {

    private Long id;

    private String name;

    private String description;

    private String price;

    private Integer stockCount;

    private String measurementValue;

    private String city;

    private String userEmail;

    private String sortBy;

    private int pageNumber;

    private int pageSize;

    private String direction;

    private String cnpj;

    private String images;

    private Boolean available;

    @JsonProperty
    private ProductCategoryGenericRequest productBrand;

    @JsonProperty
    private ProductCategoryGenericRequest productAnimalCategory;

    @JsonProperty
    private ProductCategoryGenericRequest productCategory;

    @JsonProperty
    private ProductCategoryGenericRequest productMeasurementCategory;

    @JsonProperty
    private ProductCategoryGenericRequest ageCategory;
}
