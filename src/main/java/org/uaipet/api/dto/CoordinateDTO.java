package org.uaipet.api.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Builder
@AllArgsConstructor
@Data
public class CoordinateDTO {

    private Double longitude;
    private Double latitude;
}
