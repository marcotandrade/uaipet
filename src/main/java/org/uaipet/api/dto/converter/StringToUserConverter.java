package org.uaipet.api.dto.converter;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.NonNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.uaipet.api.dto.request.UserRequest;
import org.uaipet.api.service.exception.ConversionException;

import java.io.IOException;

@Component
public class StringToUserConverter implements Converter<String, UserRequest> {

    private static final Logger log = LoggerFactory.getLogger(StringToUserConverter.class);

    @Autowired
    private ObjectMapper objectMapper;

    @Override
    public UserRequest convert(@NonNull String source)  {
        try {
            return objectMapper.readValue(source, UserRequest.class);
        } catch (IOException e) {
            log.error(e.getMessage());
            throw new ConversionException();
        }
    }
}
