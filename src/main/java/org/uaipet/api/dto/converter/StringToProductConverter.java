package org.uaipet.api.dto.converter;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.NonNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import org.uaipet.api.dto.request.ProductRequest;
import org.uaipet.api.service.exception.ConversionException;

import java.io.IOException;

@Component
public class StringToProductConverter implements Converter<String, ProductRequest> {

    private static final Logger log = LoggerFactory.getLogger(StringToProductConverter.class);

    @Autowired
    private ObjectMapper objectMapper;

    @Override
    public ProductRequest convert(@NonNull String source)  {
        try {
            return objectMapper.readValue(source, ProductRequest.class);
        } catch (IOException e) {
            log.error(e.getMessage());
            throw new ConversionException();
        }
    }
}
