package org.uaipet.api.dto;

import lombok.Builder;
import lombok.Data;
import org.uaipet.api.data.model.*;

@Builder
@Data
public class ProductDTO {

    private Long id;

    private String name;

    private String description;

    private String price;

    private Integer stockCount;

    private String measurementValue;

    private String image;

    private Boolean available;

    private ProductBrand productBrand;

    private ProductAnimalCategory productAnimalCategory;

    private ProductCategory productCategory;

    private ProductMeasurementCategory productMeasurementCategory;

    private ProductAgeCategory productAgeCategory;

    private CompanyDTO company;

}
