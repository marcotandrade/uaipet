package org.uaipet.api.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Authority {

    private String authority;
}
