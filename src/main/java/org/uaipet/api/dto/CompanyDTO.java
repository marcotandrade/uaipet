package org.uaipet.api.dto;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class CompanyDTO {

    private Long id;

    private String cnpj;

    private String freightValue;

    private String name;

    private boolean isOpen;

    private String  minimumOrderValue;

    private String  amountFreeFreight;

    private String companyLogo;
}
