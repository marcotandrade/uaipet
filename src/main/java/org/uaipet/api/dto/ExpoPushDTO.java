package org.uaipet.api.dto;

import lombok.Builder;
import lombok.Data;
import lombok.ToString;

import java.util.List;

@Data
@ToString
@Builder
public class ExpoPushDTO {

    private List<String> to;
    private String title;
    private String body;
    private String sound;
    private Integer badge;

}
