package org.uaipet.api.dto;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class OrderProductsDTO {

    private String productName;
    private String productQuantity;
    private String productValue;
    private String productAmount;
    private String productMeasurement;
    private String productImage;
}
