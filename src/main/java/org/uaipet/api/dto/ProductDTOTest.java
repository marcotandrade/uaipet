package org.uaipet.api.dto;

import lombok.Builder;
import lombok.Data;
import org.uaipet.api.data.model.ProductAgeCategory;
import org.uaipet.api.data.model.ProductAnimalCategory;
import org.uaipet.api.data.model.ProductBrand;
import org.uaipet.api.data.model.ProductCategory;
import org.uaipet.api.data.model.ProductMeasurementCategory;

@Data
public class ProductDTOTest {

    private Long id;

    private String name;

    private String description;

    private String price;

    private Integer stockCount;

    private String measurementValue;

    private String image;


}
