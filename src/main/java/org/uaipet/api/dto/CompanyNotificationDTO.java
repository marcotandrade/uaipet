package org.uaipet.api.dto;

import lombok.Builder;
import lombok.Data;

import java.util.Date;

@Data
@Builder
public class CompanyNotificationDTO {

    private Long id;
    private Long orderId;
    private String customerName;
    private Boolean viewed;
    private Date createdAt;

}
