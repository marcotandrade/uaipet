package org.uaipet.api.dto.response;

import lombok.Builder;
import lombok.Data;
import org.springframework.http.HttpHeaders;
import org.uaipet.api.data.model.Company;

@Data
@Builder
public class CompanyRegisterResponse {

    private Company company;
    private HttpHeaders headers;

}
