package org.uaipet.api.dto.response;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ProcessedResponse {

    private Boolean processed;
}
