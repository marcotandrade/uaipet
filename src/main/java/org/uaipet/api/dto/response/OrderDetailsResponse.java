package org.uaipet.api.dto.response;

import lombok.Builder;
import lombok.Data;
import org.uaipet.api.dto.OrderProductsDTO;

import java.util.List;

@Data
@Builder
public class OrderDetailsResponse {

    private Long saleId;
    private String orderStatus;
    private String totalAmount;
    private String freightAmount;
    private String customerName;
    private String street;
    private Integer number;
    private String district;
    private String complement;
    private String customerPhone;
    private String customerCPF;
    private List<OrderProductsDTO> productsFromOrder;
    private String paymentMethod;
    private String changeFor;
    private String couponAmount;
}
