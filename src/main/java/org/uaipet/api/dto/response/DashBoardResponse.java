package org.uaipet.api.dto.response;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class DashBoardResponse {

    private List<DashboardProductsProjectionResponse> products;
    private DashboardSalesProjectionResponse values;


}
