package org.uaipet.api.dto.response;

import lombok.Builder;
import lombok.Data;
import org.uaipet.api.dto.CompanyNotificationDTO;

import java.util.List;

@Data
@Builder
public class CompanyNotificationResponse {

    private Integer totalNotifications;
    private List<CompanyNotificationDTO> notifications;

}
