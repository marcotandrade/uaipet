package org.uaipet.api.dto.response;

import lombok.Builder;
import lombok.Data;
import org.uaipet.api.dto.OrderProductsDTO;

import java.util.List;

@Data
@Builder
public class UserOrdersResponse {

    private Long saleId;
    private String amount;
    private String street;
    private String paymentMethod;
    private String date;
    private CompanyResponse company;
    private String orderStatus;
    private List<OrderProductsDTO> productsFromOrder;


}
