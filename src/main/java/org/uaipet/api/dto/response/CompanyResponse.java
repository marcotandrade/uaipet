package org.uaipet.api.dto.response;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class CompanyResponse {

    private String name;
    private String phone;
    private String street;
    private String number;
    private String district;
}
