package org.uaipet.api.dto.response;

import lombok.Builder;
import lombok.Data;
import org.uaipet.api.dto.OrderProductsDTO;

import java.util.List;

@Data
@Builder
public class OrderResponse {

    private Long saleId;
    private String amount;
    private String totalAmount;
    private String couponAmount;
    private String date;
    private String storeName;
    private String orderStatus;
    private String customerName;
    private String street;
    private String number;
    private String district;
    private String customerPhone;
    private List<OrderProductsDTO> productsFromOrder;
    private String freightValue;
    private String distance;
    private String paymentMethod;
    private String changeFor;
}
