package org.uaipet.api.dto.response;


import lombok.Builder;
import lombok.Data;
import org.uaipet.api.data.repository.projection.InfluencerSalesProjection;

import java.math.BigDecimal;
import java.math.RoundingMode;

@Data
@Builder
public class InfluencerSalesResponse {

    private String customerName;
    private String profit;

    public static InfluencerSalesResponse toInfluencerSalesResponse(InfluencerSalesProjection req){
       return InfluencerSalesResponse.builder()
                .customerName(req.getCustomerName())
                .profit(new BigDecimal(req.getProfit()).setScale(2, RoundingMode.FLOOR).toString())
                .build();
    }

}
