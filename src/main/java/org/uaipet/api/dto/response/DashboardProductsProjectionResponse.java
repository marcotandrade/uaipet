package org.uaipet.api.dto.response;


public interface DashboardProductsProjectionResponse {

    Integer getQuantity();
    Integer setQuantity(Integer i);

    String getProduct();
    String setProduct(String s);

}
