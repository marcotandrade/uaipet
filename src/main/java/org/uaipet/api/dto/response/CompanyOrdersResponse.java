package org.uaipet.api.dto.response;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class CompanyOrdersResponse {

    private List<OrderResponse> orderResponse;
    private long totalPages;
    private long totalElements;
    private long number;
    private long size;
    private boolean last;
    private boolean first;
    private boolean empty;


}
