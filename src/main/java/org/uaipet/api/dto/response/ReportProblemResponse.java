package org.uaipet.api.dto.response;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class ReportProblemResponse {

    private String storeName;
    private String storePhone;
    private String street;
    private String number;
    private String district;

}
