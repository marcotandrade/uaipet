package org.uaipet.api.dto.response;


public interface DashboardSalesProjectionResponse {

    Integer getSalesQuantity();
    Integer setSalesQuantity(Integer i);

    String getSaleAmount();
    String setSaleAmount(String s);

    Integer getQttCustomerPurchased();
    Integer setQttCustomerPurchased(Integer s);

    Integer getQttCustomerAround();
    Integer setQttCustomerAround(Integer s);


}
