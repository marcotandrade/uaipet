package org.uaipet.api.dto.response;

import lombok.Builder;
import lombok.Data;
import org.springframework.http.HttpHeaders;
import org.uaipet.api.data.model.User;

@Data
@Builder
public class RegisterResponse {

    private User user;
    private HttpHeaders headers;

}
