package org.uaipet.api.dto;

import lombok.Builder;
import lombok.Data;

import java.util.Map;

@Builder
@Data
public class MailDTO {

    private String mailTo;
    private String mailFrom;
    private String name;
    private String subject;
    private Map<String, Object> props;
    private String templateName;

}