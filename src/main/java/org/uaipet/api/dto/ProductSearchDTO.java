package org.uaipet.api.dto;

import lombok.Builder;
import lombok.Data;

import java.util.List;


@Builder
@Data
public class ProductSearchDTO {

    private List<ProductDTO> content;
    private long totalPages;
    private long totalElements;
    private long number;
    private long size;
    private long numberOfElements;
    private boolean last;
    private boolean first;
    private boolean empty;
}
