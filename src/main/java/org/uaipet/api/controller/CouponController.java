package org.uaipet.api.controller;


import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.uaipet.api.data.model.Coupon;
import org.uaipet.api.dto.request.CouponRequest;
import org.uaipet.api.dto.response.CouponResponse;
import org.uaipet.api.service.CouponService;

@RequestMapping("/coupon")
@RequiredArgsConstructor
@Service
public class CouponController {

    private static final Logger log = LoggerFactory.getLogger(CouponController.class);

    private final CouponService couponService;

    @PostMapping
    public ResponseEntity<Coupon> saveCoupon(@RequestBody CouponRequest couponRequest) {
        log.info("save");
        return ResponseEntity.ok(couponService.createCoupon(couponRequest));
    }


    @GetMapping("/{code}")
    public ResponseEntity<CouponResponse> saveCoupon(@PathVariable String code) {
        log.info("save");
        return ResponseEntity.ok(couponService.couponIsValid(code));
    }


}
