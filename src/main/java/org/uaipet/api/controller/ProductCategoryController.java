package org.uaipet.api.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.uaipet.api.data.model.ProductCategory;
import org.uaipet.api.service.ProductCategoryService;

import javax.validation.Valid;
import java.util.List;


@RestController
@RequestMapping("/product/category")
public class ProductCategoryController {

    private final ProductCategoryService service;

    @Autowired
    public ProductCategoryController(ProductCategoryService service ){
        this.service = service;
    }

    @GetMapping
    public ResponseEntity<List<ProductCategory>> findAll( ) {
        return ResponseEntity.ok( this.service.findAll( ) );
    }

    @GetMapping( "/{id}" )
    public ResponseEntity<ProductCategory> findById( @PathVariable Long id ) {
        return ResponseEntity.ok( this.service.findOne( id ) );
    }

    @PostMapping
    public ResponseEntity<ProductCategory> save( @Valid @RequestBody ProductCategory product ) {

        ProductCategory clientSave = this.service.save( product );
        return ResponseEntity.status( HttpStatus.CREATED ).body( clientSave );
    }

    @PutMapping( "/{id}" )
    public ResponseEntity update(@PathVariable Long id, @Valid @RequestBody ProductCategory product ) {

        this.service.update( id, product );
        return ResponseEntity.ok().build();
    }

}
