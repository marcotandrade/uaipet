package org.uaipet.api.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.uaipet.api.data.model.ProductAnimalCategory;
import org.uaipet.api.service.ProductAnimalCategoryService;

import javax.validation.Valid;
import java.util.List;


@RestController
@RequestMapping( "/product/animal" )
public class ProductAnimalCategoryController {

    private final ProductAnimalCategoryService service;

    @Autowired
    public ProductAnimalCategoryController(ProductAnimalCategoryService service ){
        this.service = service;
    }

    @GetMapping
    public ResponseEntity<List<ProductAnimalCategory>> findAll( ) {
        return ResponseEntity.ok( this.service.findAll( ) );
    }

    @GetMapping( "/{id}" )
    public ResponseEntity<ProductAnimalCategory> findById( @PathVariable Long id ) {
        return ResponseEntity.ok( this.service.findOne( id ) );
    }

    @PostMapping
    public ResponseEntity<ProductAnimalCategory> save( @Valid @RequestBody ProductAnimalCategory product ) {

        ProductAnimalCategory clientSave = this.service.save( product );
        return ResponseEntity.status( HttpStatus.CREATED ).body( clientSave );
    }

    @PutMapping( "/{id}" )
    public ResponseEntity update(@PathVariable Long id, @Valid @RequestBody ProductAnimalCategory product ) {

        this.service.update( id, product );
        return ResponseEntity.ok().build();
    }

}
