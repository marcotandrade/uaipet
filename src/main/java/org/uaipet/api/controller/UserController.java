package org.uaipet.api.controller;

import lombok.AllArgsConstructor;
import lombok.NonNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.uaipet.api.data.model.User;
import org.uaipet.api.dto.request.AddressRequest;
import org.uaipet.api.dto.request.UserRequest;
import org.uaipet.api.dto.request.ValidationRequest;
import org.uaipet.api.dto.response.ProcessedResponse;
import org.uaipet.api.dto.response.RegisterResponse;
import org.uaipet.api.dto.response.UserOrdersResponse;
import org.uaipet.api.service.UserService;

import java.io.IOException;
import java.util.List;

@RequestMapping("/user")
@AllArgsConstructor
@Service
public class UserController {

    private static final Logger log = LoggerFactory.getLogger(UserController.class);

    private final UserService userService;

    @PostMapping
    public ResponseEntity<Object> save(@RequestParam("user") UserRequest userRequest,
                                       @RequestParam("image") List<MultipartFile> image) {
        log.info(String.format("save - %s ", userRequest.toString()));
        RegisterResponse response = userService.saveUser(userRequest, image);
        response.getUser().setValidationCode(null);
        return ResponseEntity.ok().headers(response.getHeaders()).body(response.getUser());
    }

    @PostMapping("/register/incomplete")
    public ResponseEntity<Object> registerIncompleteUser(@RequestBody UserRequest userRequest) {
        log.info(String.format("registerIncompleteUser - %s ", userRequest.toString()));
        RegisterResponse response = userService.saveUserIncomplete(userRequest);
        return ResponseEntity.ok().body(response.getUser());
    }

    @PostMapping("/update")
    public ResponseEntity<User> updateUserInfo(@RequestParam(value = "user", required = false) @NonNull UserRequest userRequest,
                                               @RequestParam(value = "image", required = false) List<MultipartFile> image) throws IOException {
        log.info(String.format("updateUserInfo - %s ", userRequest.toString()));
        return ResponseEntity.ok(userService.updateUser(userRequest, image));
    }

    @PostMapping("/validate")
    public ResponseEntity<ProcessedResponse> validateEmail(@RequestBody ValidationRequest validationRequest) {
        log.info(String.format("validateEmail - %s", validationRequest.toString()));
        return ResponseEntity.ok(ProcessedResponse.builder().processed(userService.validateUserEmail(validationRequest)).build());
    }

    @PostMapping("/address")
    public ResponseEntity<Object> saveOrUpdateAddress(@RequestBody @NonNull AddressRequest addressRequest) {
        log.info(String.format("saveOrUpdateAddress - %s", addressRequest.toString()));
        return ResponseEntity.ok(userService.saveOrUpdateAddress(addressRequest));
    }

    @PostMapping("/find")
    public ResponseEntity<User> findUser(@RequestBody @NonNull UserRequest userRequest) {
        log.info(String.format("findUserFromEmail - %s", userRequest));
        return ResponseEntity.ok(userService.findUser(userRequest));
    }

    @DeleteMapping("/address/{userEmail}/{addressId}")
    public ResponseEntity<Object> deleteAddress(@PathVariable @NonNull Long addressId,
                                                @PathVariable @NonNull String userEmail) {
        log.info(String.format("deleteAddress - %s %s", userEmail, addressId.toString()));
        return ResponseEntity.ok(userService.deleteAddress(addressId, userEmail));
    }


    @GetMapping("/orders/{email}")
    public ResponseEntity<List<UserOrdersResponse>> getOrders(@PathVariable @NonNull String email) {
        log.info(String.format("getOrders - %s", email));
        return ResponseEntity.ok(userService.findUserOrders(email));
    }

    @GetMapping("/report-problem")
    public ResponseEntity<Object> reportProblem(@RequestParam("orderId") String orderId) {
        log.info(String.format("reportProblem - %s", orderId));
        return ResponseEntity.ok(userService.reportProblem(orderId));
    }

    @GetMapping("/influencer/{cpf}")
    public ResponseEntity<Object> getInfluencerSales(@PathVariable(name = "cpf") @NonNull String cpf) {
        log.info(String.format("getInfluencerSales - %s", cpf));
        return ResponseEntity.ok(userService.getSalesInfoInfluencerCupom(cpf));
    }
}


