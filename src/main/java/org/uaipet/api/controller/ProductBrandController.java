package org.uaipet.api.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.uaipet.api.data.model.ProductBrand;
import org.uaipet.api.service.ProductBrandService;

import javax.validation.Valid;
import java.util.List;


@RestController
@RequestMapping( "/product/brand" )
public class ProductBrandController {

    private final ProductBrandService service;

    @Autowired
    public ProductBrandController(ProductBrandService service ){
        this.service = service;
    }

    @GetMapping
    public ResponseEntity<List<ProductBrand>> findAll( ) {
        return ResponseEntity.ok( this.service.findAll( ) );
    }

    @GetMapping( "/{id}" )
    public ResponseEntity<ProductBrand> findById( @PathVariable Long id ) {
        return ResponseEntity.ok( this.service.findOne( id ) );
    }

    @PostMapping
    public ResponseEntity<ProductBrand> save( @Valid @RequestBody ProductBrand product ) {

        ProductBrand clientSave = this.service.save( product );
        return ResponseEntity.status( HttpStatus.CREATED ).body( clientSave );
    }

    @PutMapping( "/{id}" )
    public ResponseEntity update(@PathVariable Long id, @Valid @RequestBody ProductBrand product ) {

        this.service.update( id, product );
        return ResponseEntity.ok().build();
    }

}
