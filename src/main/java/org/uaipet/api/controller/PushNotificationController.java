package org.uaipet.api.controller;

import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.uaipet.api.dto.request.PushNotificationRequest;
import org.uaipet.api.service.PushNotificationService;


@RestController
@RequiredArgsConstructor
@RequestMapping( "/push-notification" )
public class PushNotificationController {

    private static final Logger log = LoggerFactory.getLogger(PushNotificationController.class);

    private final PushNotificationService pushNotificationService;

    @PostMapping
    public ResponseEntity<Object> pushNotification(@RequestBody PushNotificationRequest pushNotificationRequest) {
        log.info(String.format("pushNotification - %s ",pushNotificationRequest.toString()));
        pushNotificationService.sendNotification(pushNotificationRequest, true);
        return ResponseEntity.ok().build();
    }

}
