package org.uaipet.api.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.uaipet.api.data.model.ProductMeasurementCategory;
import org.uaipet.api.service.ProductMeasurementCategoryService;

import javax.validation.Valid;
import java.util.List;


@RestController
@RequestMapping( "/product/measurement" )
public class ProductMeasurementCategoryController {

    private final ProductMeasurementCategoryService service;

    @Autowired
    public ProductMeasurementCategoryController(ProductMeasurementCategoryService service ){
        this.service = service;
    }

    @GetMapping
    public ResponseEntity<List<ProductMeasurementCategory>> findAll( ) {
        return ResponseEntity.ok( this.service.findAll( ) );
    }

    @GetMapping( "/{id}" )
    public ResponseEntity<ProductMeasurementCategory> findById( @PathVariable Long id ) {
        return ResponseEntity.ok( this.service.findOne( id ) );
    }

    @PostMapping
    public ResponseEntity<ProductMeasurementCategory> save( @Valid @RequestBody ProductMeasurementCategory product ) {

        ProductMeasurementCategory clientSave = this.service.save( product );
        return ResponseEntity.status( HttpStatus.CREATED ).body( clientSave );
    }

    @PutMapping( "/{id}" )
    public ResponseEntity update(@PathVariable Long id, @Valid @RequestBody ProductMeasurementCategory product ) {

        this.service.update( id, product );
        return ResponseEntity.ok().build();
    }

}
