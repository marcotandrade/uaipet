package org.uaipet.api.controller;

import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.uaipet.api.config.security.JWTUtil;
import org.uaipet.api.data.model.User;
import org.uaipet.api.dto.Authority;
import org.uaipet.api.dto.EmailDTO;
import org.uaipet.api.dto.request.UserRequest;
import org.uaipet.api.service.AuthService;
import org.uaipet.api.service.UserService;
import org.uaipet.api.service.exception.UserNotFoundException;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/auth")
public class AuthController {

    private static final Logger log = LoggerFactory.getLogger(AuthController.class);

    private final JWTUtil jwtUtil;
    private final AuthService service;
    private final UserService userService;


    @PostMapping("/refresh_token")
    public ResponseEntity<Void> refreshToken(@RequestBody UserRequest userRequest, HttpServletResponse response) {
        User user = userService.findUserFromEmailAndId(userRequest);
        if (null != user) {
            ArrayList<String> strImg = new ArrayList<>();
            if (user != null && user.getImage() != null) {
                for (String s : user.getImage().split(";")) {
                    strImg.add(s);
                }
            }
            List<Authority> roleList = new ArrayList<>();
            roleList.add(Authority.builder().authority(user.getUserRoleList().get(0).getRole().getName()).build());

            Collection<? extends GrantedAuthority> authorities = (Collection<GrantedAuthority>) (Collection<?>) roleList;
            String cnpj = user.getCompany() != null ? user.getCompany().getCnpj() : null;

            String token = jwtUtil.generateToken(user.getEmail(), authorities, user.getId(),
                    user.getName(), strImg, cnpj, user.getStatus());
            response.addHeader("Authorization", "Bearer " + token);
            response.addHeader("access-control-expose-headers", "Authorization");
            return ResponseEntity.noContent().build();
        } else {
            throw new UserNotFoundException();
        }

    }

    @PostMapping("/forgot")
    public ResponseEntity<Void> forgot(@Valid @RequestBody EmailDTO dto) {
        long startTime = System.currentTimeMillis();
        service.sendNewPassword(dto.getEmail());
        log.info(String.format("passwordForgotten Finalizado em %s ms!", System.currentTimeMillis() - startTime));
        return ResponseEntity.noContent().build();
    }
}
