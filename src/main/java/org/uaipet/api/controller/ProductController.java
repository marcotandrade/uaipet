package org.uaipet.api.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.uaipet.api.data.model.Product;
import org.uaipet.api.dto.ProductDTO;
import org.uaipet.api.dto.ProductSearchDTO;
import org.uaipet.api.dto.request.ProductRequest;
import org.uaipet.api.dto.request.PurchaseRequest;
import org.uaipet.api.service.ProductService;

import java.util.List;


@RestController
@RequestMapping( "/product" )
public class   ProductController {

    private final ProductService service;

    @Autowired
    public ProductController( ProductService service ){
        this.service = service;
    }

    @PostMapping
    public ResponseEntity<ProductSearchDTO> search(@RequestBody ProductRequest request) {
        return ResponseEntity.ok( this.service.search(request));
    }

    @GetMapping( "/{id}" )
    public ResponseEntity<Product> findOneProduct(@PathVariable Long id ) {
        return ResponseEntity.ok( this.service.findOne(id));
    }

    @GetMapping( "/detail/{id}" )
    public ResponseEntity<ProductDTO> findProductById(@PathVariable Long id ) {
        return ResponseEntity.ok( this.service.findOneById(id) );
    }

    @PostMapping("/company/{cnpj}")
    public ResponseEntity<Product> save(@PathVariable("cnpj") String cnpj,
                                @RequestParam("product") ProductRequest product,
                                @RequestParam("image") List<MultipartFile> images) {

        Product clientSave = this.service.save( cnpj, product, images );
        return ResponseEntity.status( HttpStatus.CREATED ).body( clientSave );
    }

    @PutMapping("{id}/company/{cnpj}")
    public ResponseEntity update( @PathVariable("id") Long id,
                                  @PathVariable("cnpj") String cnpj,
                                  @RequestParam("product") ProductRequest product,
                                  @RequestParam("image") List<MultipartFile> images) {

        this.service.update( id, cnpj, product, images );
        return ResponseEntity.accepted().build();
    }

    @PostMapping("/purchase")
    public ResponseEntity<Boolean> purchase(@RequestBody PurchaseRequest purchaseRequest){
        return ResponseEntity.ok(service.purchase(purchaseRequest));
    }

    @GetMapping("/spotlight")
    public ResponseEntity<ProductSearchDTO> spotlight(){
        return ResponseEntity.ok(service.productsSpotlight());
    }


    @DeleteMapping("/{id}")
    public ResponseEntity<Boolean> deleteProduct(@PathVariable("id") Long id){
        return ResponseEntity.ok(service.deleteProduct(id));
    }

}
