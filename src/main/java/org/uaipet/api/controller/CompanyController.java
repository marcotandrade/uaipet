package org.uaipet.api.controller;

import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.uaipet.api.data.model.BusinessHour;
import org.uaipet.api.data.model.Company;
import org.uaipet.api.data.model.DeliveryRange;
import org.uaipet.api.data.model.Product;
import org.uaipet.api.data.model.User;
import org.uaipet.api.dto.request.CompanyOrderRequest;
import org.uaipet.api.dto.request.CompanyRequest;
import org.uaipet.api.dto.request.CompanyUserRequest;
import org.uaipet.api.dto.request.DashboardRequest;
import org.uaipet.api.dto.request.DeliveryRangeRequest;
import org.uaipet.api.dto.request.MoveOrderStatusRequest;
import org.uaipet.api.dto.request.ProductRequest;
import org.uaipet.api.dto.response.CompanyNotificationResponse;
import org.uaipet.api.dto.response.CompanyOrdersResponse;
import org.uaipet.api.dto.response.CompanyRegisterResponse;
import org.uaipet.api.dto.response.DashBoardResponse;
import org.uaipet.api.dto.response.DashboardSalesProjectionResponse;
import org.uaipet.api.dto.response.OrderDetailsResponse;
import org.uaipet.api.dto.response.ProcessedResponse;
import org.uaipet.api.service.CompanyNotificationService;
import org.uaipet.api.service.CompanyService;

import java.io.IOException;
import java.util.List;

@RequestMapping("/company")
@RequiredArgsConstructor
@Service
public class CompanyController {

    private static final Logger log = LoggerFactory.getLogger(CompanyController.class);

    private final CompanyService companyService;
    private final CompanyNotificationService companyNotificationService;

    @PostMapping
    public ResponseEntity<Company> save(@RequestParam("company") CompanyRequest companyRequest,
                                                        @RequestParam("image") List<MultipartFile> image) throws IOException {
        log.info("save");
        CompanyRegisterResponse companyInfo = companyService.saveCompany(companyRequest, image);
        return ResponseEntity.ok().headers(companyInfo.getHeaders()).body(companyInfo.getCompany());

    }

    @PostMapping("/update")
    public ResponseEntity<Boolean> updateInfo(@RequestParam("company") CompanyRequest companyRequest,
                                        @RequestParam("image") List<MultipartFile> image) throws IOException {
        log.info("updateInfo");
        Boolean companyUpdated = companyService.updateCompanyInfo(companyRequest, image);
        return ResponseEntity.ok().body(companyUpdated);

    }

    @PostMapping("/user")
    public ResponseEntity<User> addUser(@RequestBody CompanyUserRequest userRequest) {
        log.info("addUser");
        return ResponseEntity.ok(companyService.addUser(userRequest));
    }

    @DeleteMapping("/user/{id}")
    public ResponseEntity<Boolean> deleteUser(@PathVariable Long id) {
        log.info("deleteUser");
        return ResponseEntity.ok(companyService.deleteUser(id));
    }

    @PostMapping("/delivery-range/{cnpj}")
    public ResponseEntity<Boolean> saveDeliveryRange(@PathVariable String cnpj, @RequestBody DeliveryRangeRequest deliveryRangeRequest) {
        log.info("saveDeliveryRange");
        return ResponseEntity.ok(companyService.addDeliveryRange(cnpj, deliveryRangeRequest));
    }

    @DeleteMapping("/delivery-range/{cnpj}")
    public ResponseEntity<Boolean> deleteAllDeliveryRanges(@PathVariable String cnpj) {
        log.info("deleteAllDeliveryRanges");
        return ResponseEntity.ok(companyService.deleteAllDeliveryRanges(cnpj));
    }

    @GetMapping("/{cnpj}")
    public ResponseEntity<Company> getCompanyInfo(@PathVariable String cnpj) {
        log.info("getCompanyInfo");
        return ResponseEntity.ok(companyService.findCompanyInfo(cnpj));
    }

    @PostMapping("/close")
    public ResponseEntity<Boolean> closedForBusiness(@RequestBody CompanyRequest companyRequest) {
        log.info("closedForBusiness");
        return ResponseEntity.ok(companyService.closeForBusiness(companyRequest));
    }

    @PostMapping("/open")
    public ResponseEntity<Boolean> openForBusiness(@RequestBody CompanyRequest companyRequest) {
        log.info("openForBusiness");
        return ResponseEntity.ok(companyService.openForBusiness(companyRequest));
    }

    @PostMapping("/business-hours/{cnpj}")
    public ResponseEntity<String> updateBusinessHours(@PathVariable String cnpj, @RequestBody List<BusinessHour> businessHours) {
        log.info("updateBusinessHours");
        return ResponseEntity.ok(companyService.updateBusinessHour(cnpj, businessHours));
    }

    @DeleteMapping("/business-hours/{cnpj}")
    public ResponseEntity<Boolean> deleteBusinessHours(@PathVariable String cnpj) {
        log.info("deleteBusinessHours");
        return ResponseEntity.ok(companyService.deleteAllBusinessHours(cnpj));
    }

    @PostMapping("/find/orders")
    public ResponseEntity<CompanyOrdersResponse> getOrders(@RequestBody CompanyOrderRequest companyOrderRequest) {
        log.info("getOrders");
        return ResponseEntity.ok(companyService.findOrders(companyOrderRequest));
    }

    @GetMapping("/find/order/details/{id}")
    public ResponseEntity<OrderDetailsResponse> getOrderDetails(@PathVariable("id") Long orderId) {
        log.info("getOrderDetails");
        return ResponseEntity.ok(companyService.findOrderDetails(orderId));
    }

    @PostMapping("/move/order")
    public ResponseEntity<Boolean> moveOrder(@RequestBody MoveOrderStatusRequest moveOrderStatusRequest) {
        log.info("moveOrderStatusRequest");
        return ResponseEntity.ok(companyService.moveOrderToNextStatus(moveOrderStatusRequest));
    }

    @PostMapping("/search/product")
    public ResponseEntity<Page<Product>> searchProduct(@RequestBody ProductRequest request) {
        log.info("searchProduct");
        return ResponseEntity.ok(companyService.productSearch(request));
    }

    @GetMapping("/users/{cnpj}")
    public ResponseEntity<List<User>> findUsers(@PathVariable String cnpj) {
        log.info("searchProduct");
        return ResponseEntity.ok(companyService.findUsers(cnpj));
    }

    @PostMapping("/dashboard")
    public ResponseEntity<DashBoardResponse> getDashboardInfo(@RequestBody DashboardRequest request) {
        log.info("getDashboardInfo");
        return ResponseEntity.ok(companyService.dashboard(request));
    }

    @GetMapping("/notifications/{cnpj}")
    public ResponseEntity<CompanyNotificationResponse> getCompanyNotifications(@PathVariable String cnpj) {
        log.info("getCompanyNotifications");
        return ResponseEntity.ok(companyNotificationService.getNotifications(cnpj));
    }

    @PutMapping("/notifications/{orderId}")
    public ResponseEntity<ProcessedResponse> markSeenNotifications(@PathVariable Long orderId) {
        log.info("putStatusNotifications");
        return ResponseEntity.ok(companyNotificationService.setNotificationAsSeen(orderId));
    }


}
