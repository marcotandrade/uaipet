package org.uaipet.api.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.uaipet.api.data.model.ProductAgeCategory;
import org.uaipet.api.service.ProductAgeCategoryService;

import javax.validation.Valid;
import java.util.List;


@RestController
@RequestMapping( "/product/age-category" )
public class ProductAgeCategoryController {

    private final ProductAgeCategoryService service;

    @Autowired
    public ProductAgeCategoryController(ProductAgeCategoryService service ){
        this.service = service;
    }

    @GetMapping
    public ResponseEntity<List<ProductAgeCategory>> findAll( ) {
        return ResponseEntity.ok( this.service.findAll( ) );
    }

    @GetMapping( "/{id}" )
    public ResponseEntity<ProductAgeCategory> findById( @PathVariable Long id ) {
        return ResponseEntity.ok( this.service.findOne( id ) );
    }

    @PostMapping
    public ResponseEntity<ProductAgeCategory> save( @Valid @RequestBody ProductAgeCategory ageCategory ) {

        ProductAgeCategory clientSave = this.service.save( ageCategory );
        return ResponseEntity.status( HttpStatus.CREATED ).body( clientSave );
    }

    @PutMapping( "/{id}" )
    public ResponseEntity update(@PathVariable Long id, @Valid @RequestBody ProductAgeCategory ageCategory ) {

        this.service.update( id, ageCategory );
        return ResponseEntity.ok().build();
    }

}
