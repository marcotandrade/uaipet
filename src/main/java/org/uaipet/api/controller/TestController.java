package org.uaipet.api.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.uaipet.api.dto.request.KmsDecryptRequest;
import org.uaipet.api.service.KmsService;

import java.nio.ByteBuffer;

@RestController
@RequiredArgsConstructor
@RequestMapping("/test")
public class TestController {

    private final KmsService kmsService;

    @Value("${aws.cloud.kms.key}")
    private String keyId;

    @GetMapping("/kms/encrypt/{value}")
    public ResponseEntity encodeTeste(@PathVariable("value") String value) {
        return ResponseEntity.ok(kmsService.encode(value, keyId));
    }

    @PostMapping("/kms/decrypt")
    public ResponseEntity<String> decodeTeste(@RequestBody KmsDecryptRequest kmsDecryptRequest) {
        return ResponseEntity.ok(kmsService.decode(kmsDecryptRequest.getCypherText(), keyId));
    }

}
