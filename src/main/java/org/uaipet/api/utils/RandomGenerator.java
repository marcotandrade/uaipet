package org.uaipet.api.utils;

import java.util.Random;

public class RandomGenerator {

    public static String generateRandomIntegerCode(){
        Random rnd = new Random();
        int number = rnd.nextInt(999999);
        // this will convert any number sequence into 6 character.
        return String.format("%06d", number);
    }
}
