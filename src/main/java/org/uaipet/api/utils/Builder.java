package org.uaipet.api.utils;

import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;
import org.uaipet.api.data.enums.CityEnum;
import org.uaipet.api.data.enums.CouponTypeEnum;
import org.uaipet.api.data.enums.UserStatusEnum;
import org.uaipet.api.data.enums.UserTypeEnum;
import org.uaipet.api.data.model.*;
import org.uaipet.api.data.repository.SearchIndexRepository;
import org.uaipet.api.data.repository.projection.ProductProjection;
import org.uaipet.api.dto.CompanyDTO;
import org.uaipet.api.dto.ProductDTO;
import org.uaipet.api.dto.ProductSearchDTO;
import org.uaipet.api.dto.request.*;
import org.uaipet.api.service.CityService;
import org.uaipet.api.service.exception.AddressNotFoundForUserException;
import org.uaipet.api.service.exception.ProductException;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static org.uaipet.api.utils.CheckDate.checkIfTheCompanyIsOpen;

@RequiredArgsConstructor
@Component
public class Builder {

    private static final Logger log = LoggerFactory.getLogger(Builder.class);

    @Value("${company.default.fee}")
    private Integer fee;

    @Value("${company.default.monthlyFee}")
    private Integer monthlyFee;

    private final BCryptPasswordEncoder encoder;
    private final CityService cityService;
    private final SearchIndexRepository searchIndexRepository;

    public User buildUser(UserRequest userRequest){
        log.info(String.format("buildUser - %s",userRequest.toString()));
        User user = User.builder()
                .name(userRequest.getName())
                .email(userRequest.getEmail())
                .cpf(userRequest.getCpf())
                .password(userRequest.getPassword())
                .type(userRequest.getType())
                .validationCode(userRequest.getValidationCode())
                .phone(userRequest.getPhone())
                .status(UserStatusEnum.NOT_VALID.getStatus())
                .expoToken(userRequest.getExpoToken())
                .build();

        List<Address> addressList = new ArrayList<>();
        if(userRequest.getAddress() != null) {
            addressList.add(buildAddress(userRequest.getAddress(), user, false));
        }

        List<UserRole> userRoleList = new ArrayList<>();
        if(userRequest.getRole() != null) {
            userRoleList.add(buildRole(userRequest.getRole(), user));
        }

        List<Coupon> userCouponList = new ArrayList<>();
        userCouponList.add(Coupon.builder()
                .active(true)
                .code(RandomGenerator.generateRandomIntegerCode())
                .expirationDate(LocalDateTime.now().plusDays(7))
                .value("10.00")
                .rule("Aproveite seu cupom válido por 7 dias para comprar rações, remédios, brinquedos, temos tudo que seu melhor amigo precisa")
                .description("R$10,00 de cupom no seu primeiro pedido!")
                .type(CouponTypeEnum.SINGLE_USE)
                .user(user)
                .build());

        user.setCouponList(userCouponList);
        user.setAddressList(addressList);
        user.setUserRoleList(userRoleList);

       return user;
    }

    public User buildCompanyUser(CompanyUserRequest userRequest){
        log.info(String.format("buildCompanyUser - %s",userRequest.toString()));
        User user = User.builder()
                .name(userRequest.getName())
                .email(userRequest.getEmail())
                .cpf(userRequest.getCpf())
                .password(userRequest.getPassword())
                .type(UserTypeEnum.COMPANY)
                .validationCode(null)
                .phone(userRequest.getPhone())
                .status(UserStatusEnum.VALID.getStatus())
                .build();

        List<UserRole> userRoleList = new ArrayList<>();
        userRoleList.add(buildRole(userRequest.getRole(), user));

        user.setUserRoleList(userRoleList);
        return user;
    }

    public Address buildAddress(AddressRequest addressRequest, User user, Boolean blockRegister) {
        return Address.builder()
                .city(cityService.findCity(addressRequest, blockRegister).getCity())
                .state(addressRequest.getState())
                .street(addressRequest.getStreet())
                .complement(addressRequest.getComplement())
                .number(addressRequest.getNumber())
                .district(addressRequest.getDistrict())
                .zipCode(addressRequest.getZipCode())
                .latitude(addressRequest.getLatitude())
                .longitude(addressRequest.getLongitude())
                .defaultAddress(true)
                .deleted(false)
                .user(user)
                .build();
    }


    public Address buildAddress(AddressRequest addressRequest, Boolean blockRegister) {
        return Address.builder()
                .city(cityService.findCity(addressRequest, blockRegister).getCity())
                .state(addressRequest.getState())
                .street(addressRequest.getStreet())
                .complement(addressRequest.getComplement())
                .number(addressRequest.getNumber())
                .district(addressRequest.getDistrict())
                .zipCode(addressRequest.getZipCode())
                .latitude(addressRequest.getLatitude())
                .longitude(addressRequest.getLongitude())
                .defaultAddress(true)
                .id(addressRequest.getId())
                .build();
    }

    public UserRole buildRole(RoleRequest roleRequest, User user){
      return UserRole.builder()
                .role(roleRequest.getName())
                .user(user)
                .build();
    }

    public Company buildCompany(CompanyRequest companyRequest){
        List<User> userList = new ArrayList<>();
        userList.add(buildUser(companyRequest.getUser()));

        Company company = Company.builder()
                .name(companyRequest.getName())
                .socialContractName(companyRequest.getSocialContractName())
                .cnpj(companyRequest.getCnpj())
                .phone(companyRequest.getPhone())
                .fee(fee)
                .monthlyFee(monthlyFee)
                .userList(userList)
                .address(buildAddress(companyRequest.getAddress(), true))
                .build();

        userList.forEach(user -> {
            user.setCompany(company);
            user.setStatus(UserStatusEnum.NOT_VALID.getStatus());
        });

        return company;
    }

    public Product buildProduct(ProductRequest request) {
        String stringPattern = "\\d+\\.{1}\\d{2}$";
        Pattern pattern = Pattern.compile(stringPattern);
        Matcher matcher = pattern.matcher(request.getPrice());
        boolean matches = matcher.matches();

        if (!matches) {
            throw new ProductException("Preço do produto foi informado no formato incorreto");
        }

        return Product.builder()
                .id(request.getId())
                .name(request.getName())
                .price(request.getPrice())
                .description(request.getDescription())
                .stockCount(request.getStockCount())
                .measurementValue(request.getMeasurementValue())
                .productCategory(buildProductCategory(request.getProductCategory()))
                .productBrand(buildProductBrand(request.getProductBrand()))
                .productAnimalCategory(buildProductAnimalCategory(request.getProductAnimalCategory()))
                .productMeasurementCategory(buildProductMeasurementCategory(request.getProductMeasurementCategory()))
                .productAgeCategory(buildProductAgeCategory(request.getAgeCategory()))
                .deleted(false)
                .available(request.getAvailable() == null ? true : request.getAvailable())
                .build();
    }

    public ProductCategory buildProductCategory(ProductCategoryGenericRequest request) {
        if(Objects.isNull(request)) return null;
        return ProductCategory.builder()
                .id(request.getId())
                .name(request.getName())
                .description(request.getDescription())
                .build();
    }

    public ProductBrand buildProductBrand(ProductCategoryGenericRequest request) {
        if(Objects.isNull(request)) return null;
        return ProductBrand.builder()
                .id(request.getId())
                .name(request.getName())
                .description(request.getDescription())
                .build();
    }

    public ProductMeasurementCategory buildProductMeasurementCategory(ProductCategoryGenericRequest request) {
        if(Objects.isNull(request)) return null;
        return ProductMeasurementCategory.builder()
                .id(request.getId())
                .name(request.getName())
                .description(request.getDescription())
                .build();
    }

    public ProductAnimalCategory buildProductAnimalCategory(ProductCategoryGenericRequest request) {
        if(Objects.isNull(request)) return null;
        return ProductAnimalCategory.builder()
                .id(request.getId())
                .name(request.getName())
                .description(request.getDescription())
                .build();
    }

    public ProductAgeCategory buildProductAgeCategory(ProductCategoryGenericRequest request) {
        if(Objects.isNull(request)) return null;
        return ProductAgeCategory.builder()
                .id(request.getId())
                .name(request.getName())
                .description(request.getDescription())
                .build();
    }

    public ProductSearchDTO buildProductSearchDTO(Page<Product> page, User user){

        ProductSearchDTO productSearchDTO = ProductSearchDTO.builder()
                .content(buildProductDTO(page.getContent(), user))
                .totalPages(page.getTotalPages())
                .totalElements(page.getTotalElements())
                .number(page.getNumber())
                .size(page.getSize())
                .numberOfElements(page.getNumberOfElements())
                .last(page.isLast())
                .first(page.isFirst())
                .empty(page.isEmpty())
                .build();
        return productSearchDTO;
    }

    public List<ProductDTO> buildProductDTO(List<Product> list,  User user) {
        List<ProductDTO> responseList = new ArrayList<>();
        for(Product p : list){
            responseList.add(buildProductDTO(p,user));
        }
        return responseList;
    }

    public ProductDTO buildProductDTO(Product product, User user) {
        log.info("buildProductDTO");
        return ProductDTO.builder()
                .id(product.getId())
                .name(product.getName())
                .description(product.getDescription())
                .price(product.getPrice())
                .stockCount(product.getStockCount())
                .measurementValue(product.getMeasurementValue())
                .image(product.getImage())
                .available(product.getAvailable())
                .productBrand(product.getProductBrand())
                .productAnimalCategory(product.getProductAnimalCategory())
                .productCategory(product.getProductCategory())
                .productMeasurementCategory(buildMeasurementCategory(product.getProductMeasurementCategory()))
                .productAgeCategory(product.getProductAgeCategory())
                .company(buildCompanyDTO(product.getCompany(), user, product.getPrice()))
                .build();
    }

    public CompanyDTO buildCompanyDTO(Company company, User user, String productPrice) {
        return CompanyDTO.builder()
                .id(company.getId())
                .cnpj(company.getCnpj())
                .name(company.getName())
                .freightValue(getFreightPrice(company, user, productPrice))
                .isOpen(checkIfTheCompanyIsOpen(company.getBusinessHours()))
                .minimumOrderValue(company.getMinimumOrderAmount())
                .amountFreeFreight(company.getAmountFreeFreight())
                .companyLogo(company.getImage())
                .build();
    }

    private ProductMeasurementCategory buildMeasurementCategory(ProductMeasurementCategory category){
       return ProductMeasurementCategory.builder().name(category.getName()).build();
    }


    private String getFreightPrice(Company company, User user, String productPrice) {
        if(validateFreeFreight(company,productPrice)){
            // free freight
            return null;
        } else if(user != null && user.getEmail() != null) {
                List<DeliveryRange> deliveryRangeList = company.getDeliveryRanges()
                        .stream().sorted(Comparator.comparing(DeliveryRange::getDistance)).collect(Collectors.toList());

                Address address = user.getAddressList().stream().filter(ad -> ad.getDefaultAddress()).findFirst().orElseThrow(AddressNotFoundForUserException::new);

                SearchIndex searchIndex = searchIndexRepository.findFirstByUserAndCompanyAndUserAddress(user, company, address);

                if(null != searchIndex) {
                    return deliveryRangeList.stream().filter(dlr -> searchIndex.getDistance() <= dlr.getDistance() * 1000).findFirst().get().getFreightValue();
                }
        }
        return null;
    }

    private Boolean validateFreeFreight(Company company, String productPrice) {
        if(StringUtils.isNotBlank(company.getAmountFreeFreight())) {
            BigDecimal amountForFreeFreight = new BigDecimal(company.getAmountFreeFreight());
            BigDecimal prodPrice = new BigDecimal(productPrice);

            return prodPrice.compareTo(amountForFreeFreight) > 0;
        }
        return false;
    }

}
