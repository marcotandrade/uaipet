package org.uaipet.api.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.uaipet.api.data.model.BusinessHour;
import org.uaipet.api.service.impl.ProductServiceImpl;

import java.time.DayOfWeek;
import java.time.LocalDate;

import java.time.LocalTime;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.Optional;

import static java.lang.Integer.parseInt;

public class CheckDate {

    private static final Logger LOGGER = LoggerFactory.getLogger(ProductServiceImpl.class);

    public static boolean checkIfTheCompanyIsOpen(List<BusinessHour> businessHours) {
        LocalDate localDate = LocalDate.now();
        DayOfWeek dayOfWeek = localDate.getDayOfWeek();
        ZonedDateTime currentDate = ZonedDateTime.now();

        Optional<BusinessHour> businessHour = businessHours
                                                .stream()
                                                .filter(BusinessHour::getActive)
                                                .filter(day -> dayOfWeek.name().equals(day.getDayOfWeek()))
                                                .findAny();
        if(businessHour.isEmpty()){
            LOGGER.warn("businessHour is empty for company");
            return false;
        }

        String[] openingTime = businessHour.get().getOpeningTime().split(":");
        int openingTimeHour = parseInt(openingTime[0]);
        int openingTimeMinutes = parseInt(openingTime[1]);

        String[] closingTime = businessHour.get().getClosingTime().split(":");
        int closingTimeHour = parseInt(closingTime[0]);
        int closingTimeMinutes = parseInt(closingTime[1]);

        Boolean isOpen =
                currentDate
                        .toLocalTime().minusHours(3)
                        .isAfter(LocalTime.of(openingTimeHour, openingTimeMinutes)) &&
                currentDate
                        .toLocalTime().minusHours(3)
                        .isBefore(LocalTime.of(closingTimeHour, closingTimeMinutes));
        return isOpen;
    }
}
