package org.uaipet.api.job;


import lombok.RequiredArgsConstructor;
import net.javacrumbs.shedlock.core.SchedulerLock;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.uaipet.api.service.CompanyService;
import org.uaipet.api.service.CouponService;

@RequiredArgsConstructor
@Component
public class DefaultScheduler {

    private static final Logger log = LoggerFactory.getLogger(DefaultScheduler.class);

    private final CompanyService companyService;
    private final CouponService couponService;

    @Scheduled(cron = "0 10 23 * * *")
    @SchedulerLock(name = "activateBusinessHours_scheduledTask",
            lockAtLeastForString = "PT1380M", lockAtMostForString = "PT1380M")
    public void activateBusinessHours(){
        log.info("activateBusinessHours");
        companyService.scheduledOpenForBusiness();
    }

    @Scheduled(cron = "0 20 23 * * *")
    @SchedulerLock(name = "inactivateCoupon_scheduledTask",
            lockAtLeastForString = "PT1380M", lockAtMostForString = "PT1380M")
    public void inactivateCoupon(){
        log.info("inactivateCoupon");
        couponService.inactiveCoupon();
    }


    @Scheduled(cron = "0 20 3 * * *")
    @SchedulerLock(name = "billingService_scheduledTask",
            lockAtLeastForString = "PT1380M", lockAtMostForString = "PT1380M")
    public void billingService(){
        log.info("billingService");
        couponService.inactiveCoupon();
    }

    @Scheduled(cron = "* */25 * * * *")
    @SchedulerLock(name = "startup_task",
            lockAtLeastForString = "PT1M", lockAtMostForString = "PT1M")
    public void startup(){
        log.info("startup");
    }

}
