CREATE SEQUENCE product_category_id_seq
  START WITH 1
  INCREMENT BY 1;

CREATE SEQUENCE product_animal_category_id_seq
    START WITH 1
    INCREMENT BY 1;


CREATE SEQUENCE age_category_id_seq
      START WITH 1
      INCREMENT BY 1;

CREATE SEQUENCE product_measurement_category_id_seq
      START WITH 1
      INCREMENT BY 1;

CREATE SEQUENCE product_brand_id_seq
           START WITH 1
           INCREMENT BY 1;

INSERT INTO PRODUCT_CATEGORY(id, name, description, created_date, last_modified_date) VALUES (nextval('product_category_id_seq'),'Alimentos',null, now(), now());
INSERT INTO PRODUCT_CATEGORY(id, name, description, created_date, last_modified_date) VALUES (nextval('product_category_id_seq'),'Medicamentos e Saúde',null, now(), now());
INSERT INTO PRODUCT_CATEGORY(id, name, description, created_date, last_modified_date) VALUES (nextval('product_category_id_seq'),'Higiene e beleza',null, now(), now());
INSERT INTO PRODUCT_CATEGORY(id, name, description, created_date, last_modified_date) VALUES (nextval('product_category_id_seq'),'Brinquedos',null, now(), now());
INSERT INTO PRODUCT_CATEGORY(id, name, description, created_date, last_modified_date) VALUES (nextval('product_category_id_seq'),'Passeio e viagem',null, now(), now());
INSERT INTO PRODUCT_CATEGORY(id, name, description, created_date, last_modified_date) VALUES (nextval('product_category_id_seq'),'Conforto',null, now(), now());
INSERT INTO PRODUCT_CATEGORY(id, name, description, created_date, last_modified_date) VALUES (nextval('product_category_id_seq'),'Outros',null, now(), now());

INSERT INTO PRODUCT_ANIMAL_CATEGORY(id, name, description, created_date, last_modified_date) VALUES (nextval('product_animal_category_id_seq'),'Gatos',null, now(), now());
INSERT INTO PRODUCT_ANIMAL_CATEGORY(id, name, description, created_date, last_modified_date) VALUES (nextval('product_animal_category_id_seq'),'Cachorros',null, now(), now());
INSERT INTO PRODUCT_ANIMAL_CATEGORY(id, name, description, created_date, last_modified_date) VALUES (nextval('product_animal_category_id_seq'),'Passaros',null, now(), now());
INSERT INTO PRODUCT_ANIMAL_CATEGORY(id, name, description, created_date, last_modified_date) VALUES (nextval('product_animal_category_id_seq'),'Peixes',null, now(), now());
INSERT INTO PRODUCT_ANIMAL_CATEGORY(id, name, description, created_date, last_modified_date) VALUES (nextval('product_animal_category_id_seq'),'Repteis',null, now(), now());
INSERT INTO PRODUCT_ANIMAL_CATEGORY(id, name, description, created_date, last_modified_date) VALUES (nextval('product_animal_category_id_seq'),'Outros',null, now(), now());

INSERT INTO AGE_CATEGORY(id, name, description, created_date, last_modified_date) VALUES (nextval('age_category_id_seq'),'Recém Nascido',null, now(), now());
INSERT INTO AGE_CATEGORY(id, name, description, created_date, last_modified_date) VALUES (nextval('age_category_id_seq'),'Filhote',null, now(), now());
INSERT INTO AGE_CATEGORY(id, name, description, created_date, last_modified_date) VALUES (nextval('age_category_id_seq'),'Adulto',null, now(), now());
INSERT INTO AGE_CATEGORY(id, name, description, created_date, last_modified_date) VALUES (nextval('age_category_id_seq'),'Idoso',null, now(), now());
INSERT INTO AGE_CATEGORY(id, name, description, created_date, last_modified_date) VALUES (nextval('age_category_id_seq'),'Todas',null, now(), now());


INSERT INTO PRODUCT_MEASUREMENT_CATEGORY(id, name, description, created_date, last_modified_date) VALUES (nextval('product_measurement_category_id_seq'),'Quilograma',null, now(), now());
INSERT INTO PRODUCT_MEASUREMENT_CATEGORY(id, name, description, created_date, last_modified_date) VALUES (nextval('product_measurement_category_id_seq'),'Grama',null, now(), now());
INSERT INTO PRODUCT_MEASUREMENT_CATEGORY(id, name, description, created_date, last_modified_date) VALUES (nextval('product_measurement_category_id_seq'),'Miligrama',null, now(), now());
INSERT INTO PRODUCT_MEASUREMENT_CATEGORY(id, name, description, created_date, last_modified_date) VALUES (nextval('product_measurement_category_id_seq'),'Litros',null, now(), now());
INSERT INTO PRODUCT_MEASUREMENT_CATEGORY(id, name, description, created_date, last_modified_date) VALUES (nextval('product_measurement_category_id_seq'),'Tamanho',null, now(), now());
INSERT INTO PRODUCT_MEASUREMENT_CATEGORY(id, name, description, created_date, last_modified_date) VALUES (nextval('product_measurement_category_id_seq'),'Outros',null, now(), now());


INSERT INTO PRODUCT_BRAND(id, name, description, created_date, last_modified_date) VALUES (nextval('product_brand_id_seq'),'Adaptil',null, now(), now());
INSERT INTO PRODUCT_BRAND(id, name, description, created_date, last_modified_date) VALUES (nextval('product_brand_id_seq'),'Adimax Pet',null, now(), now());
INSERT INTO PRODUCT_BRAND(id, name, description, created_date, last_modified_date) VALUES (nextval('product_brand_id_seq'),'Affinity',null, now(), now());
INSERT INTO PRODUCT_BRAND(id, name, description, created_date, last_modified_date) VALUES (nextval('product_brand_id_seq'),'Agromix',null, now(), now());
INSERT INTO PRODUCT_BRAND(id, name, description, created_date, last_modified_date) VALUES (nextval('product_brand_id_seq'),'Alcon',null, now(), now());
INSERT INTO PRODUCT_BRAND(id, name, description, created_date, last_modified_date) VALUES (nextval('product_brand_id_seq'),'Allcanis',null, now(), now());
INSERT INTO PRODUCT_BRAND(id, name, description, created_date, last_modified_date) VALUES (nextval('product_brand_id_seq'),'Alpo',null, now(), now());
INSERT INTO PRODUCT_BRAND(id, name, description, created_date, last_modified_date) VALUES (nextval('product_brand_id_seq'),'American Pets',null, now(), now());
INSERT INTO PRODUCT_BRAND(id, name, description, created_date, last_modified_date) VALUES (nextval('product_brand_id_seq'),'Anion',null, now(), now());
INSERT INTO PRODUCT_BRAND(id, name, description, created_date, last_modified_date) VALUES (nextval('product_brand_id_seq'),'Apolo',null, now(), now());
INSERT INTO PRODUCT_BRAND(id, name, description, created_date, last_modified_date) VALUES (nextval('product_brand_id_seq'),'Astro',null, now(), now());
INSERT INTO PRODUCT_BRAND(id, name, description, created_date, last_modified_date) VALUES (nextval('product_brand_id_seq'),'Atacama',null, now(), now());
INSERT INTO PRODUCT_BRAND(id, name, description, created_date, last_modified_date) VALUES (nextval('product_brand_id_seq'),'Baw Waw',null, now(), now());
INSERT INTO PRODUCT_BRAND(id, name, description, created_date, last_modified_date) VALUES (nextval('product_brand_id_seq'),'Bayer',null, now(), now());
INSERT INTO PRODUCT_BRAND(id, name, description, created_date, last_modified_date) VALUES (nextval('product_brand_id_seq'),'Bellavita',null, now(), now());
INSERT INTO PRODUCT_BRAND(id, name, description, created_date, last_modified_date) VALUES (nextval('product_brand_id_seq'),'Besser',null, now(), now());
INSERT INTO PRODUCT_BRAND(id, name, description, created_date, last_modified_date) VALUES (nextval('product_brand_id_seq'),'Bicho Green',null, now(), now());
INSERT INTO PRODUCT_BRAND(id, name, description, created_date, last_modified_date) VALUES (nextval('product_brand_id_seq'),'Bilisko',null, now(), now());
INSERT INTO PRODUCT_BRAND(id, name, description, created_date, last_modified_date) VALUES (nextval('product_brand_id_seq'),'Billy Dog',null, now(), now());
INSERT INTO PRODUCT_BRAND(id, name, description, created_date, last_modified_date) VALUES (nextval('product_brand_id_seq'),'Biofresh',null, now(), now());
INSERT INTO PRODUCT_BRAND(id, name, description, created_date, last_modified_date) VALUES (nextval('product_brand_id_seq'),'Birbo',null, now(), now());
INSERT INTO PRODUCT_BRAND(id, name, description, created_date, last_modified_date) VALUES (nextval('product_brand_id_seq'),'BOB',null, now(), now());
INSERT INTO PRODUCT_BRAND(id, name, description, created_date, last_modified_date) VALUES (nextval('product_brand_id_seq'),'Bob Dog',null, now(), now());
INSERT INTO PRODUCT_BRAND(id, name, description, created_date, last_modified_date) VALUES (nextval('product_brand_id_seq'),'Bomguy',null, now(), now());
INSERT INTO PRODUCT_BRAND(id, name, description, created_date, last_modified_date) VALUES (nextval('product_brand_id_seq'),'Bone Apettit',null, now(), now());
INSERT INTO PRODUCT_BRAND(id, name, description, created_date, last_modified_date) VALUES (nextval('product_brand_id_seq'),'Bravecto',null, now(), now());
INSERT INTO PRODUCT_BRAND(id, name, description, created_date, last_modified_date) VALUES (nextval('product_brand_id_seq'),'Canis',null, now(), now());
INSERT INTO PRODUCT_BRAND(id, name, description, created_date, last_modified_date) VALUES (nextval('product_brand_id_seq'),'Capitão Dog',null, now(), now());
INSERT INTO PRODUCT_BRAND(id, name, description, created_date, last_modified_date) VALUES (nextval('product_brand_id_seq'),'Cat Chow',null, now(), now());
INSERT INTO PRODUCT_BRAND(id, name, description, created_date, last_modified_date) VALUES (nextval('product_brand_id_seq'),'Cesar',null, now(), now());
INSERT INTO PRODUCT_BRAND(id, name, description, created_date, last_modified_date) VALUES (nextval('product_brand_id_seq'),'Chalesco',null, now(), now());
INSERT INTO PRODUCT_BRAND(id, name, description, created_date, last_modified_date) VALUES (nextval('product_brand_id_seq'),'Champ',null, now(), now());
INSERT INTO PRODUCT_BRAND(id, name, description, created_date, last_modified_date) VALUES (nextval('product_brand_id_seq'),'Chef',null, now(), now());
INSERT INTO PRODUCT_BRAND(id, name, description, created_date, last_modified_date) VALUES (nextval('product_brand_id_seq'),'Chakal',null, now(), now());
INSERT INTO PRODUCT_BRAND(id, name, description, created_date, last_modified_date) VALUES (nextval('product_brand_id_seq'),'Chakalzinho',null, now(), now());
INSERT INTO PRODUCT_BRAND(id, name, description, created_date, last_modified_date) VALUES (nextval('product_brand_id_seq'),'Chronos Pet',null, now(), now());
INSERT INTO PRODUCT_BRAND(id, name, description, created_date, last_modified_date) VALUES (nextval('product_brand_id_seq'),'Cibau',null, now(), now());
INSERT INTO PRODUCT_BRAND(id, name, description, created_date, last_modified_date) VALUES (nextval('product_brand_id_seq'),'Comfortis',null, now(), now());
INSERT INTO PRODUCT_BRAND(id, name, description, created_date, last_modified_date) VALUES (nextval('product_brand_id_seq'),'Dalpet',null, now(), now());
INSERT INTO PRODUCT_BRAND(id, name, description, created_date, last_modified_date) VALUES (nextval('product_brand_id_seq'),'Dog',null, now(), now());
INSERT INTO PRODUCT_BRAND(id, name, description, created_date, last_modified_date) VALUES (nextval('product_brand_id_seq'),'Dog Chow',null, now(), now());
INSERT INTO PRODUCT_BRAND(id, name, description, created_date, last_modified_date) VALUES (nextval('product_brand_id_seq'),'Dog Excellence',null, now(), now());
INSERT INTO PRODUCT_BRAND(id, name, description, created_date, last_modified_date) VALUES (nextval('product_brand_id_seq'),'Dog Power',null, now(), now());
INSERT INTO PRODUCT_BRAND(id, name, description, created_date, last_modified_date) VALUES (nextval('product_brand_id_seq'),'Dog Star',null, now(), now());
INSERT INTO PRODUCT_BRAND(id, name, description, created_date, last_modified_date) VALUES (nextval('product_brand_id_seq'),'Dogo Premium',null, now(), now());
INSERT INTO PRODUCT_BRAND(id, name, description, created_date, last_modified_date) VALUES (nextval('product_brand_id_seq'),'Domus',null, now(), now());
INSERT INTO PRODUCT_BRAND(id, name, description, created_date, last_modified_date) VALUES (nextval('product_brand_id_seq'),'Ecopet Natural',null, now(), now());
INSERT INTO PRODUCT_BRAND(id, name, description, created_date, last_modified_date) VALUES (nextval('product_brand_id_seq'),'Equilíbrio',null, now(), now());
INSERT INTO PRODUCT_BRAND(id, name, description, created_date, last_modified_date) VALUES (nextval('product_brand_id_seq'),'Evolve',null, now(), now());
INSERT INTO PRODUCT_BRAND(id, name, description, created_date, last_modified_date) VALUES (nextval('product_brand_id_seq'),'Excellent',null, now(), now());
INSERT INTO PRODUCT_BRAND(id, name, description, created_date, last_modified_date) VALUES (nextval('product_brand_id_seq'),'Fabene',null, now(), now());
INSERT INTO PRODUCT_BRAND(id, name, description, created_date, last_modified_date) VALUES (nextval('product_brand_id_seq'),'Farmina',null, now(), now());
INSERT INTO PRODUCT_BRAND(id, name, description, created_date, last_modified_date) VALUES (nextval('product_brand_id_seq'),'Fenix',null, now(), now());
INSERT INTO PRODUCT_BRAND(id, name, description, created_date, last_modified_date) VALUES (nextval('product_brand_id_seq'),'Finotrato',null, now(), now());
INSERT INTO PRODUCT_BRAND(id, name, description, created_date, last_modified_date) VALUES (nextval('product_brand_id_seq'),'Fitpet''s',null, now(), now());
INSERT INTO PRODUCT_BRAND(id, name, description, created_date, last_modified_date) VALUES (nextval('product_brand_id_seq'),'Fort Cão',null, now(), now());
INSERT INTO PRODUCT_BRAND(id, name, description, created_date, last_modified_date) VALUES (nextval('product_brand_id_seq'),'Fosferpet',null, now(), now());
INSERT INTO PRODUCT_BRAND(id, name, description, created_date, last_modified_date) VALUES (nextval('product_brand_id_seq'),'Foster',null, now(), now());
INSERT INTO PRODUCT_BRAND(id, name, description, created_date, last_modified_date) VALUES (nextval('product_brand_id_seq'),'Fri Dog',null, now(), now());
INSERT INTO PRODUCT_BRAND(id, name, description, created_date, last_modified_date) VALUES (nextval('product_brand_id_seq'),'Friskies',null, now(), now());
INSERT INTO PRODUCT_BRAND(id, name, description, created_date, last_modified_date) VALUES (nextval('product_brand_id_seq'),'Frost',null, now(), now());
INSERT INTO PRODUCT_BRAND(id, name, description, created_date, last_modified_date) VALUES (nextval('product_brand_id_seq'),'Futura',null, now(), now());
INSERT INTO PRODUCT_BRAND(id, name, description, created_date, last_modified_date) VALUES (nextval('product_brand_id_seq'),'FVO Alimentos',null, now(), now());
INSERT INTO PRODUCT_BRAND(id, name, description, created_date, last_modified_date) VALUES (nextval('product_brand_id_seq'),'Fórmula Natural',null, now(), now());
INSERT INTO PRODUCT_BRAND(id, name, description, created_date, last_modified_date) VALUES (nextval('product_brand_id_seq'),'Generic',null, now(), now());
INSERT INTO PRODUCT_BRAND(id, name, description, created_date, last_modified_date) VALUES (nextval('product_brand_id_seq'),'Genesis',null, now(), now());
INSERT INTO PRODUCT_BRAND(id, name, description, created_date, last_modified_date) VALUES (nextval('product_brand_id_seq'),'Golden',null, now(), now());
INSERT INTO PRODUCT_BRAND(id, name, description, created_date, last_modified_date) VALUES (nextval('product_brand_id_seq'),'Gran Plus',null, now(), now());
INSERT INTO PRODUCT_BRAND(id, name, description, created_date, last_modified_date) VALUES (nextval('product_brand_id_seq'),'Granvita',null, now(), now());
INSERT INTO PRODUCT_BRAND(id, name, description, created_date, last_modified_date) VALUES (nextval('product_brand_id_seq'),'Guabi',null, now(), now());
INSERT INTO PRODUCT_BRAND(id, name, description, created_date, last_modified_date) VALUES (nextval('product_brand_id_seq'),'Guabi Natural',null, now(), now());
INSERT INTO PRODUCT_BRAND(id, name, description, created_date, last_modified_date) VALUES (nextval('product_brand_id_seq'),'HD',null, now(), now());
INSERT INTO PRODUCT_BRAND(id, name, description, created_date, last_modified_date) VALUES (nextval('product_brand_id_seq'),'Hercosul',null, now(), now());
INSERT INTO PRODUCT_BRAND(id, name, description, created_date, last_modified_date) VALUES (nextval('product_brand_id_seq'),'Herói',null, now(), now());
INSERT INTO PRODUCT_BRAND(id, name, description, created_date, last_modified_date) VALUES (nextval('product_brand_id_seq'),'Hill''s',null, now(), now());
INSERT INTO PRODUCT_BRAND(id, name, description, created_date, last_modified_date) VALUES (nextval('product_brand_id_seq'),'Hill''s Pet Nutrition',null, now(), now());
INSERT INTO PRODUCT_BRAND(id, name, description, created_date, last_modified_date) VALUES (nextval('product_brand_id_seq'),'Hot Dog',null, now(), now());
INSERT INTO PRODUCT_BRAND(id, name, description, created_date, last_modified_date) VALUES (nextval('product_brand_id_seq'),'Hunter',null, now(), now());
INSERT INTO PRODUCT_BRAND(id, name, description, created_date, last_modified_date) VALUES (nextval('product_brand_id_seq'),'Imbramil',null, now(), now());
INSERT INTO PRODUCT_BRAND(id, name, description, created_date, last_modified_date) VALUES (nextval('product_brand_id_seq'),'Japi',null, now(), now());
INSERT INTO PRODUCT_BRAND(id, name, description, created_date, last_modified_date) VALUES (nextval('product_brand_id_seq'),'Jolitex',null, now(), now());
INSERT INTO PRODUCT_BRAND(id, name, description, created_date, last_modified_date) VALUES (nextval('product_brand_id_seq'),'Kdengo',null, now(), now());
INSERT INTO PRODUCT_BRAND(id, name, description, created_date, last_modified_date) VALUES (nextval('product_brand_id_seq'),'Keldog',null, now(), now());
INSERT INTO PRODUCT_BRAND(id, name, description, created_date, last_modified_date) VALUES (nextval('product_brand_id_seq'),'Lester',null, now(), now());
INSERT INTO PRODUCT_BRAND(id, name, description, created_date, last_modified_date) VALUES (nextval('product_brand_id_seq'),'Livelong Healty &amp; Strong',null, now(), now());
INSERT INTO PRODUCT_BRAND(id, name, description, created_date, last_modified_date) VALUES (nextval('product_brand_id_seq'),'Lucky',null, now(), now());
INSERT INTO PRODUCT_BRAND(id, name, description, created_date, last_modified_date) VALUES (nextval('product_brand_id_seq'),'Líder',null, now(), now());
INSERT INTO PRODUCT_BRAND(id, name, description, created_date, last_modified_date) VALUES (nextval('product_brand_id_seq'),'Magnus',null, now(), now());
INSERT INTO PRODUCT_BRAND(id, name, description, created_date, last_modified_date) VALUES (nextval('product_brand_id_seq'),'Mano',null, now(), now());
INSERT INTO PRODUCT_BRAND(id, name, description, created_date, last_modified_date) VALUES (nextval('product_brand_id_seq'),'Marisol',null, now(), now());
INSERT INTO PRODUCT_BRAND(id, name, description, created_date, last_modified_date) VALUES (nextval('product_brand_id_seq'),'Mars Petcare',null, now(), now());
INSERT INTO PRODUCT_BRAND(id, name, description, created_date, last_modified_date) VALUES (nextval('product_brand_id_seq'),'Matacura',null, now(), now());
INSERT INTO PRODUCT_BRAND(id, name, description, created_date, last_modified_date) VALUES (nextval('product_brand_id_seq'),'Max',null, now(), now());
INSERT INTO PRODUCT_BRAND(id, name, description, created_date, last_modified_date) VALUES (nextval('product_brand_id_seq'),'Max Cat',null, now(), now());
INSERT INTO PRODUCT_BRAND(id, name, description, created_date, last_modified_date) VALUES (nextval('product_brand_id_seq'),'Miramar',null, now(), now());
INSERT INTO PRODUCT_BRAND(id, name, description, created_date, last_modified_date) VALUES (nextval('product_brand_id_seq'),'Mix Dog',null, now(), now());
INSERT INTO PRODUCT_BRAND(id, name, description, created_date, last_modified_date) VALUES (nextval('product_brand_id_seq'),'Monello',null, now(), now());
INSERT INTO PRODUCT_BRAND(id, name, description, created_date, last_modified_date) VALUES (nextval('product_brand_id_seq'),'Multidog',null, now(), now());
INSERT INTO PRODUCT_BRAND(id, name, description, created_date, last_modified_date) VALUES (nextval('product_brand_id_seq'),'Murano',null, now(), now());
INSERT INTO PRODUCT_BRAND(id, name, description, created_date, last_modified_date) VALUES (nextval('product_brand_id_seq'),'Must',null, now(), now());
INSERT INTO PRODUCT_BRAND(id, name, description, created_date, last_modified_date) VALUES (nextval('product_brand_id_seq'),'Natural',null, now(), now());
INSERT INTO PRODUCT_BRAND(id, name, description, created_date, last_modified_date) VALUES (nextval('product_brand_id_seq'),'Natural &amp; Delicious',null, now(), now());
INSERT INTO PRODUCT_BRAND(id, name, description, created_date, last_modified_date) VALUES (nextval('product_brand_id_seq'),'Naturalis',null, now(), now());
INSERT INTO PRODUCT_BRAND(id, name, description, created_date, last_modified_date) VALUES (nextval('product_brand_id_seq'),'Nature',null, now(), now());
INSERT INTO PRODUCT_BRAND(id, name, description, created_date, last_modified_date) VALUES (nextval('product_brand_id_seq'),'Neopet',null, now(), now());
INSERT INTO PRODUCT_BRAND(id, name, description, created_date, last_modified_date) VALUES (nextval('product_brand_id_seq'),'Nero',null, now(), now());
INSERT INTO PRODUCT_BRAND(id, name, description, created_date, last_modified_date) VALUES (nextval('product_brand_id_seq'),'New Pet',null, now(), now());
INSERT INTO PRODUCT_BRAND(id, name, description, created_date, last_modified_date) VALUES (nextval('product_brand_id_seq'),'NexGard',null, now(), now());
INSERT INTO PRODUCT_BRAND(id, name, description, created_date, last_modified_date) VALUES (nextval('product_brand_id_seq'),'Nhock!',null, now(), now());
INSERT INTO PRODUCT_BRAND(id, name, description, created_date, last_modified_date) VALUES (nextval('product_brand_id_seq'),'Nutridani',null, now(), now());
INSERT INTO PRODUCT_BRAND(id, name, description, created_date, last_modified_date) VALUES (nextval('product_brand_id_seq'),'Nutrire',null, now(), now());
INSERT INTO PRODUCT_BRAND(id, name, description, created_date, last_modified_date) VALUES (nextval('product_brand_id_seq'),'Nutrição',null, now(), now());
INSERT INTO PRODUCT_BRAND(id, name, description, created_date, last_modified_date) VALUES (nextval('product_brand_id_seq'),'Optimum',null, now(), now());
INSERT INTO PRODUCT_BRAND(id, name, description, created_date, last_modified_date) VALUES (nextval('product_brand_id_seq'),'Origens',null, now(), now());
INSERT INTO PRODUCT_BRAND(id, name, description, created_date, last_modified_date) VALUES (nextval('product_brand_id_seq'),'Ouro Fino',null, now(), now());
INSERT INTO PRODUCT_BRAND(id, name, description, created_date, last_modified_date) VALUES (nextval('product_brand_id_seq'),'Pastel Dog',null, now(), now());
INSERT INTO PRODUCT_BRAND(id, name, description, created_date, last_modified_date) VALUES (nextval('product_brand_id_seq'),'Pedigree',null, now(), now());
INSERT INTO PRODUCT_BRAND(id, name, description, created_date, last_modified_date) VALUES (nextval('product_brand_id_seq'),'Percane',null, now(), now());
INSERT INTO PRODUCT_BRAND(id, name, description, created_date, last_modified_date) VALUES (nextval('product_brand_id_seq'),'PerGatti',null, now(), now());
INSERT INTO PRODUCT_BRAND(id, name, description, created_date, last_modified_date) VALUES (nextval('product_brand_id_seq'),'Pet Delícia',null, now(), now());
INSERT INTO PRODUCT_BRAND(id, name, description, created_date, last_modified_date) VALUES (nextval('product_brand_id_seq'),'Pet Food Solution',null, now(), now());
INSERT INTO PRODUCT_BRAND(id, name, description, created_date, last_modified_date) VALUES (nextval('product_brand_id_seq'),'Petinjet',null, now(), now());
INSERT INTO PRODUCT_BRAND(id, name, description, created_date, last_modified_date) VALUES (nextval('product_brand_id_seq'),'PetSafe',null, now(), now());
INSERT INTO PRODUCT_BRAND(id, name, description, created_date, last_modified_date) VALUES (nextval('product_brand_id_seq'),'Premiatta',null, now(), now());
INSERT INTO PRODUCT_BRAND(id, name, description, created_date, last_modified_date) VALUES (nextval('product_brand_id_seq'),'Premier',null, now(), now());
INSERT INTO PRODUCT_BRAND(id, name, description, created_date, last_modified_date) VALUES (nextval('product_brand_id_seq'),'Premier pet',null, now(), now());
INSERT INTO PRODUCT_BRAND(id, name, description, created_date, last_modified_date) VALUES (nextval('product_brand_id_seq'),'Presence',null, now(), now());
INSERT INTO PRODUCT_BRAND(id, name, description, created_date, last_modified_date) VALUES (nextval('product_brand_id_seq'),'Prime',null, now(), now());
INSERT INTO PRODUCT_BRAND(id, name, description, created_date, last_modified_date) VALUES (nextval('product_brand_id_seq'),'Primor',null, now(), now());
INSERT INTO PRODUCT_BRAND(id, name, description, created_date, last_modified_date) VALUES (nextval('product_brand_id_seq'),'Primus',null, now(), now());
INSERT INTO PRODUCT_BRAND(id, name, description, created_date, last_modified_date) VALUES (nextval('product_brand_id_seq'),'Pro Omega',null, now(), now());
INSERT INTO PRODUCT_BRAND(id, name, description, created_date, last_modified_date) VALUES (nextval('product_brand_id_seq'),'Pro Plan',null, now(), now());
INSERT INTO PRODUCT_BRAND(id, name, description, created_date, last_modified_date) VALUES (nextval('product_brand_id_seq'),'Purina',null, now(), now());
INSERT INTO PRODUCT_BRAND(id, name, description, created_date, last_modified_date) VALUES (nextval('product_brand_id_seq'),'Qualidy',null, now(), now());
INSERT INTO PRODUCT_BRAND(id, name, description, created_date, last_modified_date) VALUES (nextval('product_brand_id_seq'),'Qualy Day',null, now(), now());
INSERT INTO PRODUCT_BRAND(id, name, description, created_date, last_modified_date) VALUES (nextval('product_brand_id_seq'),'Quartz',null, now(), now());
INSERT INTO PRODUCT_BRAND(id, name, description, created_date, last_modified_date) VALUES (nextval('product_brand_id_seq'),'Quatree',null, now(), now());
INSERT INTO PRODUCT_BRAND(id, name, description, created_date, last_modified_date) VALUES (nextval('product_brand_id_seq'),'Raminelli Pet Food',null, now(), now());
INSERT INTO PRODUCT_BRAND(id, name, description, created_date, last_modified_date) VALUES (nextval('product_brand_id_seq'),'Royal',null, now(), now());
INSERT INTO PRODUCT_BRAND(id, name, description, created_date, last_modified_date) VALUES (nextval('product_brand_id_seq'),'Royal Canin',null, now(), now());
INSERT INTO PRODUCT_BRAND(id, name, description, created_date, last_modified_date) VALUES (nextval('product_brand_id_seq'),'Sanol',null, now(), now());
INSERT INTO PRODUCT_BRAND(id, name, description, created_date, last_modified_date) VALUES (nextval('product_brand_id_seq'),'Schesir',null, now(), now());
INSERT INTO PRODUCT_BRAND(id, name, description, created_date, last_modified_date) VALUES (nextval('product_brand_id_seq'),'Sheba',null, now(), now());
INSERT INTO PRODUCT_BRAND(id, name, description, created_date, last_modified_date) VALUES (nextval('product_brand_id_seq'),'Snack',null, now(), now());
INSERT INTO PRODUCT_BRAND(id, name, description, created_date, last_modified_date) VALUES (nextval('product_brand_id_seq'),'Special Cat',null, now(), now());
INSERT INTO PRODUCT_BRAND(id, name, description, created_date, last_modified_date) VALUES (nextval('product_brand_id_seq'),'Special Dog',null, now(), now());
INSERT INTO PRODUCT_BRAND(id, name, description, created_date, last_modified_date) VALUES (nextval('product_brand_id_seq'),'Specialle Pet',null, now(), now());
INSERT INTO PRODUCT_BRAND(id, name, description, created_date, last_modified_date) VALUES (nextval('product_brand_id_seq'),'Speed Dog',null, now(), now());
INSERT INTO PRODUCT_BRAND(id, name, description, created_date, last_modified_date) VALUES (nextval('product_brand_id_seq'),'Spert',null, now(), now());
INSERT INTO PRODUCT_BRAND(id, name, description, created_date, last_modified_date) VALUES (nextval('product_brand_id_seq'),'Star Foods',null, now(), now());
INSERT INTO PRODUCT_BRAND(id, name, description, created_date, last_modified_date) VALUES (nextval('product_brand_id_seq'),'Supra',null, now(), now());
INSERT INTO PRODUCT_BRAND(id, name, description, created_date, last_modified_date) VALUES (nextval('product_brand_id_seq'),'The Pets Brasil',null, now(), now());
INSERT INTO PRODUCT_BRAND(id, name, description, created_date, last_modified_date) VALUES (nextval('product_brand_id_seq'),'Three Dogs',null, now(), now());
INSERT INTO PRODUCT_BRAND(id, name, description, created_date, last_modified_date) VALUES (nextval('product_brand_id_seq'),'Tinkle Dog',null, now(), now());
INSERT INTO PRODUCT_BRAND(id, name, description, created_date, last_modified_date) VALUES (nextval('product_brand_id_seq'),'Total Alimentos',null, now(), now());
INSERT INTO PRODUCT_BRAND(id, name, description, created_date, last_modified_date) VALUES (nextval('product_brand_id_seq'),'Vet Life',null, now(), now());
INSERT INTO PRODUCT_BRAND(id, name, description, created_date, last_modified_date) VALUES (nextval('product_brand_id_seq'),'VittaMax',null, now(), now());
INSERT INTO PRODUCT_BRAND(id, name, description, created_date, last_modified_date) VALUES (nextval('product_brand_id_seq'),'Whiskas',null, now(), now());



insert into users
        (ID, created_date, last_modified_date, company_id, cpf, email, expo_token, image, name, password, phone, status, type, validation_code)
    values  (1, now(),now(),NULL, '09487484847', 'marcotulio@teste.com',null,null,'Marco Túlio', '$2a$10$1iCeFGcvJXQymb0DMveiX.MUMDrRWBfXnjJPJZ5ZMOwhh8ZDsn93G',
    '34999867686','valid','CUSTOMER', NULL);


    insert
    into
        address
        (created_date, last_modified_date, city, complement, default_address, deleted, district, latitude, longitude, number, state, street, user_id, zip_code)
    values
        (now(),now(),'Uberlândia','AP 402, BL 2', TRUE, FALSE,'BRASIL',NULL,NULL,90,'MG','Rua Renato de Oliveira Grama',1,38411673);


insert into user_role values (1,now(),now(),'USER',1);